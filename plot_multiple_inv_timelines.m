%script to plot multiple inversion results

filenamecell={'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d4000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d6000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop1_8_h20frac0_25.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop1_8_h20frac0_9.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P800_Rc10_Rr1250_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1200_Rc10_Rr1250_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc5_Rr1250_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc15_Rr1250_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1350_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1150_fixedlaketop1_8.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop1_4.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop2_2.mat';
    'volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop1_8.mat'};
%shift = (700 - (-1940 + 1250))*9.8*1000
legcell = {'South Caldera reservoir centroid elevation -3000 m ASL';
    'South Caldera reservoir centroid elevation -5000 m ASL';
    'H2O/CO2 mass ratio 0.33';
    'H2O/CO2 mass ratio 9';
    'baseline (3/7/2011) conduit bottom pressure 2.3 MPa';
    'baseline (3/7/2011) conduit bottom pressure 3.4 MPa';
    'conduit radius 5 m';
    'conduit radius 15 m';
    'conduit length 190 m';
    'conduit length 390 m';
    'lava lake top volatiles 1.4 wt%';
    'lava lake top volatiles 2.2 wt%';
    'reference scenario'};

% pcell = {true;
%     true;
%     false;
%     false;
%     false;
%     false;
%     false;
%     false;
%     false;
%     false;
%     false;
%     false;
%     false};


colorcell = {[0.4,0.4,1];
    [0,0,0.8];
    [0.4,1,0.4];
    [0,0.8,0];
    [1,0.4,0.4];
    [0.8,0,0];
    [0.8,0.4,0.8];
    [0.4,0,0.4];
    [0.8,0.8,0.4];
    [0.4,0.4,0];
    [0.4,0.8,0.8];
    [0,0.4,0.4];
    [0,0,0]};

symcell = {'-','-','-','-','-','-','-','-','-','-','-','-',':'};
linewidthcell = {0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,0.75,1.5};

parray = gobjects(size(legcell));

usesmooth = true;

figure()
Tlim = [15,43];
Xlim = [datetime(2009,8,1),datetime(2018,6,1)];
heightincrease = 0.02;
plotL = 0.05;
plotw = 0.945;

% ax1 = subplot('Position',[plotL,0.8,plotw,0.21]); hold on
% % scatter(VLP_tab_cr.start,VLP_tab_cr.TC,9,VLP_tab_cr.volatile_resnorm,'filled','MarkerEdgeColor','k')
% % colormap(ax3,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
% ylabel('pressure change (MPa)')
% hold on
% grid on, grid minor, box on
% ytickangle(90)
% % yticks([1000,1100,1200,1300,1400,1500]),yticklabels({'','1100','','1300','',''})
% % ylim([min(VLP_tab_cr.TC),max(VLP_tab_cr.TC)])
% xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
% xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
% xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
% xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
% set(gca,'Layer','top')
% xticklabels([])
% ttl = title('a.');
% set(gca,'FontSize',11)

ax2 = subplot('Position',[plotL,0.67,plotw,0.31]); hold on
% scatter(VLP_tab_cr.start,VLP_tab_cr.TC,9,VLP_tab_cr.volatile_resnorm,'filled','MarkerEdgeColor','k')
% colormap(ax3,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
ylabel('temperature (C)')
hold on
grid on, grid minor, box on
ytickangle(90)
% yticks([1000,1100,1200,1300,1400,1500]),yticklabels({'','1100','','1300','',''})
% ylim([min(VLP_tab_cr.TC),max(VLP_tab_cr.TC)])
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('a.');
set(gca,'FontSize',11)

ax3 = subplot('Position',[plotL,0.35,plotw,0.31]);
ylabel('X^{avg} (wt%)')
ytickangle(90)
% ylim([1.5,4.5])
% yticks([0,1,2,3,4])
ytickangle(90)
hold on
grid on, grid minor, box on
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('b.');
set(gca,'FontSize',11)

ax4 = subplot('Position',[plotL,0.03,plotw,0.31]);
ylabel('\Delta X (wt%)')
%ylim([0,4.5])
%yticks([-4,-3,-2,-1,0])
ytickangle(90)
hold on
grid on, grid minor, box on
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
% xticklabels([])
ttl = title('c.');
set(gca,'FontSize',11)

for jj = 1:length(filenamecell)
    %% volatile nonlin sol plot preprocess
    vary_CO2 = false;
    vary_H2O = false;
    fixed_ratio = true;
    vary_lake = false;
    load(filenamecell{jj});
 
    if usesmooth
        cr_table = (VLP_tab_cr.start(1):days(1):VLP_tab_cr.start(end))';
        cr_table = timetable([cr_table(:)],NaN(size(cr_table)),'VariableNames',{'T_7'});
        % cr_table.T_7 = NaN(size(cr_table.Time));
        cr_table.Q_7 = NaN(size(cr_table.Time));
        if vary_CO2
            cr_table.CO2_condbot_7 = NaN(size(cr_table.Time));
            cr_table.CO2_condtop_7 = NaN(size(cr_table.Time));
            % cr_table.CO2_laketop_7 = NaN(size(cr_table.Time));
        elseif vary_H2O
            cr_table.H2O_condbot_7 = NaN(size(cr_table.Time));
            cr_table.H2O_condtop_7 = NaN(size(cr_table.Time));
            % cr_table.H2O_laketop_7 = NaN(size(cr_table.Time));
        elseif fixed_ratio
            cr_table.vol_condbot_7 = NaN(size(cr_table.Time));
            cr_table.vol_condtop_7 = NaN(size(cr_table.Time));
            % cr_table.vol_laketop_7 = NaN(size(cr_table.Time));
        elseif vary_lake
            cr_table.vol_cond_7 = NaN(size(cr_table.Time));
            cr_table.vol_lake_7 = NaN(size(cr_table.Time));
        end
        cr_table.TC_7 = NaN(size(cr_table.Time));

        Tspan = days(30/2);

        for ii = 1:length(cr_table.Time)  
            datain = VLP_tab_cr.start >= cr_table.Time(ii) - Tspan & ...
                VLP_tab_cr.start <= cr_table.Time(ii) + Tspan;
            dist = abs(VLP_tab_cr.start(datain) - cr_table.Time(ii));
            weight = 1-dist/Tspan;

            datain = datain & ~isnan(VLP_tab_cr.TC);
            dist = abs(VLP_tab_cr.start(datain) - cr_table.Time(ii));
            weight = 1-dist/Tspan;
            cr_table.vol_condbot_7(ii) = sum(VLP_tab_cr.vol_condbot(datain).*weight)/sum(weight);
            cr_table.vol_condtop_7(ii) = sum(VLP_tab_cr.vol_condtop(datain).*weight)/sum(weight);
            cr_table.vol_7(ii) = sum(VLP_tab_cr.vol(datain).*weight)/sum(weight);
            cr_table.dvol_7(ii) = sum(VLP_tab_cr.dvol(datain).*weight)/sum(weight);
            % cr_table.vol_laketop_7(ii) = sum(VLP_tab_cr.vol_laketop(datain).*weight)/sum(weight);
            cr_table.TC_7(ii) = sum(VLP_tab_cr.TC(datain).*weight)/sum(weight);
        end
    end
    
    
    %% plots
%     plot(ax1,VLP_tab_cr.start,VLP_tab_cr.P_restop,'.','LineWidth',0.5,'MarkerSize',6,'Color',colorcell{jj});
    if ~usesmooth
        parray(jj) = plot(ax2,VLP_tab_cr.start,VLP_tab_cr.TC,'.','LineWidth',0.5,'MarkerSize',6,'Color',colorcell{jj});
    else
        parray(jj) = plot(ax2,cr_table.Time,cr_table.TC_7,symcell{jj},'LineWidth',linewidthcell{jj},'Color',colorcell{jj});
    end
    
    if ~usesmooth
        plot(ax3,VLP_tab_cr.start,VLP_tab_cr.vol,'.','LineWidth',0.5,'MarkerSize',6,'Color',colorcell{jj})
    else
        plot(ax3,cr_table.Time,cr_table.vol_7,symcell{jj},'LineWidth',linewidthcell{jj},'Color',colorcell{jj})
    end
    
    if ~usesmooth
        plot(ax4,VLP_tab_cr.start,-VLP_tab_cr.dvol,'.','LineWidth',0.5,'MarkerSize',6,'Color',colorcell{jj})
    else
        plot(ax4,cr_table.Time,-cr_table.dvol_7,symcell{jj},'LineWidth',linewidthcell{jj},'Color',colorcell{jj})
    end
    
end
lgd = legend(ax2,parray,legcell,'AutoUpdate','off');
lgd.NumColumns = 2;
lgd.Orientation = 'horizontal';
xlim(ax2,Xlim)
ylim(ax2,[1101,1499])
yticks(ax2,1100:100:1500)
xlim(ax3,Xlim)
ylim(ax3,[1.4,4.7])
yticks(ax3,0:5)
xlim(ax4,Xlim)
ylim(ax4,[-0.5,6])
yticks(ax4,0:5)


