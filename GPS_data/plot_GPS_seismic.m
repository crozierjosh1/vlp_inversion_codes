%% Set up the Import Options and import the data from UNR files
s = struct();

s(1).station = 'UWEV';
s(end).latlon = [19+25/60+15.14/3600, -(155+17/60+28.08/3600), 1257.1];

s(end+1).station = 'CRIM';
s(end).latlon = [19+23/60+43.11/3600, -(155+16/60+25.54/3600), 1147.7];

s(end+1).station = 'OUTL';
s(end).latlon = [19+23/60+12.94/3600, -(155+16/60+50.88/3600), 1102.8];

s(end+1).station = 'AHUP';
s(end).latlon = [19+22/60+44.80/3600, -(155+15/60+57.96/3600), 1104.8471];

s(end+1).station = 'BYRL';
s(end).latlon = [19+24/60+43.06/3600, -(155+15/60+36.19/3600), 1097.8];

s(end+1).station = 'CNPK';
s(end).latlon = [19+23/60+32.06/3600, -(155+18/60+20.35/3600), 1123.9];

s(end+1).station = 'MANE';
s(end).latlon = [19.339, -155.273, 996.466];

s(end+1).station = 'NPIT';
s(end).latlon = [19.412, -155.281, 1132.112];

s(end+1).station = 'KOSM';
s(end).latlon = [19.363, -155.316, 990.363];

ss = struct();
ss(1).station = 'NPT/NPB';
ss(1).latlon = [19.41200 -155.28100 1115.0];

ss(end+1).station = 'SRM/RIMD';
ss(end).latlon = [19.39531 -155.27400 1128.0];

ss(end+1).station = 'OBL';
ss(end).latlon = [19.41750 -155.28400 1107.0];

ss(end+1).station = 'WRM';
ss(end).latlon = [19.40650 -155.30000 1163.0];

ss(end+1).station = 'SDH';
ss(end).latlon = [19.38950 -155.29400 1133.0];

ss(end+1).station = 'UWE';
ss(end).latlon = [19.42111 -155.29300 1240.0];

ss(end+1).station = 'UWB';
ss(end).latlon = [19.42469 -155.27800 1091.0];

ss(end+1).station = 'SBL';
ss(end).latlon = [19.42700 -155.26800 1084.0];

ss(end+1).station = 'HAT';
ss(end).latlon = [19.42300 -155.26100 1082.0];

ss(end+1).station = 'BYL';
ss(end).latlon = [19.41200 -155.26000 1059.0];

ss(end+1).station = 'KKO';
ss(end).latlon = [19.39781 -155.26600 1146.0];

% ss(end+1).station = 'RIMD';
% ss(end).latlon = [19.39531 -155.27400 1128.0];

ss(end+1).station = 'PUHI';
ss(end).latlon = [19.385510 -155.251310 1079];


%% utm
zone = utmzone(s(1).latlon(1),s(1).latlon(2));
utmstruct = defaultm('utm');
utmstruct.zone = zone;
utmstruct.geoid = wgs84Ellipsoid;
utmstruct = defaultm(utmstruct);
for i = 1:length(s)
    s(i).loc = [0 0 0];
   [s(i).loc_east, s(i).loc_north, s(i).loc_el] = ...
       mfwdtran(utmstruct,s(i).latlon(1),s(i).latlon(2),s(i).latlon(3)); 
end
for i = 1:length(ss)
    ss(i).loc = [0 0 0];
   [ss(i).loc_east, ss(i).loc_north, ss(i).loc_el] = ...
       mfwdtran(utmstruct,ss(i).latlon(1),ss(i).latlon(2),ss(i).latlon(3)); 
end


%% figure
fighandle = figure('Name','Ground Displacements');
waveformaxishandles = gobjects(1);
%cmap = cmocean('thermal',64);
% cmap = hsv(100);
% cmap = [cmap;cmap];
% saturation = linspace(-1,1,size(cmap,1))';
% saturation = 0.4+0.6*(1./(1 + exp(-saturation*10)));
% cmap = cmap.*repmat(saturation,1,3);
cmap = parula;
colormap(cmap)
fighandle.Units = 'pixels';
% screensize = get(0,'screensize');
% fighandle.OuterPosition = [screensize(3)/2,1,screensize(3)/2,screensize(4)];


%% DEM
%import dem
DEM = struct();
[DEM.Z,DEM.x,DEM.y] = import_hawaii_dem();
domain = [256.5,264.5,2139.7,2150.3]*1000;
DEM.Z = DEM.Z(:,DEM.x>=domain(1)&DEM.x<=domain(2));
DEM.x = DEM.x(DEM.x>=domain(1)&DEM.x<=domain(2));
DEM.Z = DEM.Z(DEM.y>=domain(3)&DEM.y<=domain(4),:);
DEM.y = DEM.y(DEM.y>=domain(3)&DEM.y<=domain(4));


%downsample
DEM.Z = imgaussfilt(DEM.Z,1);
% DEM.downsamp = floor(length(DEM.x)/100);
% DEM.x = DEM.x(1:DEM.downsamp:end);
% DEM.y = DEM.y(1:DEM.downsamp:end);
% DEM.Z = DEM.Z(1:DEM.downsamp:end,1:DEM.downsamp:end);


epilatlon = [19.405402 -155.281041 100.0];
zone = utmzone(epilatlon(1),epilatlon(2));
utmstruct = defaultm('utm');
utmstruct.zone = zone;
utmstruct.geoid = wgs84Ellipsoid;
utmstruct = defaultm(utmstruct);
[epiloc_east, epiloc_north, epiloc_vert] = mfwdtran(utmstruct,epilatlon(1),epilatlon(2),epilatlon(3));

    
%% plot ground disp
dispgrid_ax = subplot('position',[0.08,0.08,0.91,0.91]);
hold on

axes(dispgrid_ax)
plot([0,0],[0,0]),hold on
xlabel('east (km), UTM 5Q')
ylabel('north (km)')
grid off
box on
axis equal
xlim([min((DEM.x-epiloc_east))/1000,max((DEM.x-epiloc_east))/1000])
ylim([min((DEM.y-epiloc_north))/1000,max((DEM.y-epiloc_north))/1000])
set(gca,'ydir','normal')
%contour((DEM.x-epiloc_east)/1000,(DEM.y-epiloc_north)/1000,DEM.Z,0:20:3000,'LineColor',[0.8,0.8,0.8])
cmap = cmocean('topo',100);
cmap = cmap(60:end,:);
cmap(cmap>1) = 1;
[DEM.xgrid,DEM.ygrid] = meshgrid(DEM.x,DEM.y);
[cindx,cimap,clim] = shaderel((DEM.xgrid-epiloc_east)/1000,(DEM.ygrid-epiloc_north)/1000,DEM.Z/1000,cmap,[120,45]);
p = pcolor((DEM.x-epiloc_east)/1000,(DEM.y-epiloc_north)/1000,cindx);
colormap(cimap); caxis(clim); p.EdgeColor = 'none';
set(gca,'layer','top')

for jj = 1:length(s)
    p1 = scatter((s(jj).loc_east-epiloc_east)/1000,(s(jj).loc_north-epiloc_north)/1000,20,...
        'k','+','LineWidth',1.5);
    text((s(jj).loc_east-epiloc_east)/1000+0.14,(s(jj).loc_north-epiloc_north)/1000+0.14,...
        s(jj).station,'Color','k','FontWeight','bold')
end
for jj = 1:length(ss)
    p2 = scatter((ss(jj).loc_east-epiloc_east)/1000,(ss(jj).loc_north-epiloc_north)/1000,20,...
        [0.4,0.4,0.4],'x','LineWidth',1.5);
    text((ss(jj).loc_east-epiloc_east)/1000+0.14,(ss(jj).loc_north-epiloc_north)/1000-0.14,...
        ss(jj).station,'Color',[0.4,0.4,0.4],'FontWeight','bold')
end
legend([p1,p2],{'GPS','seismic'},'Location','NorthWest','AutoUpdate','off')


source.latlon_ref = [19.4073, -155.2784]; %reference lat-lon from Anderson 2019
[source.east_ref, source.north_ref,~] = ...
    mfwdtran(utmstruct,source.latlon_ref(1),source.latlon_ref(2),0);
source.east = source.east_ref + 70.3;
source.north = source.north_ref + 183;
V = 3.94E9; %reservoir volume (large negative impact)
aspectratio = 1.23; %height/width (minor impact)
R_hor = (V*3/(4*pi*aspectratio))^(1/3);
% plot((source.east-epiloc_east)/1000,(source.north-epiloc_north)/1000,'+',...
%     'Color','g','Markersize',9,'LineWidth',2)
theta = linspace(0,2*pi,60);
x = R_hor*cos(theta);
y = R_hor*sin(theta);
patch((source.east-epiloc_east+x)/1000,(source.north-epiloc_north+y)/1000,'green','EdgeColor','b','LineWidth',0.5,'FaceColor','b','FaceAlpha',0.1)
text((source.east-epiloc_east)/1000-0.1,(source.north-epiloc_north)/1000,'Halema`uma`u reservoir','Color','b','FontWeight','bold','FontSize',12)

[SCR_source.east, SCR_source.north,~] = ...
        mfwdtran(utmstruct,19.39,-155.27,0);
SCR_V = 20E9; %reservoir volume
SCR_aspectratio = 1.0; %height/width
SCR_R_hor = (SCR_V*3/(4*pi*SCR_aspectratio))^(1/3);
theta = linspace(0,2*pi,60);
x = SCR_R_hor*cos(theta);
y = SCR_R_hor*sin(theta);
patch((SCR_source.east-epiloc_east+x)/1000,(SCR_source.north-epiloc_north+y)/1000,'magenta','EdgeColor','r','LineWidth',0.5,'FaceColor','r','FaceAlpha',0.1)
% plot((SCR_source.east-epiloc_east)/1000,(SCR_source.north-epiloc_north)/1000,'+',...
%     'Color','m','Markersize',9,'LineWidth',2)
text((SCR_source.east-epiloc_east)/1000-0.1,(SCR_source.north-epiloc_north)/1000,'South Caldera reservoir','Color','r','FontWeight','bold','FontSize',12)

[int_source.east, int_source.north,~] = ...
        mfwdtran(utmstruct,19.3857,-155.287,0);
int_R_vert = 3687/2;
int_aspectratio = 0.496; %height/width
int_R_hor = int_aspectratio*int_R_vert;
[int_source.east, int_source.north,~] = ...
    mfwdtran(utmstruct,19.3857,-155.287,0); 
int_source.depth = 2832;
int_source.Zref = 990.4;
int_dip = 2.342;
int_strike = 249.7;
theta = linspace(0,2*pi,60);
thetashift = theta+int_strike;
int_R = int_R_hor*int_R_vert./sqrt((int_R_hor*sin(thetashift)).^2+(int_R_vert*cos(thetashift)).^2);
x = int_R.*cos(theta);
y = int_R.*sin(theta);
patch((int_source.east-epiloc_east+x)/1000,(int_source.north-epiloc_north+y)/1000,'cyan','EdgeColor','m','LineWidth',0.5,'FaceColor','m','FaceAlpha',0.1)
% plot((int_source.east-epiloc_east)/1000,(int_source.north-epiloc_north)/1000,'+',...
%     'Color','c','Markersize',9,'LineWidth',2)
text((int_source.east-epiloc_east)/1000-0.6,(int_source.north-epiloc_north)/1000,'2015 intrusion','Color','m','FontWeight','bold','FontSize',12)

plot(0,0,'o','Color',[0,0.5,0.5],'Markersize',4,'LineWidth',2)
text(0+0.14,0-0.07,'vent','Color',[0,0.5,0.5],'FontWeight','bold','FontSize',12)

text(-3.8,-6.5,'UTM 5Q\newlinezero at\newline19.4054^{\circ}N\newline-155.2810^{\circ}E')

set(gca,'FontSize',11)
% title('b. map')





