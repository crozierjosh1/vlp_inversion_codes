function animate_ground_displacements_old(s)
%animate ground displacements

timeindexstart = 1;

%%%%lava lake location (from Chouet work)
epilatlon = [19.405402 -155.281041 100.0];
%convert lat-lon locations to UTM E-N coordinates
zone = utmzone(epilatlon(1),epilatlon(2));
utmstruct = defaultm('utm');
utmstruct.zone = zone;
utmstruct.geoid = wgs84Ellipsoid;
utmstruct = defaultm(utmstruct);
[epiloc_east, epiloc_north, epiloc_vert] = mfwdtran(utmstruct,epilatlon(1),epilatlon(2),epilatlon(3));

%figure mapview ground displacements
figure('Name','Mapview Displacements','Position',[10,10,1000,800])
circlescale = 5E6;
ground_disp_anim(length(s(1).datetime)) = struct('cdata',[],'colormap',[]);
for timeindex = timeindexstart:1:length(s(1).datetime)
    clf
    plot3(s(1).loc_east-epiloc_east,s(1).loc_north-epiloc_north,1000,'b','Markersize',10),hold on
    plot3(s(1).loc_east-epiloc_east,s(1).loc_north-epiloc_north,1000,'g','Markersize',10)
    for i = 1:length(s)
        %predamp = max((s(i).east_pred.^2 + s(i).north_pred.^2 + s(i).vert_pred.^2).^.5,[],2);
        obsamp = max((s(i).east.^2 + s(i).north.^2 + s(i).vert.^2).^.5,[],2);
        %quiver3(s(i).loc_east-epiloc_east,s(i).loc_north-epiloc_north,0,circlescale*s(i).east_pred(timeindex),circlescale*s(i).north_pred(timeindex),circlescale*s(i).vert_pred(timeindex),0,'b'),hold on
        quiver3(s(i).loc_east-epiloc_east,s(i).loc_north-epiloc_north,0,circlescale*s(i).east(timeindex),circlescale*s(i).north(timeindex),circlescale*s(i).vert(timeindex),0,'g')
        text(s(i).loc_east-epiloc_east,s(i).loc_north-epiloc_north,0,s(i).station)
    end
%     dispvec = [HVO_okadastruct.ss(timeindex); HVO_okadastruct.ds(timeindex); HVO_okadastruct.op(timeindex)];
%     rot_to_vert = [1,0,0; 0,cosd(-HVO_okadastruct.min_param.dip),sind(-HVO_okadastruct.min_param.dip); 0,-sind(-HVO_okadastruct.min_param.dip),cosd(-HVO_okadastruct.min_param.dip)];
%     rot_to_EN = [sind(HVO_okadastruct.min_param.strike),-cosd(HVO_okadastruct.min_param.strike),0; cosd(HVO_okadastruct.min_param.strike),sind(HVO_okadastruct.min_param.strike),0; 0,0,1];
%     dispvecrot = rot_to_vert*dispvec;
%     dispvecrot = rot_to_EN*dispvecrot;
%     dispeast = dispvecrot(1);
%     dispnorth = dispvecrot(2);
%     dispvert = dispvecrot(3);
%     circlescale = circlescale/2;
%     quiver3(minparam.centroideast-epiloc_east,minparam.centroidnorth-epiloc_north,-minparam.centroiddepth,circlescale*dispeast,circlescale*dispnorth,circlescale*dispvert,0,'k')
%     quiver3(minparam.centroideast-epiloc_east,minparam.centroidnorth-epiloc_north,-minparam.centroiddepth,-circlescale*dispeast,-circlescale*dispnorth,-circlescale*dispvert,0,':k')
%     circlescale = circlescale*2;
%     %legend('predicted amplitude','measured amplitude')
%     topeast = minparam.eastoffset + cosd(-minparam.strike)*minparam.width/2*cosd(minparam.dip); %top edge center if dip is positive
%     topnorth = minparam.northoffset + sind(-minparam.strike)*minparam.width/2*cosd(minparam.dip); %top edge center if dip is positive
%     crackeast = [topeast+minparam.len/2*sind(minparam.strike), topeast+minparam.len/2*sind(minparam.strike)-cosd(minparam.strike)*minparam.width*cosd(-minparam.dip), topeast+minparam.len/2*sind(minparam.strike+180)-cosd(minparam.strike)*minparam.width*cosd(-minparam.dip), topeast+minparam.len/2*sind(minparam.strike+180), topeast+minparam.len/2*sind(minparam.strike)];
%     cracknorth = [topnorth+minparam.len/2*cosd(minparam.strike), topnorth+minparam.len/2*cosd(minparam.strike)+sind(minparam.strike)*minparam.width*cosd(-minparam.dip), topnorth+minparam.len/2*cosd(minparam.strike+180)+sind(minparam.strike)*minparam.width*cosd(-minparam.dip), topnorth+minparam.len/2*cosd(minparam.strike+180), topnorth+minparam.len/2*cosd(minparam.strike)];
%     crackvert = -[minparam.depth, minparam.depth+minparam.width*sind(-minparam.dip), minparam.depth+minparam.width*sind(-minparam.dip), minparam.depth, minparam.depth];
%     fill3(crackeast,cracknorth,crackvert,'r'),hold on
%     fill3(crackeast,cracknorth,[0,0,0,0,0]*-1,'r','FaceAlpha','0')
    domainsize = 3000;
    fill3([domainsize,-domainsize,-domainsize,domainsize,domainsize],[domainsize,domainsize,-domainsize,-domainsize,domainsize],[0,0,0,0,0],'k','FaceAlpha',.25)
    axis equal
    xlim([-domainsize,domainsize]);
    ylim([-domainsize,domainsize]);
    zlim([-3000,1000]);
    grid on
    xlabel('E (m)')
    ylabel('N (m)')
    zlabel('depth (m)')
    drawnow
    ground_disp_anim(timeindex) = getframe(gcf);
end
fig = figure('Name','grounddispanimation','Position',[10,10,1000,800]);
movie(fig,ground_disp_anim)

myVideo = VideoWriter('groundispanim.avi');
open(myVideo);
writeVideo(myVideo, ground_disp_anim);
close(myVideo);
emd
