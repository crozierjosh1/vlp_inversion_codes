function DEM = import_Kilauea_DEM()
    %%%%import dem
    DEM = struct();
    [DEM.Z,DEM.x,DEM.y] = import_hawaii_dem();
    
    %% make dimensions even
    if mod(length(DEM.x),2) == 1
        DEM.x = DEM.x(1:end-1);
        DEM.Z = DEM.Z(:,1:end-1);
    end
    if mod(length(DEM.y),2) == 1
        DEM.y = DEM.y(1:end-1);
        DEM.Z = DEM.Z(1:end-1,:);
    end
    
    %% make square
    if length(DEM.x)>length(DEM.y)
        DEM.x = DEM.x(1:length(DEM.y));
        DEM.Z = DEM.Z(:,1:length(DEM.y));
    elseif length(DEM.x)<length(DEM.y)
        DEM.y = DEM.y(1:length(DEM.x));
        DEM.Z = DEM.Z(1:length(DEM.x),:);
    end
    
    %%
%     'gaussian test dem'
%     [xg,yg] = meshgrid(DEM.x,DEM.y);
%     DEM.Z = 1500*exp(-((xg-epiloc_east).^2 + (yg-epiloc_north).^2)/(1500)^2);

    %set ref elevation to inner crater floor (similar to demean) 
%     'test no demean'
    DEM.Z=DEM.Z-1030;%mean(DEM.Z(:)); 
    
    %% taper
    datasize = size(DEM.Z);
    taper = ones(datasize);
    %taperlength = floor(max(datasize)*(.5 - 2^.5/12)); %set so that taper is longer than diagonal length of untouched area, simple geometric problem
    taperlength = floor(min(datasize)/3);
    taper(taperlength+1:end-taperlength, 1:taperlength) = repmat(sin((1:taperlength)*pi/(2*taperlength)), datasize(1)-2*taperlength, 1);
    taper(taperlength+1:end-taperlength, end+1-taperlength:end) = repmat(cos((1:taperlength)*pi/(2*taperlength)), datasize(1)-2*taperlength, 1);
    taper(1:taperlength, taperlength+1:end-taperlength) = repmat(sin((1:taperlength)*pi/(2*taperlength)).', 1, datasize(2)-2*taperlength);
    taper(end+1-taperlength:end, taperlength+1:end-taperlength) = repmat(cos((1:taperlength)*pi/(2*taperlength)).', 1, datasize(2)-2*taperlength);
    cornerxdist = repmat(1:taperlength, taperlength,1);
    cornerydist = repmat((1:taperlength).', 1, taperlength);
    corner = (cornerxdist.^2 + cornerydist.^2).^.5;
    corner(corner > taperlength) = taperlength;
    corner = cos(corner*pi/(2*taperlength));
    taper(end+1-taperlength:end, end+1-taperlength:end) = corner;
    taper(end+1-taperlength:end, 1:taperlength) = fliplr(corner);
    taper(1:taperlength, end+1-taperlength:end) = flipud(corner);
    taper(1:taperlength, 1:taperlength) = rot90(corner,2);
    DEM.Z = DEM.Z.*taper;
    
    %% filter and interpolate
    %topo correction artifacts at shallow sources worsen with low res, 
    %and station locations become less inaccurate at worse
    %recommend 50m res or better, and divisor of 1 or less in gaussfilt sigma
    %these params wor for mogi sources at depths > ~100m
    interpres = 40;
    DEM.Z = imgaussfilt(DEM.Z, interpres/(DEM.x(2)-DEM.x(1))/1);
    [xg,yg] = meshgrid(DEM.x,DEM.y);
    DEM.x = min(DEM.x):interpres:max(DEM.x);
    DEM.y = min(DEM.y):interpres:max(DEM.y);
    [ixg,iyg] = meshgrid(DEM.x,DEM.y);
    DEM.Z = interp2(xg,yg,DEM.Z,ixg,iyg,'cubic'); 
end

function [Z,Zx,Zy] = import_hawaii_dem()%filename, domain)
    %imports hawaii dem
        %inputs
    %filename = name of geotiff dem file, string
    %domain = section of dem to import, vector
        %outputs
    %Z = ice surface elevation dem, matrix
    %Zx,Zy = x,y coordinates of Z
    filename = 'grdn20w156_13_ProjectRaster1.tif';
    domain = [-inf,inf,-inf,inf];




    [Z, R1] = geotiffread(filename);
    Zsize = size(Z);
    [Zx,~] = intrinsicToWorld(R1,1:Zsize(2),ones(1,Zsize(2)));
    [~,Zy] = intrinsicToWorld(R1,ones(1,Zsize(1)),1:Zsize(1));
    Zxi = Zx<=domain(2) & Zx>=domain(1);
    Zyi = Zy<=domain(4) & Zy>=domain(3);
    Z = Z(Zyi,Zxi);
    Zx = Zx(Zxi).';
    Zy = Zy(Zyi).';
    if Zx(2)<Zx(1)
        Zx = flipud(Zx);
        Z = fliplr(Z);
    end
    if Zy(2)<Zy(1)
        Zy = flipud(Zy);
        Z = flipud(Z);
    end
end

