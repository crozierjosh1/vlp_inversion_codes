function plot_ground_displacement_particlemotions(s,stationind,showcorr)
% animate GPS data
% josh crozier 2020


starttime = s(1).data.Time(1);%datetime
endtime = s(1).data.Time(end);%datetime

tplotrange = [starttime,endtime];

%% figure
fighandle = figure('Name','Ground Displacements');
waveformaxishandles = gobjects(1);
%cmap = cmocean('thermal',64);
% cmap = hsv(100);
% cmap = [cmap;cmap];
% saturation = linspace(-1,1,size(cmap,1))';
% saturation = 0.4+0.6*(1./(1 + exp(-saturation*10)));
% cmap = cmap.*repmat(saturation,1,3);
cmap = parula;
colormap(cmap)
fighandle.Units = 'pixels';
screensize = get(0,'screensize');
fighandle.OuterPosition = [screensize(3)/2,1,screensize(3)/2,screensize(4)];


%% plot waveforms
% plotind = s(stationind).data.Time > tplotrange(1)...
%     & s(stationind).data.Time < tplotrange(2);
% waveformaxishandles(1) = subplot(5,1,1);
% if ~showcorr
%     plot(s(stationind).data.Time, s(stationind).data.vert,'k');
%     hold on
%     scatter(s(stationind).data.Time, s(stationind).data.vert,9,datenum(s(stationind).data.Time),'filled');
% else
% %     scatter(s(stationind).data.Time, s(stationind).data.vert,9,datenum(s(stationind).data.Time),'filled');
%     hold on
% %     scatter(s(stationind).data.Time, s(stationind).data.vertcorr,9,datenum(s(stationind).data.Time),'filled');
%     plot(s(stationind).data.Time, s(stationind).data.vert,'r');
%     hold on
%     plot(s(stationind).data.Time, s(stationind).data.vertcorr,'b');
%     legend({strcat(s(stationind).station, ' uncorrected'),...
%         strcat(s(stationind).station, ' correctet for south flank')})
% end
% caxis(datenum(tplotrange))
% hold on
% %xlabel('Time (UTC)')
% % title(strcat('a.',{' '},s(stationind).station))
% ylabel('vertical disp (m)')
% xlim(tplotrange)
% if ~showcorr
%     ylim([min(s(stationind).data.vert(plotind)),...
%         max(s(stationind).data.vert(plotind))])
% else
%     ylim([min([min(s(stationind).data.vert(plotind)),min(s(stationind).data.vertcorr(plotind))]),...
%         max([max(s(stationind).data.vert(plotind)),max(s(stationind).data.vertcorr(plotind))])])
% end
% grid on
% box on
% set(gca,'FontSize',11)


%% DEM
%import dem
DEM = struct();
[DEM.Z,DEM.x,DEM.y] = import_hawaii_dem();
domain = [255.0,266.0,2137.2,2151.1]*1000;
DEM.Z = DEM.Z(:,DEM.x>=domain(1)&DEM.x<=domain(2));
DEM.x = DEM.x(DEM.x>=domain(1)&DEM.x<=domain(2));
DEM.Z = DEM.Z(DEM.y>=domain(3)&DEM.y<=domain(4),:);
DEM.y = DEM.y(DEM.y>=domain(3)&DEM.y<=domain(4));


%downsample
DEM.Z = imgaussfilt(DEM.Z,2);
DEM.downsamp = floor(length(DEM.x)/100);
DEM.x = DEM.x(1:DEM.downsamp:end);
DEM.y = DEM.y(1:DEM.downsamp:end);
DEM.Z = DEM.Z(1:DEM.downsamp:end,1:DEM.downsamp:end);

% disprange = 2*[-max(abs(s(stationind).data.vert)),max(abs(s(stationind).data.vert))];
quivscale = 0.18*range(DEM.x)/1000/...
    max([max(abs(s(stationind).data.east)),...
    max(abs(s(stationind).data.north)),...
    max(abs(s(stationind).data.vert))]);


epilatlon = [19.405402 -155.281041 100.0];
zone = utmzone(epilatlon(1),epilatlon(2));
utmstruct = defaultm('utm');
utmstruct.zone = zone;
utmstruct.geoid = wgs84Ellipsoid;
utmstruct = defaultm(utmstruct);
[epiloc_east, epiloc_north, epiloc_vert] = mfwdtran(utmstruct,epilatlon(1),epilatlon(2),epilatlon(3));

    
%% plot ground disp
dispgrid_ax = subplot(3,1,[1,2]);
hold on

axes(dispgrid_ax)
plot([0,0],[0,0]),hold on
xlabel('east (km)')
ylabel('north (km)')
grid on
box on
axis equal
xlim([min((DEM.x-epiloc_east))/1000,max((DEM.x-epiloc_east))/1000])
ylim([min((DEM.y-epiloc_north))/1000,max((DEM.y-epiloc_north))/1000])
set(gca,'ydir','normal')
contour((DEM.x-epiloc_east)/1000,(DEM.y-epiloc_north)/1000,DEM.Z,0:20:3000,'LineColor',[0.7,0.7,0.7])
caxis(datenum(tplotrange))

for jj = 1:length(s)
    if ~showcorr
        p1 = scatter(dispgrid_ax,...
            (s(jj).loc_east-epiloc_east)/1000 + s(jj).data.east*quivscale,...
            (s(jj).loc_north-epiloc_north)/1000 + s(jj).data.north*quivscale,...
            3, datenum(s(jj).data.Time), 'filled');
    else
%         scatter(dispgrid_ax,...
%             (s(jj).loc_east-epiloc_east)/1000 + s(jj).data.east*quivscale,...
%             (s(jj).loc_north-epiloc_north)/1000 + s(jj).data.north*quivscale,...
%             6, datenum(s(jj).data.Time), 'filled')
%         scatter(dispgrid_ax,...
%             (s(jj).loc_east-epiloc_east)/1000 + s(jj).data.eastcorr*quivscale,...
%             (s(jj).loc_north-epiloc_north)/1000 + s(jj).data.northcorr*quivscale,...
%             6, datenum(s(jj).data.Time), 'filled')
        p1 = plot(dispgrid_ax,...
            (s(jj).loc_east-epiloc_east)/1000 + s(jj).data.eastuncorr*quivscale,...
            (s(jj).loc_north-epiloc_north)/1000 + s(jj).data.northuncorr*quivscale,'Color','r','linewidth',1);
        p2 = plot(dispgrid_ax,...
            (s(jj).loc_east-epiloc_east)/1000 + s(jj).data.east*quivscale,...
            (s(jj).loc_north-epiloc_north)/1000 + s(jj).data.north*quivscale,'Color','b','linewidth',1);
    end
    scatter((s(jj).loc_east-epiloc_east)/1000,(s(jj).loc_north-epiloc_north)/1000,20,...
        'k','+','LineWidth',1.5) 
    text((s(jj).loc_east-epiloc_east)/1000+0.14,(s(jj).loc_north-epiloc_north)/1000,...
        s(jj).station,'Color','k','FontWeight','bold')
end

quiver(-5,-9.65,1*quivscale,0,'k','AutoScale','off','LineWidth',2)
text(-5,-9.25,'1 m horizontal displacement')

%
in = true(size(s(1).data.east));
for ii = 1:length(s)
    in = in & isfinite(s(ii).data.east);
    in = in & isfinite(s(ii).data.north);
    in = in & isfinite(s(ii).data.vert);
end
startin = find(in,1);
endin = find(in,1,'last');
for ii = 1:length(s)
    if ~showcorr
        starte = (s(ii).loc_east-epiloc_east)/1000 + s(ii).data.east(startin)*quivscale;
        ende = (s(ii).loc_east-epiloc_east)/1000 + s(ii).data.east(endin)*quivscale;
        startn = (s(ii).loc_east-epiloc_north)/1000 + s(ii).data.north(startin)*quivscale;
        endn = (s(ii).loc_east-epiloc_north)/1000 + s(ii).data.north(endin)*quivscale;
%         plot([starte,ende],[startn,endn],'k')
        plot(ende,endn,'ko','LineWidth',1.5)
    else
        starte = (s(ii).loc_east-epiloc_east)/1000 + s(ii).data.eastuncorr(startin)*quivscale;
        ende = (s(ii).loc_east-epiloc_east)/1000 + s(ii).data.eastuncorr(endin)*quivscale;
        startn = (s(ii).loc_north-epiloc_north)/1000 + s(ii).data.northuncorr(startin)*quivscale;
        endn = (s(ii).loc_north-epiloc_north)/1000 + s(ii).data.northuncorr(endin)*quivscale;
%         plot([starte,ende],[startn,endn],'r')
        plot(ende,endn,'ro','LineWidth',1)
        starte = (s(ii).loc_east-epiloc_east)/1000 + s(ii).data.east(startin)*quivscale;
        ende = (s(ii).loc_east-epiloc_east)/1000 + s(ii).data.east(endin)*quivscale;
        startn = (s(ii).loc_north-epiloc_north)/1000 + s(ii).data.north(startin)*quivscale;
        endn = (s(ii).loc_north-epiloc_north)/1000 + s(ii).data.north(endin)*quivscale;
%         plot([starte,ende],[startn,endn],'b')
        plot(ende,endn,'bo','LineWidth',1)
    end
end
%}

% dispgrid_quiv = quiver(dispgrid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
%     Ux*quivscale,Uy*quivscale,...
%     'Color',[0,0,0],'AutoScale','off','LineWidth',1);
% dispgrid_zquiv = quiver(dispgrid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
%     0*Uz,Uz*quivscale,...
%     'Color',[1,0,0],'AutoScale','off','LineWidth',1);
% title(dispgrid_ax,datestr(curdatetime,1))

source.latlon_ref = [19.4073, -155.2784]; %reference lat-lon from Anderson 2019
[source.east_ref, source.north_ref,~] = ...
    mfwdtran(utmstruct,source.latlon_ref(1),source.latlon_ref(2),0);
source.east = source.east_ref + 70.3;
source.north = source.north_ref + 183;
plot((source.east-epiloc_east)/1000,(source.north-epiloc_north)/1000,'+',...
    'Color','g','Markersize',9,'LineWidth',2)
text((source.east-epiloc_east)/1000+0.5,(source.north-epiloc_north)/1000,'HMM','Color','g','FontWeight','bold')

[SCR_source.east, SCR_source.north,~] = ...
        mfwdtran(utmstruct,19.39,-155.27,0);
plot((SCR_source.east-epiloc_east)/1000,(SCR_source.north-epiloc_north)/1000,'+',...
    'Color','m','Markersize',9,'LineWidth',2)
text((SCR_source.east-epiloc_east)/1000+0.5,(SCR_source.north-epiloc_north)/1000,'SCR','Color','m','FontWeight','bold')

[int_source.east, int_source.north,~] = ...
        mfwdtran(utmstruct,19.3857,-155.287,0);
plot((int_source.east-epiloc_east)/1000,(int_source.north-epiloc_north)/1000,'+',...
    'Color','c','Markersize',9,'LineWidth',2)
text((int_source.east-epiloc_east)/1000+0.5,(int_source.north-epiloc_north)/1000,'2015','Color','c','FontWeight','bold')

set(gca,'FontSize',11)

legend([p1,p2],{'GPS','south flank\newlinecorrected GPS'})
% title('b. map')
end

