function animate_ground_displacements(s,stationind)
% animate GPS data
% josh crozier 2020

timestep = days(1);
%'!!!wrong starttime'
% starttime = 317;%seconds
% endtime = 322;%660;
starttime = s(1).datetime(1);%datetime
endtime = s(1).datetime(end);%datetime
videosave = true;%make video

tplotrange = [starttime,endtime];

%% figure
fighandle = figure('Name','Ground Displacements');
waveformaxishandles = gobjects(1);
%cmap = cmocean('thermal',64);
cmap = hsv;
cmap = cmap(1:floor(0.92*size(cmap,1)),:);
colormap(cmap)
fighandle.Units = 'pixels';
screensize = get(0,'screensize');
if ~ videosave
    fighandle.OuterPosition = [screensize(3)/2,1,screensize(3)/2,screensize(4)];
else
    %fighandle.OuterPosition = [0,0,2*screensize(3)/3,screensize(4)];
    fighandle.OuterPosition = [0,0,1300,700];
end

%% plot disp waveforms
plotind = s(stationind).datetime > tplotrange(1)...
    & s(stationind).datetime < tplotrange(2);

waveformaxishandles(1) = subplot('position',[0.05,0.8,0.28,0.15]);
plot(s(stationind).datetime, s(stationind).vert,'k'),
hold on
scatter(s(stationind).datetime, s(stationind).vert,1,datenum(s(stationind).datetime),'filled');
%xlabel('Time (UTC)')
title(s(stationind).station)
ylabel('Vert Disp (m)')
xlim(tplotrange)
% ylim([min(s(stationind).vert(plotind)),...
%     max(s(stationind).vert(plotind))])
ylim([-0.12,...
    max(s(stationind).vert(plotind))])
grid on
box on
caxis(datenum(tplotrange))
colormap(cmap)


%% plot vel waveforms
%derivative filter
Tinterp = days(1);
dfilt7 = designfilt('differentiatorfir', ... % Response type
   'FilterOrder',36, ...            % Filter order
   'PassbandFrequency',1/30, ...     % Frequency constraints
   'StopbandFrequency',1/7, ...
   'PassbandWeight',10, ...          % Design method options
   'StopbandWeight',1, ...
   'SampleRate',1/days(Tinterp)); 
%fvtool(dfilt7,'MagnitudeDisplay','zero-phase','Fs',1/days(Tinterp))
delay7 = mean(grpdelay(dfilt7));
dfilt30 = designfilt('differentiatorfir', ... % Response type
   'FilterOrder',120, ...            % Filter order
   'PassbandFrequency',1/(30*12), ...     % Frequency constraints
   'StopbandFrequency',1/30, ...
   'PassbandWeight',10, ...          % Design method options
   'StopbandWeight',1, ...
   'SampleRate',1/days(Tinterp));
%fvtool(dfilt30,'MagnitudeDisplay','zero-phase','Fs',1/days(Tinterp))
delay30 = mean(grpdelay(dfilt30));
padlen = floor(length(s(1).datetime)/4);
for jj = 1:length(s)
    data = fillmissing(s(jj).east,'nearest');
    data = [zeros(padlen,1) + data(1); data; zeros(padlen,1) + data(end)];
    data = filter(dfilt7,data)/days(Tinterp);
    data = [data(delay7+1:end);zeros(delay7,1)];
    s(jj).east_vel7 = data(padlen+1:end-padlen);
%     s(jj).east_vel7 = [0;diff(s(jj).east)];
    
    data = fillmissing(s(jj).east,'nearest');
    data = [zeros(padlen,1) + data(1); data; zeros(padlen,1) + data(end)];
    data = filter(dfilt30,data)/days(Tinterp);
    data = [data(delay30+1:end);zeros(delay30,1)];
    s(jj).east_vel30 = data(padlen+1:end-padlen);
    
    data = fillmissing(s(jj).north,'nearest');
    data = [zeros(padlen,1) + data(1); data; zeros(padlen,1) + data(end)];
    data = filter(dfilt7,data)/days(Tinterp);
    data = [data(delay7+1:end);zeros(delay7,1)];
    s(jj).north_vel7 = data(padlen+1:end-padlen);
%     s(jj).north_vel7 = [0;diff(s(jj).north)];
    
    data = fillmissing(s(jj).north,'nearest');
    data = [zeros(padlen,1) + data(1); data; zeros(padlen,1) + data(end)];
    data = filter(dfilt30,data)/days(Tinterp);
    data = [data(delay30+1:end);zeros(delay30,1)];
    s(jj).north_vel30 = data(padlen+1:end-padlen);
    
    data = fillmissing(s(jj).vert,'nearest');
    data = [zeros(padlen,1) + data(1); data; zeros(padlen,1) + data(end)];
    data = filter(dfilt7,data)/days(Tinterp);
    data = [data(delay7+1:end);zeros(delay7,1)];
    s(jj).vert_vel7 = data(padlen+1:end-padlen);
%     s(jj).vert_vel7 = [0;diff(s(jj).vert)];

    data = fillmissing(s(jj).vert,'nearest');
    data = [zeros(padlen,1) + data(1); data; zeros(padlen,1) + data(end)];
    data = filter(dfilt30,data)/days(Tinterp);
    data = [data(delay30+1:end);zeros(delay30,1)];
    s(jj).vert_vel30 = data(padlen+1:end-padlen);
end

waveformaxishandles(2) = subplot('position',[0.36,0.8,0.28,0.15]);
plot(s(stationind).datetime, s(stationind).vert_vel7,'k'),
hold on
scatter(s(stationind).datetime, s(stationind).vert_vel7,1,datenum(s(stationind).datetime),'filled');
%xlabel('Time (UTC)')
title(strcat(s(stationind).station,' 7-day lowpass'))
ylabel('Vert Vel (m/day)')
xlim(tplotrange)
% ylim([min(s(stationind).vert_vel7(plotind)),...
%     max(s(stationind).vert_vel7(plotind))])
ylim([-0.005,0.005])
grid on
box on
caxis(datenum(tplotrange))
colormap(cmap)

waveformaxishandles(3) = subplot('position',[0.67,0.8,0.28,0.15]);
plot(s(stationind).datetime, s(stationind).vert_vel30,'k'),
hold on
scatter(s(stationind).datetime, s(stationind).vert_vel30,1,datenum(s(stationind).datetime),'filled');
%xlabel('Time (UTC)')
title(strcat(s(stationind).station,' 30-day lowpass'))
ylabel('Vert Vel (m/day)')
xlim(tplotrange)
% ylim([min(s(stationind).vert_vel30(plotind)),...
%     max(s(stationind).vert_vel30(plotind))])
ylim([-0.001,0.001])
grid on
box on
caxis(datenum(tplotrange))
colormap(cmap)


%% DEM
%import dem
DEM = struct();
[DEM.Z,DEM.x,DEM.y] = import_hawaii_dem();
domain = [256.5,264,2142,2150.5]*1000;
DEM.Z = DEM.Z(:,DEM.x>=domain(1)&DEM.x<=domain(2));
DEM.x = DEM.x(DEM.x>=domain(1)&DEM.x<=domain(2));
DEM.Z = DEM.Z(DEM.y>=domain(3)&DEM.y<=domain(4),:);
DEM.y = DEM.y(DEM.y>=domain(3)&DEM.y<=domain(4));


%downsample
DEM.Z = imgaussfilt(DEM.Z,2);
DEM.downsamp = floor(length(DEM.x)/100);
DEM.x = DEM.x(1:DEM.downsamp:end);
DEM.y = DEM.y(1:DEM.downsamp:end);
DEM.Z = DEM.Z(1:DEM.downsamp:end,1:DEM.downsamp:end);

epilatlon = [19.405402 -155.281041 100.0];
zone = utmzone(epilatlon(1),epilatlon(2));
utmstruct = defaultm('utm');
utmstruct.zone = zone;
utmstruct.geoid = wgs84Ellipsoid;
utmstruct = defaultm(utmstruct);
[epiloc_east, epiloc_north, epiloc_vert] = mfwdtran(utmstruct,epilatlon(1),epilatlon(2),epilatlon(3));


%% scaling
% disprange = 2*[-max(abs(s(stationind).vert)),max(abs(s(stationind).vert))];
quivscale = 0.18*range(DEM.x)/1000/...
    max([max(abs(s(stationind).east)),...
    max(abs(s(stationind).north)),...
    max(abs(s(stationind).vert))]);
% vel7quivscale = 1.8*range(DEM.x)/1000/...
%     max([max(abs(s(stationind).east_vel7)),...
%     max(abs(s(stationind).north_vel7)),...
%     max(abs(s(stationind).vert_vel7))]);
vel7quivscale = 0.25*range(DEM.x)/1000/...
    max([max(abs(s(stationind).east_vel7)),...
    max(abs(s(stationind).north_vel7)),...
    max(abs(s(stationind).vert_vel7))]);
vel30quivscale = 0.6*range(DEM.x)/1000/...
    max([max(abs(s(stationind).east_vel30)),...
    max(abs(s(stationind).north_vel30)),...
    max(abs(s(stationind).vert_vel30))]);



%% loop for animations

datetimes = starttime:timestep:endtime;
if videosave
    videoanim(length(datetimes))...
        = struct('cdata',[],'colormap',[]);
end
for tin = 1:length(datetimes)
    curdatetime = datetimes(tin);
    cur_ind = find(s(1).datetime>=curdatetime,1);
    
    %% ground disp
    if tin == 1
        dispgrid_ax = subplot('position',[0.05,0.05,0.28,0.7]);
        stationx = zeros(size(s));
        stationy = zeros(size(s));
        stationname = cell(size(s));
        for j = 1:length(s)
            stationx(j) = s(j).loc_east;
            stationy(j) = s(j).loc_north;
            stationname{j} = s(j).station;
        end
        Ux = zeros(size(s));
        Uy = zeros(size(s));
        Uz = zeros(size(s));
        hold on
    else
        delete(dispgrid_quiv)
        delete(dispgrid_zquiv)
    end
    for j = 1:length(s)
        Uz(j) = s(j).vert(cur_ind);
        Ux(j) = s(j).east(cur_ind);
        Uy(j) = s(j).north(cur_ind);
    end
    
    if tin == 1
        axes(dispgrid_ax)
        plot([0,0],[0,0]),hold on
        xlabel('East (km)')
        ylabel('North (km)')
        grid on
        box on
        axis equal
        xlim([min((DEM.x-epiloc_east))/1000,max((DEM.x-epiloc_east))/1000])
        ylim([min((DEM.y-epiloc_north))/1000,max((DEM.y-epiloc_north))/1000])
        set(gca,'ydir','normal')
        contour((DEM.x-epiloc_east)/1000,(DEM.y-epiloc_north)/1000,DEM.Z,0:20:3000,'LineColor',[0.5,0.5,0.5])
        scatter((stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,4,[0,0,0],'filled') 
        text((stationx-epiloc_east)/1000+0.14,(stationy-epiloc_north)/1000,...
            stationname,'Color','k','FontWeight','bold')
        for jj = 1:length(s)
            scatter(dispgrid_ax,...
                (s(jj).loc_east-epiloc_east)/1000 + s(jj).east*quivscale,...
                (s(jj).loc_north-epiloc_north)/1000 + s(jj).north*quivscale,...
                1, datenum(s(jj).datetime), 'filled')
            scatter((s(jj).loc_east-epiloc_east)/1000,(s(jj).loc_north-epiloc_north)/1000,4,[0,0,0],'.') 
        end
        colormap(dispgrid_ax,cmap)
        caxis(dispgrid_ax,datenum(tplotrange))
    end
    dispgrid_quiv = quiver(dispgrid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
        Ux*quivscale,Uy*quivscale,...
        'Color',[0,0,0],'AutoScale','off','LineWidth',1.5);
    dispgrid_zquiv = quiver(dispgrid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
        0*Uz,Uz*quivscale,...
        'Color',[0.4,0.2,0],'AutoScale','off','LineWidth',1.5);
    title(dispgrid_ax,strcat('Disp',{' '},datestr(curdatetime,1)))
    
    
    %% 7-day ground vel
    if tin == 1
        vel7grid_ax = subplot('position',[0.36,0.05,0.28,0.7]);
        d7Ux = zeros(size(s));
        d7Uy = zeros(size(s));
        d7Uz = zeros(size(s));
        hold on
    else
        delete(vel7grid_quiv)
        delete(vel7grid_zquiv)
    end
    for j = 1:length(s)
        d7Uz(j) = s(j).vert_vel7(cur_ind);
        d7Ux(j) = s(j).east_vel7(cur_ind);
        d7Uy(j) = s(j).north_vel7(cur_ind);
    end
    
    if tin == 1
        axes(vel7grid_ax)
        plot([0,0],[0,0]),hold on
        xlabel('East (km)')
        ylabel('North (km)')
        grid on
        box on
        axis equal
        xlim([min((DEM.x-epiloc_east))/1000,max((DEM.x-epiloc_east))/1000])
        ylim([min((DEM.y-epiloc_north))/1000,max((DEM.y-epiloc_north))/1000])
        set(gca,'ydir','normal')
        contour((DEM.x-epiloc_east)/1000,(DEM.y-epiloc_north)/1000,DEM.Z,0:20:3000,'LineColor',[0.5,0.5,0.5])
        scatter((stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,4,[0,0,0],'filled') 
        text((stationx-epiloc_east)/1000+0.14,(stationy-epiloc_north)/1000,...
            stationname,'Color','k','FontWeight','bold')
        for jj = 1:length(s)
            scatter(vel7grid_ax,...
                (s(jj).loc_east-epiloc_east)/1000 + s(jj).east_vel7*vel7quivscale,...
                (s(jj).loc_north-epiloc_north)/1000 + s(jj).north_vel7*vel7quivscale,...
                1, datenum(s(jj).datetime), 'filled')
            scatter((s(jj).loc_east-epiloc_east)/1000,(s(jj).loc_north-epiloc_north)/1000,4,[0,0,0],'.') 
        end
        colormap(vel7grid_ax,cmap)
        caxis(vel7grid_ax,datenum(tplotrange))
    end
    vel7grid_quiv = quiver(vel7grid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
        d7Ux*vel7quivscale,d7Uy*vel7quivscale,...
        'Color',[0,0,0],'AutoScale','off','LineWidth',1.5);
    vel7grid_zquiv = quiver(vel7grid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
        0*d7Uz,d7Uz*vel7quivscale,...
        'Color',[0.4,0.2,0],'AutoScale','off','LineWidth',1.5);
    title(vel7grid_ax,strcat('7-day lowpass Vel',{' '},datestr(curdatetime,1)))
    
    
    %% 30-day ground vel
    if tin == 1
        vel30grid_ax = subplot('position',[0.67,0.05,0.28,0.7]);
        d30Ux = zeros(size(s));
        d30Uy = zeros(size(s));
        d30Uz = zeros(size(s));
        hold on
    else
        delete(vel30grid_quiv)
        delete(vel30grid_zquiv)
    end
    for j = 1:length(s)
        d30Uz(j) = s(j).vert_vel30(cur_ind);
        d30Ux(j) = s(j).east_vel30(cur_ind);
        d30Uy(j) = s(j).north_vel30(cur_ind);
    end
    
    if tin == 1
        axes(vel30grid_ax)
        plot([0,0],[0,0]),hold on
        xlabel('East (km)')
        ylabel('North (km)')
        grid on
        box on
        axis equal
        xlim([min((DEM.x-epiloc_east))/1000,max((DEM.x-epiloc_east))/1000])
        ylim([min((DEM.y-epiloc_north))/1000,max((DEM.y-epiloc_north))/1000])
        set(gca,'ydir','normal')
        contour((DEM.x-epiloc_east)/1000,(DEM.y-epiloc_north)/1000,DEM.Z,0:20:3000,'LineColor',[0.5,0.5,0.5])
        scatter((stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,4,[0,0,0],'filled') 
        text((stationx-epiloc_east)/1000+0.14,(stationy-epiloc_north)/1000,...
            stationname,'Color','k','FontWeight','bold')
        for jj = 1:length(s)
            scatter(vel30grid_ax,...
                (s(jj).loc_east-epiloc_east)/1000 + s(jj).east_vel30*vel30quivscale,...
                (s(jj).loc_north-epiloc_north)/1000 + s(jj).north_vel30*vel30quivscale,...
                1, datenum(s(jj).datetime), 'filled')
            scatter((s(jj).loc_east-epiloc_east)/1000,(s(jj).loc_north-epiloc_north)/1000,4,[0,0,0],'.') 
        end
        colormap(vel30grid_ax,cmap)
        caxis(vel30grid_ax,datenum(tplotrange))
    end
    vel30grid_quiv = quiver(vel30grid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
        d30Ux*vel30quivscale,d30Uy*vel30quivscale,...
        'Color',[0,0,0],'AutoScale','off','LineWidth',1.5);
    vel30grid_zquiv = quiver(vel30grid_ax,(stationx-epiloc_east)/1000,(stationy-epiloc_north)/1000,...
        0*d30Uz,d30Uz*vel30quivscale,...
        'Color',[0.4,0.2,0],'AutoScale','off','LineWidth',1.5);
    title(vel30grid_ax,strcat('30-day lowpass Vel',{' '},datestr(curdatetime,1)))
    
    
    %% lines on waveforms
    figure(fighandle)
    if exist('waveformlinehandles','var')
        for k =1:length(waveformlinehandles)
            delete(waveformlinehandles(k))
        end
    end
    waveformlinehandles = gobjects(size(waveformaxishandles));
    for j = 1:length(waveformlinehandles)
        waveformlinehandles(j) = xline(waveformaxishandles(j),curdatetime,'k-');
    end
    
    
    
    drawnow
    if videosave
        videoanim(tin) = getframe(gcf);
    end
end

%% save
if videosave

    myVideo = VideoWriter(strcat('GPSanim_plusvel_2015intrusion.mp4'),'MPEG-4');
    myVideo.FrameRate = 20;
    myVideo.Quality = 75;
    open(myVideo);
    writeVideo(myVideo, videoanim);
    close(myVideo);
    
end

end

