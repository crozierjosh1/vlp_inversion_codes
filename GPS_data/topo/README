Date: Thu, 6 Feb 2003 17:05:36 -0900 (AKST)
From: Evelyn Price <evelyn@giseis.alaska.edu>
Reply-To: Evelyn Price <evelyn@giseis.alaska.edu>
Subject: Re: Fwd: Re: topo effects
To: segall@pangea.Stanford.EDU
Cc: evelyn@eq.giseis.alaska.edu

Content-Type: TEXT/plain; charset=us-ascii
Content-MD5: vItZWxbsZvgpCnn3aPOrLg==

Hi Paul,

Three files are attached.  The first "MakeGSAR_topo.m" is an (InSAR) example of 
how to call the functions that generate the topography-corrected displacements.  
First there is a call to "vol_stress" which computes the displacements, 
stresses, and strains at the surface of an elastic half-space due to a volume 
change in a spherical source and "topo_corr" implements the algorithm outlined 
in Williams and Wadge using the output of vol_stress and a DEM.  

The displacements are computed at every location of a DEM so if you want to 
invert pointwise geodetic measurements using the routines, you have to assume 
that each pointwise measurement is within a DEM cell and then mask the output of 
topo_corr with a matrix that is the same size as the DEM (see the last 4 lines 
of "MakeGSAR_topo.m").  If you want to do non-linear inversions, the routine can 
be speeded up by doing some computations, which don't have to be done hundreds 
of times, in the calling routine (e.g. computing the transfer functions).

If you have any questions, don't hesitate to ask.  Also, please let me know if 
anyone finds any bugs.

Cheers,
Evelyn

