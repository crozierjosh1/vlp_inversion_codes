	function [u_hs, u_topo_deriv, u_topo_conv, u_topo, u] ...
		= sphare_topo(dis_geom, xy_o, xy_h, h, ...
			topo_rows, topo_dx, topo_dy, nu, mu)

% Subroutine to make displacements for point sources
% August 18, 2002 --EJP (evelyn@giseis.alaska.edu)
% This treats range change data as absolute with respect to a reference
% level of zero deformation.  In practice the reference level is uncertain
% and the inversion should allow for a best-fitting reference level.
% Hence the appearance of 3*nv+1 parameters below.
% 
% Input:
%	vol_geom     - Point Source Geometry.Rows=[x(km) y(km) depth(km)
%                      dV(1e6 m^3)]. The very last element of vol_geom is the
%                      DEM offset.
%	xy	     - xy coordinates of InSAR measurements (km)
%	mat_xy_vals  - a vector whose elements are the localxy locations of
%                      the elements in d_DEM.
%	mat_nul_vals - a vector whose elements are at the same xy locations
%                      as those of d_DEM and the vector has Nans at the 
%                      locations of no SAR (or displacement) data.
%	d_DEM	     - a vector with DEM data in row-major order 
%                      (e.g. GMT -ZLTa)
%	topo_rows     - the number of rows in the DEM 
%	topo_dx       - the DEM resolution in the x direction (meters) 
%	topo_dy       - the DEM resolution in the y direction (meters) 
%	nu	     - Poisson's ratio
%	mu	     - Bulk Modulus (MPa)


    % Number of fault patches

	Nf = size(dis_geom,2);

    % Number of obs stations

	Nsta = size(xy_o,1);

    % Number of data 

	Ndata = Nsta; 

    % Number of height data

        Ntopo = length(h(:));

    % initialization of an array

	deform = zeros(Ntopo,10);

    % Calculation of 
    %   U: displacement at depth (incl. surface)
    %   D: gradient of U
    %   S: Stress
    % and output to a single variable "deform". 
    %
    %   Note that we need to divide D and S by 1000 because 
    %   we use [m] for slip and [km] for len/wid/dep.

	for i=1:Nf
	    [U, D, S, flag] = disloc3d(dis_geom(:,i), xy_o', mu, nu);
	    D = D/1000; S = S/1000;  % slip in [m], len/wid/dep in [km]
	    S11 = S(1,:); S22 = S(4,:); S12 = S(2,:);
	    E31 = D(7,:); E32 = D(8,:); E11 = D(1,:); E22 = D(5,:);
	    deform_f = [U; S11; S22; S12; E31; E32; E11; E22];
	    deform = deform + deform_f';
	end

    % u_hs: displacement due to fault slip.

	u_hs = deform(:,1:3);

    % calculate topo correction

	[u_topo_deriv, u_topo_conv, u_topo] ...
		= topo_corr(deform, h, topo_rows, ...
			topo_dx, topo_dy, nu, mu);

    % Look at results.

	u = u_hs + u_topo;


