
%	clear all
    path(path, '/matlab/disloc')
    path(path, '/deform/paul/matlab/matools')

    % material constant

	lambda = 1.9*10^10;  % [Pa]
	%mu = 1.45*10^10;  % [Pa]
	mu = lambda;  % [Pa]
	nu = lambda/2/(lambda+mu);

    % study area [km]: [-area, area] \times [area, area]
    % Npixel: the number of obs points
    % topo_rows: the number of column for xy_h (topography)

    area = 2000;
    Npixel = 250;
    topo_rows = 250;



% observation points: xy_o = [x, y, 0]

	x = linspace(-area,area,Npixel)';
	x0 = x*ones(1,Npixel);
	x1 = x0';
	x1 = x1(:);
	x2 = x0(:);
	zr = zeros(size(x1));
	xy_o = [x1, x2, zr];

% construct a topographic model
     Re = 6378.137;     %earth radius in km.

     topo_x = linspace(-area,area,topo_rows)';
     [tempx, tempy] = meshgrid(topo_x);
     xy_h = [tempx(:), tempy(:)];

     topo = -0.5*(tempx.^2 + tempy.^2)/Re;
         h = 1000*topo(:);   %%% COnvert topography to meters

	topo_dx = 1000*(topo_x(2)-topo_x(1));
	topo_dy = 1000*(topo_x(2)-topo_x(1));

    % Fault geometry
    %  - [len,wid,dep,dip,str,E,N,dislo_str,dislo_dip,open]
    %    depth is the lower edge

	%str_geom = [0.40,0.25,300,90,45,0,0,10000,0,0]'; % case 1
	dip_geom = [50,10,75,30,90,0,0,0,-10,0]'; % case 2
    dip_geom = [200,125,75,30,90,0,0,0, 10,0]'; % case 2
    
	%dik_geom = [0.40,0.25,300,90,45,0,0,0,0,10000]'; % case 3
	%sil_geom = [0.40,0.25,300,0,45,0,0,0,0,10000]'; % case 4
	% [U, D, S, flag] = disloc3d(dis_geom, xy_o', mu, nu);
	% Ux = reshape(U(1,:),Npixel,Npixel);
	% Uy = reshape(U(2,:),Npixel,Npixel);
	% Uz = reshape(U(3,:),Npixel,Npixel);
	% quiver(x,x,Ux,Uy)
	% axis([-100,100,-100,100])

    % topography correction

	%[str_hs, str_topo_deriv, str_topo_conv, str_topo, str] ...
	%	= sphere_topo2(str_geom, xy_o, xy_h, h, ...
	%		topo_rows, topo_dx, topo_dy, nu, mu);
	 [dip_hs, dip_topo_deriv, dip_topo_conv, dip_topo, dip] ...
	 	= sphere_topo2(dip_geom, xy_o, xy_h, h, ...
	 		topo_rows, topo_dx, topo_dy, nu, mu);
	%[dik_hs, dik_topo_deriv, dik_topo_conv, dik_topo, dik] ...
	%	= sphere_topo2(dik_geom, xy_o, xy_h, h, ...
	%		topo_rows, topo_dx, topo_dy, nu, mu);
	%[sil_hs, sil_topo_deriv, sil_topo_conv, sil_topo, sil] ...
	%	= sphere_topo2(sil_geom, xy_o, xy_h, h, ...
	%		topo_rows, topo_dx, topo_dy, nu, mu);

      % x-directoin

	%str_x_hs = reshape(str_hs(:,1),Npixel,Npixel);   % half-space
	%str_x_topo_deriv = reshape(str_topo_deriv(:,1),Npixel,Npixel); %perturb derivative term
	%str_x_topo_conv = reshape(str_topo_conv(:,1),Npixel,Npixel); %perturb convol. term
	%str_x_topo = reshape(str_topo(:,1),Npixel,Npixel); % perturb total correction
	%str_x = reshape(str(:,1),Npixel,Npixel);  % corretion plus half-space

	dip_x_hs = reshape(dip_hs(:,1),Npixel,Npixel);
	dip_x_topo_deriv = reshape(dip_topo_deriv(:,1),Npixel,Npixel);
	dip_x_topo_conv = reshape(dip_topo_conv(:,1),Npixel,Npixel);
	dip_x_topo = reshape(dip_topo(:,1),Npixel,Npixel);
	dip_x = reshape(dip(:,1),Npixel,Npixel);

	%dik_x_hs = reshape(dik_hs(:,1),Npixel,Npixel);
	%dik_x_topo_deriv = reshape(dik_topo_deriv(:,1),Npixel,Npixel);
	%dik_x_topo_conv = reshape(dik_topo_conv(:,1),Npixel,Npixel);
	%dik_x_topo = reshape(dik_topo(:,1),Npixel,Npixel);
	%dik_x = reshape(dik(:,1),Npixel,Npixel);

	%sil_x_hs = reshape(sil_hs(:,1),Npixel,Npixel);
	%sil_x_topo_deriv = reshape(sil_topo_deriv(:,1),Npixel,Npixel);
	%sil_x_topo_conv = reshape(sil_topo_conv(:,1),Npixel,Npixel);
	%sil_x_topo = reshape(sil_topo(:,1),Npixel,Npixel);
	%sil_x = reshape(sil(:,1),Npixel,Npixel);

      % y-directoin

	%str_y_hs = reshape(str_hs(:,2),Npixel,Npixel);
	%str_y_topo_deriv = reshape(str_topo_deriv(:,2),Npixel,Npixel);
	%str_y_topo_conv = reshape(str_topo_conv(:,2),Npixel,Npixel);
	%str_y_topo = reshape(str_topo(:,2),Npixel,Npixel);
	%str_y = reshape(str(:,2),Npixel,Npixel);

	dip_y_hs = reshape(dip_hs(:,2),Npixel,Npixel);
	dip_y_topo_deriv = reshape(dip_topo_deriv(:,2),Npixel,Npixel);
	dip_y_topo_conv = reshape(dip_topo_conv(:,2),Npixel,Npixel);
	dip_y_topo = reshape(dip_topo(:,2),Npixel,Npixel);
	dip_y = reshape(dip(:,2),Npixel,Npixel);

	%dik_y_hs = reshape(dik_hs(:,2),Npixel,Npixel);
	%dik_y_topo_deriv = reshape(dik_topo_deriv(:,2),Npixel,Npixel);
	%dik_y_topo_conv = reshape(dik_topo_conv(:,2),Npixel,Npixel);
	%dik_y_topo = reshape(dik_topo(:,2),Npixel,Npixel);
	%dik_y = reshape(dik(:,2),Npixel,Npixel);

	%sil_y_hs = reshape(sil_hs(:,2),Npixel,Npixel);
	%sil_y_topo_deriv = reshape(sil_topo_deriv(:,2),Npixel,Npixel);
	%sil_y_topo_conv = reshape(sil_topo_conv(:,2),Npixel,Npixel);
	%sil_y_topo = reshape(sil_topo(:,2),Npixel,Npixel);
	%sil_y = reshape(sil(:,2),Npixel,Npixel);

      % z-directoin

	%str_z_hs = reshape(str_hs(:,3),Npixel,Npixel);
	%str_z_topo_deriv = reshape(str_topo_deriv(:,3),Npixel,Npixel);
	%str_z_topo_conv = reshape(str_topo_conv(:,3),Npixel,Npixel);
	%str_z_topo = reshape(str_topo(:,3),Npixel,Npixel);
	%str_z = reshape(str(:,3),Npixel,Npixel);

	dip_z_hs = reshape(dip_hs(:,3),Npixel,Npixel);
	dip_z_topo_deriv = reshape(dip_topo_deriv(:,3),Npixel,Npixel);
	dip_z_topo_conv = reshape(dip_topo_conv(:,3),Npixel,Npixel);
	dip_z_topo = reshape(dip_topo(:,3),Npixel,Npixel);
	dip_z = reshape(dip(:,3),Npixel,Npixel);

	%dik_z_hs = reshape(dik_hs(:,3),Npixel,Npixel);
	%dik_z_topo_deriv = reshape(dik_topo_deriv(:,3),Npixel,Npixel);
	%dik_z_topo_conv = reshape(dik_topo_conv(:,3),Npixel,Npixel);
	%dik_z_topo = reshape(dik_topo(:,3),Npixel,Npixel);
	%dik_z = reshape(dik(:,3),Npixel,Npixel);

	%sil_z_hs = reshape(sil_hs(:,3),Npixel,Npixel);
	%sil_z_topo_deriv = reshape(sil_topo_deriv(:,3),Npixel,Npixel);
	%sil_z_topo_conv = reshape(sil_topo_conv(:,3),Npixel,Npixel);
	%sil_z_topo = reshape(sil_topo(:,3),Npixel,Npixel);
	%sil_z = reshape(sil(:,3),Npixel,Npixel);

    % plot

	%load fred_1.txt   % fred pollitz, transverse component
	%load fred_2.txt   % fred, radial component
	% theta = atan(fred_2(:,1)./Re);  % in radian
	%theta = fred_1(:,1)/Re;
	%xx = Re*tan(theta);  %horizotnal distance scale 
    
    %ur = fred_2(:,2);  % radial displacement
    %ut = fred_1(:,2);  % transverse displacement
    
    % convert vertical and horizontal into radial and transverse
    theta2 = atan(x/Re);
    %uR = str_z(Npixel/2,51:100).*cos(theta)' + str_x(Npixel/2,51:100).*sin(theta)';
    %uR_hs = str_z_hs(Npixel/2,51:100).*cos(theta)' + str_x_hs(Npixel/2,51:100).*sin(theta)';
    %uT = -str_z(Npixel/2,51:100).*sin(theta)' + str_x(Npixel/2,51:100).*cos(theta)';
    %uT_hs = -str_z_hs(Npixel/2,51:100).*sin(theta)' + str_x_hs(Npixel/2,51:100).*cos(theta)';
  
    uR = dip_z(:,Npixel/2).*cos(theta2) + dip_x(:,Npixel/2).*sin(theta2);
    uR_hs = dip_z_hs(:,Npixel/2).*cos(theta2) + dip_x_hs(:,Npixel/2).*sin(theta2);
  
    uT = -dip_z(:,Npixel/2).*sin(theta2) + dip_x(:,Npixel/2).*cos(theta2);
    uT_hs = -dip_z_hs(:,Npixel/2).*sin(theta2) + dip_x_hs(:,Npixel/2).*cos(theta2);

    
    
    %zz = fred_2(:,2).*cos(theta) ...
	%	- fred_1(:,2).*sin(theta);
    %ux = fred_2(:,2).*sin(theta) + fred_1(:,2).*cos(theta);

	
	figure
    set(gca,'fontsize',14)
    subplot(211), plot(x, uR, '-.', x, uR_hs,':', 'LineWidth', 2.0)
    %axis([0 1000 0 5e-4])
    ax = axis;
    axis([-300 300 ax(3)  ax(4)])
    h=legend('Perturbation', 'Half Space', 4);
    set(h,'fontsize',14,'FontWeight','bold')   
    ylabel('Vertical ','FontSize',14,'FontWeight','bold')
    set(gca,'fontsize',14)
    subplot(212), plot( x, 1e2*(uR - uR_hs), 'LineWidth', 2.0)
    %axis([0 1000 0 6e-4])
    ylabel(' Difference x 100','FontSize',14,'FontWeight','bold')
    xlabel('Distance','FontSize',14,'FontWeight','bold')
    ax = axis;
    axis([-300 300 ax(3)  ax(4)])
    set(gca,'fontsize',14)
    
    
    
    figure
    set(gca,'fontsize',14)
    subplot(211), plot(x, uT, '-.', x, uT_hs,':', 'LineWidth', 2.0)
    %axis([0 1000 0 5e-4])
    ax = axis;
    axis([-300 300 ax(3)  ax(4)])
    h=legend('Perturbation', 'Half Space', 1);
    set(h,'fontsize',14,'FontWeight','bold')   
    ylabel('Radial ','FontSize',14,'FontWeight','bold')
    set(gca,'fontsize',14)
    subplot(212), plot( x, 1e2*(uT - uT_hs), 'LineWidth', 2.0)
    %axis([0 1000 0 6e-4])
    ylabel(' Difference x 100','FontSize',14,'FontWeight','bold')
    xlabel('Distance','FontSize',14,'FontWeight','bold')
    ax = axis;
    axis([-300 300 ax(3)  ax(4)])
    set(gca,'fontsize',14)
