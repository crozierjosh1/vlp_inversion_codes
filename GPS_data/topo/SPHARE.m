
	clear all

    % material constant

	nu = 0.25;
	mu = 3.0*10^10;  % [Pa]

    % study area [km]: [-area, area] \times [area, area]
    % Npixel: the number of obs points
    % topo_rows: the number of column for xy_h (topography)

	area = 1000; 
	Npixel = 100;
	topo_rows = 100;

    % observation points: xy_o = [x, y, 0]

	x = linspace(-area,area,Npixel)';
	x0 = x*ones(1,Npixel);
	x1 = x0';
	x1 = x1(:);
	x2 = x0(:);
	zr = zeros(size(x1));
	xy_o = [x1, x2, zr];

    % Height is given at xy_h
   
% construct a topographic model
    Re = 6300;     %earth radius in km.
  
    
    topo_x = linspace(-area,area,topo_rows)';
    [tempx, tempy] = meshgrid(topo_x);  
    xy_h = [tempx(:), tempy(:)];   
  
    topo = -0.5*(tempx.^2 + tempy.^2)/Re;
	h = 1000*topo(:);   %%% COnvert topography to meters
	H = max(h);
	
    
	topo_dx = Npixel/topo_rows;
	topo_dy = Npixel/topo_rows;

    % Fault geometry
    %  - [len,wid,dep,str,dip,E,N,dislo_str,dislo_dip,open]

	%dis_geom = [30,30,300,0,89,0,0,0,0,1]';
	dis_geom = [500,75,75,0,89,0,0,1,0,0]';

    % topography correction

	[u_hs, u_topo_deriv, u_topo_conv, u_topo, u] ...
		= sphare_topo(dis_geom, xy_o, xy_h, ...
		h, topo_rows, topo_dx, topo_dy, nu, mu);

      % x-directoin

	ux_hs = reshape(u_hs(:,1),Npixel,Npixel);
	ux_topo_deriv = reshape(u_topo_deriv(:,1),Npixel,Npixel);
	ux_topo_conv = reshape(u_topo_conv(:,1),Npixel,Npixel);
	ux_topo = reshape(u_topo(:,1),Npixel,Npixel);
	ux = reshape(u(:,1),Npixel,Npixel);

      % y-directoin

	uy_hs = reshape(u_hs(:,2),Npixel,Npixel);
	uy_topo_deriv = reshape(u_topo_deriv(:,2),Npixel,Npixel);
	uy_topo_conv = reshape(u_topo_conv(:,2),Npixel,Npixel);
	uy_topo = reshape(u_topo(:,2),Npixel,Npixel);
	uy = reshape(u(:,2),Npixel,Npixel);

      % z-directoin

	uz_hs = reshape(u_hs(:,3),Npixel,Npixel);
	uz_topo_deriv = reshape(u_topo_deriv(:,3),Npixel,Npixel);
	uz_topo_conv = reshape(u_topo_conv(:,3),Npixel,Npixel);
	uz_topo = reshape(u_topo(:,3),Npixel,Npixel);
	uz = reshape(u(:,3),Npixel,Npixel);

    % plot

   figure; mesh(topo_x,topo_x,topo); title(['topography'])
%	figure; title(['topographic correction'])
%		subplot(311); mesh(ux_topo);
%		subplot(312); mesh(uy_topo);
%		subplot(313); mesh(uz_topo);
 	figure; title(['corrected displacenemt'])
 		subplot(311); mesh(ux);
 		subplot(312); mesh(uy);
 		subplot(313); mesh(uz);
	figure; 
		subplot(311); plot(x,ux(Npixel/2,:),'b', x,ux_hs(Npixel/2,:),'r--');
        title('u_x')
        legend('Corrected','Half-space')
		subplot(312); plot(x,uy(Npixel/2,:), 'b',x,uy_hs(Npixel/2,:),'r--');
		title('u_y')
        subplot(313); plot(x,uz(Npixel/2,:), 'b', x,uz_hs(Npixel/2,:),'r--');
        title('u_z')
%	figure; title(['Derivative term (Blue) - convolution term (Red)'])
%		subplot(311); plot(x,ux_topo_deriv(Npixel/2,:)); 
%			hold; plot(x,ux_topo_conv(Npixel/2,:),'r');
%		subplot(312); plot(x,uy_topo_deriv(Npixel/2,:)); 
%			hold; plot(x,uy_topo_conv(Npixel/2,:),'r');
%		subplot(313); plot(x,uz_topo_deriv(Npixel/2,:)); 
%			hold; plot(x,uz_topo_conv(Npixel/2,:),'r');


