clear all

        nu = 0.25;  % Poisson ratio
        mu = 3.0*10^4; % Bulk modulus

        SAR_area = 25;  % [-25km,25km] in square
        Npixel = 50;
        dem_rows = 50;

    % observation points
        x = linspace(-SAR_area,SAR_area,Npixel)';
        xy = [];
        for kx = 1:Npixel
            for ky = 1:Npixel
                xy = [xy; x(kx) x(ky)];
            end
        end

    % DEM point: taken as same points as obs.
        DEM_x = linspace(-SAR_area,SAR_area,dem_rows)';
        mat_xy_vals = [];
        for kx = 1:dem_rows
            for ky = 1:dem_rows
                mat_xy_vals = [mat_xy_vals; DEM_x(kx), DEM_x(ky)];
            end
        end
        mat_nul_vals = mat_xy_vals;

    % topography: one mountain with one hole 
        sig = 5;
        H = 100;
        gauss = H * exp(-0.5*x.^2/sig^2 );
        topo = gauss*gauss';
        d_DEM = topo(:);

        dem_dx = Npixel/dem_rows;
        dem_dy = Npixel/dem_rows;

    % Mogi source just below the top of mountain
        d = 4.2;  % depth [km]
	    dV = 120;  % volume change [10e-6 m^3]
        vol_geom = [0 0 d dV];

    % calc corr
        [u_hs, u_topo, u] = volc_corr(vol_geom, xy, ...
                mat_xy_vals, mat_nul_vals, d_DEM, dem_rows, ...
                dem_dx, dem_dy, nu, mu);

    % 0-th order
        u_hs = reshape(u_hs,Npixel,Npixel);
    % correction
        u_topo = reshape(u_topo,Npixel,Npixel);
    % total
        u = reshape(u,Npixel,Npixel);

        figure; mesh(topo);  title('Topography')
        figure; mesh(u_topo);  title('Correction term')
        