
	clear all
    path(path, '/matlab/disloc')
    
    % material constant

	nu = 0.25;
	mu = 3.0*10^10;  % [Pa]

    % study area [km]: [-area, area] \times [area, area]
    % Npixel: the number of obs points
    % topo_rows: the number of column for xy_h (topography)

	area = 1600; 
	Npixel = 101;
	topo_rows = 101;

    % observation points: xy_o = [x, y, 0]

	x = linspace(-area,area,Npixel)';
	x0 = x*ones(1,Npixel);
	x1 = x0';
	x1 = x1(:);
	x2 = x0(:);
	zr = zeros(size(x1));
	xy_o = [x1, x2, zr];

% construct a topographic model
     Re = 6300;     %earth radius in km.

     topo_x = linspace(-area,area,topo_rows)';
     [tempx, tempy] = meshgrid(topo_x);
     xy_h = [tempx(:), tempy(:)];

     topo = -0.5*(tempx.^2 + tempy.^2)/Re;
         h = 1000*topo(:);   %%% COnvert topography to meters

	topo_dx = 1000*(topo_x(2)-topo_x(1));
	topo_dy = 1000*(topo_x(2)-topo_x(1));

    % Fault geometry
    %%%  - [len,wid,dep,str,dip,E,N,dislo_str,dislo_dip,open]
    %  - [len,wid,dep,dip,str,E,N,dislo_str,dislo_dip,open]
    %    depth is the lower edge

	% dis_geom = [500,100,35,20,-90,0,-100,0.3,-1,0]';
%	dis_geom = [40,25,300,90,0,0,0,-1,0,0]'; % case 1
%	dis_geom = [40,25,300,90,0,0,0,0,-1,0]'; % case 2
%	dis_geom = [40,25,300,0,0,0,0,0,0,1]'; % case 4
	dis_geom = [40,25,300,90,0,0,0,0,0,1]'; % case 3
	% [U, D, S, flag] = disloc3d(dis_geom, xy_o', mu, nu);
	% Ux = reshape(U(1,:),Npixel,Npixel);
	% Uy = reshape(U(2,:),Npixel,Npixel);
	% Uz = reshape(U(3,:),Npixel,Npixel);
	% quiver(x,x,Ux,Uy)
	% axis([-100,100,-100,100])

    % topography correction

	[u_hs, u_topo_deriv, u_topo_conv, u_topo, u] ...
		= sphere_topo2(dis_geom, xy_o, xy_h, h, ...
			topo_rows, topo_dx, topo_dy, nu, mu);

      % x-directoin

	ux_hs = reshape(u_hs(:,1),Npixel,Npixel);
	ux_topo_deriv = reshape(u_topo_deriv(:,1),Npixel,Npixel);
	ux_topo_conv = reshape(u_topo_conv(:,1),Npixel,Npixel);
	ux_topo = reshape(u_topo(:,1),Npixel,Npixel);
	ux = reshape(u(:,1),Npixel,Npixel);

      % y-directoin

	uy_hs = reshape(u_hs(:,2),Npixel,Npixel);
	uy_topo_deriv = reshape(u_topo_deriv(:,2),Npixel,Npixel);
	uy_topo_conv = reshape(u_topo_conv(:,2),Npixel,Npixel);
	uy_topo = reshape(u_topo(:,2),Npixel,Npixel);
	uy = reshape(u(:,2),Npixel,Npixel);

      % z-directoin

	uz_hs = reshape(u_hs(:,3),Npixel,Npixel);
	uz_topo_deriv = reshape(u_topo_deriv(:,3),Npixel,Npixel);
	uz_topo_conv = reshape(u_topo_conv(:,3),Npixel,Npixel);
	uz_topo = reshape(u_topo(:,3),Npixel,Npixel);
	uz = reshape(u(:,3),Npixel,Npixel);

    % plot

%	figure; mesh(tempx,tempy,topo); title(['topography'])
%	figure; title(['topographic correction'])
%		subplot(311); mesh(ux_topo);
%		subplot(312); mesh(uy_topo);
%		subplot(313); mesh(uz_topo);
%	figure; title(['corrected displacenemt'])
%		subplot(311); mesh(ux_hs);
%		subplot(312); mesh(uy_hs);
%		subplot(313); mesh(ux_hs);

    mid = (Npixel+1)/2;
figure; 
		subplot(311); plot(x(mid:end),100*ux(mid,mid:end),'b', x(mid:end),100*ux_hs(mid,mid:end),'r--');
        ax = axis;
        axis([0, 800, ax(3), ax(4)])
        ylabel('U_x')
        legend('Corrected','Half-space')
		subplot(312); plot(x(mid:end),100*uy(mid,mid:end),'b', x(mid:end),100*uy_hs(mid,mid:end),'r--');
        ax = axis;
        axis([0, 800, min(ax(3),-1e-3), max(ax(4),1e-3)])
        ylabel('U_y')
       	subplot(313); plot(x(mid:end),100*uz(mid,mid:end),'b', x(mid:end),100*uz_hs(mid,mid:end),'r--');
        ax = axis;
        axis([0, 800, ax(3), ax(4)])
        xlabel('Distance From Source')
        ylabel('U_z')
        
        
	figure; 
		subplot(311); plot(x(mid:end),100*ux_topo(mid,mid:end)) 
		ax = axis;
        axis([0, 800, ax(3), ax(4)])
        ylabel('U_x')
        subplot(312); plot(x(mid:end),100*uy_topo(mid,mid:end)) 
		ax = axis;
        axis([0, 800, min(ax(3),-1e-3), max(ax(4),1e-3)])
        ylabel('U_y')
        subplot(313); plot(x(mid:end),100*uz_topo(mid,mid:end)) 
	    ax = axis;
        axis([0, 800, ax(3), ax(4)])
        xlabel('Distance From Source')
        ylabel('U_z')
    
