
	function [c, u, u_c] = topo_corr(str, hgt, ...
		topo_rows, topo_dx, topo_dy, nu, mu)

%  topo_corr does the correction to the elastic half-space displacements
%  due to interaction of shear stresses at surface of space with topography.
%  This algorithm is outlined in Williams and Wadge, 2000.
%  str is a matrix containing 9 components for each location:
%  3 displacement, 3 stress, and 3 strain e.g. as output from vol_stress
%
%  hgt is a vector with Height data in row-major order 
%                   (e.g. GMT's grd2xyz -ZLTa)
%  topo_rows is the number of rows in the Height.
%  topo_dx is the Height resolution in the E-W direction (meters)
%  topo_dy is the Height resolution in the N-S direction (meters)
%  nu is Poisson's ratio
%  mu is the shear Modulus (Pa)
%  u is a matrix containing the topographic corrections at each location 
%  8/18/2002 --EJP  (evelyn@giseis.alaska.edu)


	ndem = length(hgt);
	topo_cols = ndem./topo_rows;
	i = sqrt(-1);

	hgt_r = reshape(hgt,topo_rows,topo_cols);
	str1r = reshape(str(:,4),topo_rows,topo_cols);
	str2r = reshape(str(:,5),topo_rows,topo_cols);
	str3r = reshape(str(:,6),topo_rows,topo_cols);
	du3dx = reshape(str(:,7),topo_rows,topo_cols);
	du3dy = reshape(str(:,8),topo_rows,topo_cols);
	du1dx = reshape(str(:,9),topo_rows,topo_cols);
	du2dy = reshape(str(:,10),topo_rows,topo_cols);

% hgt_ is in meters.

	p1a = hgt_r.*str1r;
	p1b = hgt_r.*str3r;
	p2a = hgt_r.*str3r;
	p2b = hgt_r.*str2r;

% find the size of the fft in powers of 2

	fft_len = max([topo_rows topo_cols]);
	fft_len = 2.0*fft_len;
	pows2 = [2 4 8 16 32 64 128 256 512 1024 2048];
	for k = 1:11,
	  if fft_len < pows2(k),
	    fft_len = pows2(k);
	    break;
	  end
	end

% compute frequencies in x and y directions

	xfreqs = (0:fft_len-1)./(fft_len.*topo_dx);
	xfreqs = fftshift(xfreqs)-1./2./topo_dx;
	rones = ones(1,topo_rows);
	cones = ones(1,topo_cols);
	mxfreqs = xfreqs'*rones;

	yfreqs = (0:fft_len-1)./(fft_len.*topo_dy);
	yfreqs = fftshift(yfreqs)-1./2./topo_dy;
	myfreqs = yfreqs'*cones;

% compute x derivative of p1a

	fp1a_x = fft(p1a',fft_len);
	fp1a_xdx= i.*2.*pi.*fp1a_x.*mxfreqs;
	p1a_xdx = real(ifft(fp1a_xdx))';
	p1a_xdx = p1a_xdx(1:topo_rows,1:topo_cols);

% compute y derivative of p1b

	fp1b_y = fft(p1b,fft_len);
	fp1b_ydy = i.*2.*pi.*fp1b_y.*myfreqs;
	p1b_ydy = real(ifft(fp1b_ydy));
	p1b_ydy = p1b_ydy(1:topo_rows,1:topo_cols);

	p1 = p1a_xdx + p1b_ydy;

% compute x derivative of p2a

	fp2a_x = fft(p2a',fft_len);
	fp2a_xdx = i.*2.*pi.*fp2a_x.*mxfreqs;
	p2a_xdx = real(ifft(fp2a_xdx))';
	p2a_xdx = p2a_xdx(1:topo_rows,1:topo_cols);

% compute y derivative of p2b

	fp2b_y = fft(p2b,fft_len);
	fp2b_ydy = i.*2.*pi.*fp2b_y.*myfreqs;
	p2b_ydy = real(ifft(fp2b_ydy));
	p2b_ydy = p2b_ydy(1:topo_rows,1:topo_cols);

	p2 = p2a_xdx + p2b_ydy;

% compute the fourier transforms of the transfer functions

	s = (xfreqs'*ones(fft_len,1)')';
	t = yfreqs'*ones(fft_len,1)';
	s(1,1) = 1.0;
	t(1,1) = 1.0;
	g1 = (s.^2+t.^2-nu.*s.^2)./(2.*pi.*mu.*(s.^2+t.^2).^1.5);
	g2 = (s.^2+t.^2-nu.*t.^2)./(2.*pi.*mu.*(s.^2+t.^2).^1.5);
	g3 = (-1.0).*nu.*s.*t./(2.*pi.*mu.*(s.^2+t.^2).^1.5);
	g4 = (1-2.*nu).*s.*i./(4.*pi.*mu.*(s.^2+t.^2));
	g5 = (1-2.*nu).*t.*i./(4.*pi.*mu.*(s.^2+t.^2));

% set the DC values of the transfer functions to zero

	g1(1,1) = 0.0;
	g2(1,1) = 0.0;
	g3(1,1) = 0.0;
	g4(1,1) = 0.0;
	g5(1,1) = 0.0;

	fp1  = fft2(p1,fft_len,fft_len); 
	fp2  = fft2(p2,fft_len,fft_len);
	fu11 = fp1.*g1+fp2.*g3; 
	fu21 = fp2.*g2+fp1.*g3; 
	fu31 = fp1.*g4+fp2.*g5;
	u11  = real(ifft2(fu11)); 
	u21  = real(ifft2(fu21)); 
	u31  = real(ifft2(fu31));
	u11  = u11(1:topo_rows,1:topo_cols);
	u21  = u21(1:topo_rows,1:topo_cols);
	u31  = u31(1:topo_rows,1:topo_cols);
	c31  = hgt_r.*du3dx;
	c32  = hgt_r.*du3dy;
	cu33 = hgt_r.*nu.*(du1dx+du2dy)./(1-nu);

	u1_c = u11 - c31;
	u2_c = u21 - c32;
	u3_c = u31 - cu33;

	c = - [c31(:) c32(:) cu33(:)];   % derivative term
	u = [u11(:) u21(:) u31(:)];      % convolution term
	u_c = [u1_c(:) u2_c(:) u3_c(:)]; % total


