
	clear all

	nu = 0.25;
	mu = 3.0*10^4;

	SAR_area = 25;  % 50km
	Npixel = 101;
	dem_rows = 101;


	x = linspace(-SAR_area,SAR_area,Npixel)';
	xy = [];
	for kx = 1:Npixel
	    for ky = 1:Npixel
		xy = [xy; x(kx) x(ky)];
	    end
	end

	DEM_x = linspace(-SAR_area,SAR_area,dem_rows)';
	mat_xy_vals = [];
	for kx = 1:dem_rows
	    for ky = 1:dem_rows
		mat_xy_vals = [mat_xy_vals; DEM_x(kx), DEM_x(ky)];
	    end
	end
	mat_nul_vals = mat_xy_vals;

	H = 1500; sig = 5;
	%gauss = coeff/sqrt(2*pi*sig^2) * exp(-(1/2)*(x/sig).^2);
    gauss = sqrt(H)* exp(-(1/2)*(x/sig).^2);

	%coeff_c = 62;
	%cauchy = coeff_c/pi./((x/sig).^2+1);
	%coeff_t = sqrt(800);
	%trapez = zeros(Npixel,1);
	%index = find(abs(x)<2.5);
	%N_trapez = length(index);
	%trapez(index,:) = coeff_t*ones(N_trapez,1);
%	d_gauss = (-2*x/100) .* gauss;
	topo = gauss*gauss';
	% topo = cauchy*cauchy';
	% topo = trapez*trapez';
%	topo = gauss*d_gauss';
%	[dum,ind_peak] = max(d_gauss);
	d_DEM = topo(:);
	%H = max(d_DEM);

	dem_dx = Npixel/dem_rows;
	dem_dy = Npixel/dem_rows;

	d = 4.0;
	dV = 120;
	vol_geom = [0 0 d dV];
%	vol_geom = [x(ind_peak) 0 d dV];
%	vol_geom = [x(ind_peak) 0 d dV ...
%		    -x(ind_peak) 0 d dV];

	[u_hs, u_topo_deriv, u_topo_conv, u_topo, u] ...
		= makeGSAR_topo(vol_geom, xy, ...
		mat_xy_vals, mat_nul_vals, d_DEM, dem_rows, ...
		dem_dx, dem_dy, nu, mu);

	u_hs = reshape(u_hs,Npixel,Npixel);
	u_topo_deriv = reshape(u_topo_deriv,Npixel,Npixel);
	u_topo_conv = reshape(u_topo_conv,Npixel,Npixel);
	u_topo = reshape(u_topo,Npixel,Npixel);
	u = reshape(u,Npixel,Npixel);

	disp(['depth/height = ', d/H])
	disp(d/H)
	%figure; mesh(topo); title(['topography'])
	%figure; mesh(u_topo); title(['topographic correction'])
	%figure; mesh(u); title(['corrected displacenemt'])
	%figure; plot(x,u((Npixel-1)/2 +1,:)); hold; plot(x,u_hs((Npixel-1)/2 +1,:),'r--');
    figure; plot(x,u((Npixel-1)/2 +1,:),'b', x,u_hs((Npixel-1)/2 +1,:),'r--');
    legend('With topography','Half-space')
    title(['Dilational Source Beneath Topography, H/D = ',num2str(H*1e-3/d)])
	%title(['corrected displacenemt (Blue) - Half-space (Red)'])
	%figure; plot(x,u_topo_deriv(Npixel/2,:)); 
	%	hold; plot(x,u_topo_conv(Npixel/2,:),'r');
	%title(['Derivative term (Blue) - convolution term (Red)'])


