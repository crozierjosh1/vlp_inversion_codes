	function [u_hs, u_topo, u] = volc_corr(vol_geom, xy, ...
		mat_xy_vals, mat_nul_vals, d_DEM, dem_rows, ...
		dem_dx, dem_dy, nu, mu)

% Subroutine to make displacements for point sources
% August 18, 2002 --EJP (evelyn@giseis.alaska.edu)
% This treats range change data as absolute with respect to a reference
% level of zero deformation.  In practice the reference level is uncertain
% and the inversion should allow for a best-fitting reference level.
% Hence the appearance of 3*nv+1 parameters below.
% 
% Input:
%	vol_geom     - Point Source Geometry.Rows=[x(km) y(km) depth(km)
%                      dV(1e6 m^3)]. The very last element of vol_geom is the
%                      DEM offset.
%	xy	     - xy coordinates of InSAR measurements (km)
%	mat_xy_vals  - a vector whose elements are the localxy locations of
%                      the elements in d_DEM.
%	mat_nul_vals - a vector whose elements are at the same xy locations
%                      as those of d_DEM and the vector has Nans at the 
%                      locations of no SAR (or displacement) data.
%	d_DEM	     - a vector with DEM data in row-major order 
%                      (e.g. GMT -ZLTa)
%	dem_rows     - the number of rows in the DEM 
%	dem_dx       - the DEM resolution in the x direction (meters) 
%	dem_dy       - the DEM resolution in the y direction (meters) 
%	nu	     - Poisson's ratio
%	mu	     - Bulk Modulus (MPa)


	[nel,nv] = size(vol_geom);
        nv=nv/4.0;
	[nsta,nel2] = size(xy);
	ndata = nsta;  % SAR pixels
        ndem=length(d_DEM(:));

	disp ('MakeGSAR for range change only!');

% Descending track unit vector

	lookangle = 00;

% The track angle 
	track = 16.1313;

	unit = [-cos(track*pi/180)*sin(lookangle*pi/180), ...
		sin(track*pi/180)*sin(lookangle*pi/180), ...
		-cos(lookangle*pi/180)];
	unit = -1.0.*unit;
	STR = zeros(ndem,10);

	for i=1:nv
	    % disp (int2str(i));
	    u = vol_stress(nu,mu,vol_geom((4*i-3):4*i), mat_xy_vals);
	    STR = STR+u;
	end

	u_hs = STR(:,1:3);
	u_topo = topo_corr(STR,d_DEM,dem_rows, dem_dx,dem_dy,nu,mu);
%	u_hs = [u_hs(~isnan(mat_nul_vals),1) ...
%		u_hs(~isnan(mat_nul_vals),2) ...
%		u_hs(~isnan(mat_nul_vals),3)];
%	u_topo = [u_topo(~isnan(mat_nul_vals),1) ...
%		u_topo(~isnan(mat_nul_vals),2) ...
%		u_topo(~isnan(mat_nul_vals),3)];
	u_hs = (unit*u_hs')';
	u_topo = (unit*u_topo')';
	u = u_hs + u_topo;


