% script to compute topographic effect on displacements
% Mogi source beneath volcano

	clear all

% material constants
	nu = 0.25;
	mu = 3.0*10^4;
    
% observation points
%NOTE Code as exists ignores these and computes at DEM points!!

	Xlim = 25;  % 50km
	Npixel = 101;
	x = linspace(-Xlim,Xlim,Npixel)';
    
    
% Deformation source
	d = 5.0;    % depth in km
	%d = 10.0;    % depth in km
	dV = 120;   % volume change in 10^6  m^3
	vol_geom = [0 0 d dV];

% construct a topographic model
    DEM_lim = 25;
	N_rows = 101;
    
	DEM_x = linspace(-DEM_lim,DEM_lim,N_rows)';
    [tempx, tempy] = meshgrid(DEM_x);  
    mat_xy_vals = [tempx(:), tempy(:)];   
    
    H = [0, 500, 1000, 1500];
%H = [0, 1500];
 
	sig = 5;
    
for k = 1:length(H); 
    %note height in m, length scale in km!
    disp(['H/L = ', num2str(0.5*H(k)*1e-3/sig)])
    gauss = sqrt(H(k))* exp(-(1/2)*(x/sig).^2);
	topo = gauss*gauss';
	d_DEM = topo(:);

	dem_dx = Npixel/N_rows;
	dem_dy = Npixel/N_rows;

    % call correction
	[u_hs, u_topo_deriv, u_topo_conv, u_topo, u] ...
		= makeGSAR_topo(vol_geom, x, ...
		mat_xy_vals, [], d_DEM, N_rows, ...
		dem_dx, dem_dy, nu, mu);
% output
	u_hs = reshape(u_hs,Npixel,Npixel);
	%u_topo_deriv = reshape(u_topo_deriv,Npixel,Npixel);
	%u_topo_conv = reshape(u_topo_conv,Npixel,Npixel);
	%u_topo = reshape(u_topo,Npixel,Npixel);
	u = reshape(u,Npixel,Npixel);
   
    u_plot(k,:) = u((Npixel-1)/2 +1,:);
end

	%disp(['depth/height = ', d/H])
	%disp(d/H)
	%figure; mesh(u_topo); title(['topographic correction'])
	%figure; mesh(u); title(['corrected displacenemt'])
	%figure; plot(x,u((Npixel-1)/2 +1,:)); hold; plot(x,u_hs((Npixel-1)/2 +1,:),'r--');
    %%figure; plot(x,u((Npixel-1)/2 +1,:),'b', x,u_hs((Npixel-1)/2 +1,:),'r--');
    %%legend('With topography','Half-space')
    %%title(['Dilational Source Beneath Topography, H/D = ',num2str(H*1e-3/d)])
	%title(['corrected displacenemt (Blue) - Half-space (Red)'])
	%figure; plot(x,u_topo_deriv(Npixel/2,:)); 
	%	hold; plot(x,u_topo_conv(Npixel/2,:),'r');
	%title(['Derivative term (Blue) - convolution term (Red)'])

    figure
    plot(x, u_plot(1,:), ':', x, u_plot(2,:),'-.',x, u_plot(3,:),'--',x, u_plot(4,:),...
        'LineWidth', 2.0)
    legend(['H/L = ',num2str(0.5*H(1)*1e-3/sig)], ['H/L = ',num2str(0.5*H(2)*1e-3/sig)],...
        ['H/L = ',num2str(0.5*H(3)*1e-3/sig)], ['H/L = ',num2str(0.5*H(4)*1e-3/sig)])
    axis([-15 15 0 max(max(u_plot))])
    xlabel('Distance from peak (km)', 'FontSize', 14);
    ylabel('Uplift' , 'FontSize', 14);
	figure; mesh(x,x,topo); title(['topography'])
