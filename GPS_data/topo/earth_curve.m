% script to compute earth curvature effect on displacements


	clear all

% material constants
	nu = 0.25;
	mu = 3.0*10^4;
    
    Re = 6300;     %earth radius in km.
    
% construct a topographic model
    DEM_lim = 1000;
	N_rows = 101;
    
	DEM_x = linspace(-DEM_lim,DEM_lim,N_rows)';
    [tempx, tempy] = meshgrid(DEM_x);  
    mat_xy_vals = [tempx(:), tempy(:)];   
  
    topo = -0.5*(tempx.^2 + tempy.^2)/Re;
 
	d_DEM = topo(:);
    

	dem_dx = Npixel/N_rows;
	dem_dy = Npixel/N_rows;

   figure; mesh(DEM_x,DEM_x,topo); title(['topography'])