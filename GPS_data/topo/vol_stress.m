	function disp_sig_str = vol_stress(nu, G, volgeom, xy)

% vol_stress returns a vector containing the displacements,
% stresses, and strains at the surface of an elastic half-space
% due to a spherical volume change
%
% nu is Poisson's ratio
% G is the modulus of rigidity in MPa
% volgeom x,y,d in km
% volgeom dV in millions of cubic meters
% xy in km
% --EJP 8/21/2002  (evelyn@giseis.alaska.edu)
 
	nurat = nu./(1-nu);

	[nsta,m] = size(xy);
	nvec = nsta-1;
	u = zeros(nsta,3);

% compute radius from center source.

	X_e = xy(:,1) - ones(nsta,1)*volgeom(1);
	X_n = xy(:,2) - ones(nsta,1)*volgeom(2);
	d = abs(volgeom(3));

	R  = sqrt( X_e .^2 + X_n .^2 + d.^2);
	Rm = 1000*R;

% compute displacements %constant converts km to m
% Assume volgeom(4) is already in meters
% vol_geom(4) is millions of meters cubed

	K1 = (1-nu)*(volgeom(4)*1e6)/pi;
	prefact=K1.*1e3./Rm.^3;
	ue=prefact.*X_e;
	un=prefact.*X_n;
	uz=prefact.*d;

% compute displacement gradients at surface of half-space

	e11= (prefact.*(1-3.*X_e.*X_e./R.^2))./1e3;
	e22= (prefact.*(1-3.*X_n.*X_n./R.^2))./1e3;
	e12= (prefact.*(-3.0.*X_e.*X_n./R.^2))./1e3;
	e31= (prefact.*(-3.0.*X_e.*d./R.^2))./1e3;
	e32= (prefact.*(-3.0.*X_n.*d./R.^2))./1e3;
	de11dy=3.*K1./R.^5.*(5.*X_e.*X_e.*X_n./R.^2-X_n);
	de31dy=3.*K1.*d./R.^5.*(5.*X_e.*X_n./R.^2);

% compute stresses at surface of half-space

	sig11=2.*G.*(e11+nurat*(e11+e22));
	sig21=2.*G.*e12;
	sig22=2.*G.*(e22+nurat*(e11+e22));
	disp_sig_str = [ue, un, uz, sig11, sig22, sig21, e31, e32, e11, e22];

