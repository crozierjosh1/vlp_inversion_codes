%TQ catalog inversion
%josh crozier 2020

%% Load stuff

load('total_TQ_catalog_trimmed.mat')
keepind = TQ_tab.T > 10 & TQ_tab.T < 50 &...
    TQ_tab.amp_abovenoise > log10(3.0) & ...
    TQ_tab.std_abovenoise > log10(3.0) & ...
    TQ_tab.Q > 6 & ...
    TQ_tab.mean_min50_errphase < 0.1 &...
    TQ_tab.channelCount > 0 & ...
    (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2);
TQ_tab = TQ_tab(keepind,:);

TQ_tab = sortrows(TQ_tab,'start');


% lakeh = load('Lavalevel_Nov2017_nov2019export.mat');
% lakeh.date = datetime(lakeh.t,'ConvertFrom','datenum');
% TQ_tab.lakeh = interp1(lakeh.date,lakeh.h,TQ_tab.start);
lakeh = readtable('ContinuousLavaLakeElevation.txt',...
    'ReadVariableNames',false,'HeaderLines',2,'DatetimeType','text');
lakeh.Properties.VariableNames{'Var1'} = 'datestr';
lakeh.Properties.VariableNames{'Var2'} = 'h';
lakeh.Properties.VariableNames{'Var3'} = 'err';
lakeh.Properties.VariableNames{'Var4'} = 'type';
lakeh.date = NaT(height(lakeh),1);
lakeh.date(lakeh.type == 1) = datetime(lakeh.datestr(lakeh.type == 1));
lakeh.date(lakeh.type == 2) = datetime(lakeh.datestr(lakeh.type == 2));
lakeh.date(lakeh.type == 3) = datetime(lakeh.datestr(lakeh.type == 3));
lakeh.date(lakeh.type == 4) = datetime(lakeh.datestr(lakeh.type == 4));
lakeh.date(lakeh.type == 5) = datetime(lakeh.datestr(lakeh.type == 5));
lakeh = removevars(lakeh,{'err','type','datestr'});
lakeh = lakeh(~isnan(lakeh.h),:);
lakeh = sortrows(lakeh,'date');
lakeh = lakeh(diff(lakeh.date)~=seconds(0),:);
lakeh = struct('date',lakeh.date,'h',lakeh.h);
TQ_tab.lakeh = interp1(lakeh.date,lakeh.h,TQ_tab.start,'linear');

lakeh_resample_dt = hours(1);
lakeh_resample = timetable(lakeh.date,lakeh.h,'VariableNames',{'h'});
lakeh_resample = smoothdata(lakeh_resample,'movmean',lakeh_resample_dt);
lakeh_resample = retime(lakeh_resample,...
    lakeh_resample.Time(1):lakeh_resample_dt:lakeh_resample.Time(end),'linear');

lakeA = readtable('lake_area.xlsx');
lakeA.date = datetime(lakeA.Year,lakeA.Month,lakeA.day);
TQ_tab.lakeA = interp1(lakeA.date,lakeA.Area,TQ_tab.start,'linear');
firstin = find(~isnan(TQ_tab.lakeA),1);
TQ_tab.lakeA(1:firstin) = TQ_tab.lakeA(firstin);
lastin = find(~isnan(TQ_tab.lakeA),1,'last');
TQ_tab.lakeA(lastin:end) = TQ_tab.lakeA(lastin);

P_table = readtable('GPS_data/P_summit_SCR_flankcorr_GPS_ar1_d5000_mu3_HMM_ar1-23_V3-94.csv');
% P_table = readtable('GPS_data/P_summit_SCR_flankcorr_GPS_ar1_d5000_mu3_HMM_ar1-23_V3-94_tiltnorthonly.csv');
P_table = table2timetable(P_table);
TQ_tab.P_restop = interp1(P_table.Time,P_table.P,TQ_tab.start,'linear');
TQ_tab.P_SCR = interp1(P_table.Time,P_table.SCR_P,TQ_tab.start,'linear');

%% processing
%discard too high Q
% deletein = TQ_tab.Q > 100 & TQ_tab.amp_abovenoise < 0.6;
% TQ_tab = TQ_tab(~deletein,:);

%table names for each channel
networks = "HV";
% 'using limited stations'
stationlist = ["NPB","SRM","NPT","OBL","WRM","SDH","UWE","UWB",...
    "SBL","KKO","RIMD"];
locationlist = "*";
channellist = ["HHE","HHN","HHZ"];
vertchannel_station_str = strings(1,length(stationlist));
eastchannel_station_str = strings(1,length(stationlist));
northchannel_station_str = strings(1,length(stationlist));
for statin = 1:length(stationlist)
    vertchannel_station_str(statin) = strcat(networks,'_',...
        stationlist(statin),...
        '_', channellist(3));
    eastchannel_station_str(statin) = strcat(networks,'_',...
        stationlist(statin),...
        '_', channellist(1));
    northchannel_station_str(statin) = strcat(networks,'_',...
        stationlist(statin),...
        '_', channellist(2));
end
vertchannel_station_list_amp = strcat("amp_",vertchannel_station_str);
vertchannel_station_list_phase = strcat("phase_",vertchannel_station_str);
eastchannel_station_list_amp = strcat("amp_",eastchannel_station_str);
eastchannel_station_list_phase = strcat("phase_",eastchannel_station_str);
northchannel_station_list_amp = strcat("amp_",northchannel_station_str);
northchannel_station_list_phase = strcat("phase_",northchannel_station_str);

%set zeros to NaNs
% TQ_tab = standardizeMissing(TQ_tab,0,'DataVariables',...
%     [vertchannel_station_list_amp;...
%     eastchannel_station_list_amp;...
%     northchannel_station_list_amp]);
% NaNind = isnan(TQ_tab{:,...
%     [vertchannel_station_list_amp;...
%     eastchannel_station_list_amp;...
%     northchannel_station_list_amp]});
% temp_phases = TQ_tab{:,...
%     [vertchannel_station_list_phase;...
%     eastchannel_station_list_phase;...
%     northchannel_station_list_phase]};
% temp_phases(NaNind) = NaN;
% TQ_tab(:,...
%     [vertchannel_station_list_phase;...
%     eastchannel_station_list_phase;...
%     northchannel_station_list_phase]) = num2cell(temp_phases);


%% mogi inversion
%{
TQ_tab = mogi_inv_table(TQ_tab,stationlist,...
    vertchannel_station_list_amp,vertchannel_station_list_phase,...
    eastchannel_station_list_amp, eastchannel_station_list_phase, ...
    northchannel_station_list_amp, northchannel_station_list_phase,...
    true);

TQ_tab = mogi_inv_table_depth(TQ_tab,stationlist,...
    vertchannel_station_list_amp,vertchannel_station_list_phase,...
    eastchannel_station_list_amp, eastchannel_station_list_phase, ...
    northchannel_station_list_amp, northchannel_station_list_phase,...
    true);

%plot example event
eventin = find(TQ_tab.start > datetime(2017,5,21,9,50,0) &...
    TQ_tab.start < datetime(2017,5,21,9,55,0) & ...
    TQ_tab.T > 34.8 & ...
    TQ_tab.T < 45);
% eventin = find(TQ_tab.start > datetime(2017,6,21,9,50,0) &...
%     TQ_tab.start < datetime(2017,7,21,9,55,0) & ...
%     TQ_tab.T > 34.8 & ...
%     TQ_tab.T < 45, 1);
plot_disp_map(TQ_tab(eventin,:),true,false)
hold on
%}



%% other preprocessing
%find events with multiple modes
%{
TQ_tab = sortrows(TQ_tab,'start');
mode_T_sep = 100; %seconds
TQ_tab.mode = zeros(height(TQ_tab),1);
peakstartin = 1;
for ii = 1:height(TQ_tab)
    if seconds(TQ_tab.start(ii) - TQ_tab.start(peakstartin)) > mode_T_sep
        if ii - peakstartin > 1
            ivec_temp = peakstartin:ii-1;
            [~,sortin] = sort(TQ_tab.T(ivec_temp),'descend');
            TQ_tab.mode(ivec_temp(sortin)) = 1:length(ivec_temp);
        end
        peakstartin = ii;
    end
end
%}

% [unique_datetimes,ifull,iunique] = unique(TQ_tab.datetime);
% for ii = 1:length(unique_datetimes)
%     ivec_temp = find(TQ_tab.datetime == unique_datetimes(ii));
%     T_temp = TQ_tab.T(ivec_temp );
%     [~,ivec_sort] = sort(T_temp,'descend');
%     TQ_tab.mode(ivec_temp(ivec_sort)) = 1:length(T_temp);
% end

%calculate mean vert and horz displacements
TQ_tab.meanvert = nanmean(TQ_tab{:,vertchannel_station_list_amp},2);
TQ_tab.meanhor = nanmean(sqrt(TQ_tab{:,eastchannel_station_list_amp}.^2 ...
+ TQ_tab{:,northchannel_station_list_amp}.^2),2);
allchannel_station_list_amp = [eastchannel_station_list_amp,...
    northchannel_station_list_amp,...
    vertchannel_station_list_amp];
TQ_tab.Amean = log10(nansum(TQ_tab{:,allchannel_station_list_amp},2)./....
    sum(isfinite(TQ_tab{:,allchannel_station_list_amp}),2));

%calculate circular mean and variance of phase
TQ_tab.eastphase_mean = atan2(nansum(sin(TQ_tab{:,eastchannel_station_list_phase}),2),...
    nansum(cos(TQ_tab{:,eastchannel_station_list_phase}),2));
TQ_tab.eastphase_var = 1 - (sqrt(nansum(sin(TQ_tab{:,eastchannel_station_list_phase}),2).^2 +...
    nansum(cos(TQ_tab{:,eastchannel_station_list_phase}),2).^2))./...
    (sum(isfinite(TQ_tab{:,eastchannel_station_list_phase}),2));
TQ_tab.eastphase_var(sum(isfinite(TQ_tab{:,eastchannel_station_list_phase}),2) < 2) = 1;

TQ_tab.northphase_mean = atan2(nansum(sin(TQ_tab{:,northchannel_station_list_phase}),2),...
    nansum(cos(TQ_tab{:,northchannel_station_list_phase}),2));
TQ_tab.northphase_var = 1 - (sqrt(nansum(sin(TQ_tab{:,northchannel_station_list_phase}),2).^2 +...
    nansum(cos(TQ_tab{:,northchannel_station_list_phase}),2).^2))./...
    (sum(isfinite(TQ_tab{:,northchannel_station_list_phase}),2));
TQ_tab.northphase_var(sum(isfinite(TQ_tab{:,northchannel_station_list_phase}),2) < 2) = 1;

TQ_tab.vertphase_mean = atan2(nansum(sin(TQ_tab{:,vertchannel_station_list_phase}),2),...
    nansum(cos(TQ_tab{:,vertchannel_station_list_phase}),2));
TQ_tab.vertphase_var = 1 - (sqrt(nansum(sin(TQ_tab{:,vertchannel_station_list_phase}),2).^2 +...
    nansum(cos(TQ_tab{:,vertchannel_station_list_phase}),2).^2))./...
    (sum(isfinite(TQ_tab{:,vertchannel_station_list_phase}),2));
TQ_tab.vertphase_var(sum(isfinite(TQ_tab{:,vertchannel_station_list_phase}),2) < 2) = 1;

allchannel_station_list_phase = [eastchannel_station_list_phase,...
    northchannel_station_list_phase,...
    vertchannel_station_list_phase];
TQ_tab.phase_mean = atan2(nansum(sin(TQ_tab{:,allchannel_station_list_phase}),2),...
    nansum(cos(TQ_tab{:,allchannel_station_list_phase}),2));
TQ_tab.phase_var = 1 - (sqrt(nansum(sin(TQ_tab{:,allchannel_station_list_phase}),2).^2 +...
    nansum(cos(TQ_tab{:,allchannel_station_list_phase}),2).^2))./...
    (sum(isfinite(TQ_tab{:,allchannel_station_list_phase}),2));
TQ_tab.phase_var(sum(isfinite(TQ_tab{:,allchannel_station_list_phase}),2) < 2) = 1;

dateticks = datetime(2006,1,1):calmonths(3):datetime(2019,1,1);
dateticklabels = strings(size(dateticks));
dateticklabels(1:4:end) = string(num2str((2006:2019)'));

%calculate single station amplitude
TQ_tab.A_single_station = NaN(height(TQ_tab),1);
TQ_tab.amp_NP_E = NaN(height(TQ_tab),1);
TQ_tab.amp_NP_N = NaN(height(TQ_tab),1);
TQ_tab.amp_NP_Z = NaN(height(TQ_tab),1);
TQ_tab.phase_NP_E = NaN(height(TQ_tab),1);
TQ_tab.phase_NP_N = NaN(height(TQ_tab),1);
TQ_tab.phase_NP_Z = NaN(height(TQ_tab),1);
TQ_tab.errphase_E = NaN(height(TQ_tab),1);
TQ_tab.errphase_N = NaN(height(TQ_tab),1);
TQ_tab.errphase_Z = NaN(height(TQ_tab),1);

refdate = datetime(2008,11,1,0,0,0);
TQ_tab.A_single_station(TQ_tab.start >= refdate) = ...
    sqrt(TQ_tab.amp_HV_NPT_HHZ(TQ_tab.start >= refdate).^2);% + ...
    %TQ_tab.amp_HV_NPT_HHE(TQ_tab.start >= refdate).^2 + ...
    %TQ_tab.amp_HV_NPT_HHN(TQ_tab.start >= refdate).^2);
TQ_tab.A_single_station(TQ_tab.start < refdate) = ...
    sqrt(TQ_tab.amp_HV_SDH_HHZ(TQ_tab.start < refdate).^2);% + ...
    %TQ_tab.amp_HV_SDH_HHE(TQ_tab.start < refdate).^2 + ...
    %TQ_tab.amp_HV_SDH_HHN(TQ_tab.start < refdate).^2);

TQ_tab.amp_NP_E(TQ_tab.start >= refdate) = TQ_tab.amp_HV_NPT_HHE(TQ_tab.start >= refdate);
TQ_tab.amp_NP_N(TQ_tab.start >= refdate) = TQ_tab.amp_HV_NPT_HHN(TQ_tab.start >= refdate);
TQ_tab.amp_NP_Z(TQ_tab.start >= refdate) = TQ_tab.amp_HV_NPT_HHZ(TQ_tab.start >= refdate);
TQ_tab.amp_NP_E(TQ_tab.start < refdate) = TQ_tab.amp_HV_NPB_HHE(TQ_tab.start < refdate);
TQ_tab.amp_NP_N(TQ_tab.start < refdate) = TQ_tab.amp_HV_NPB_HHN(TQ_tab.start < refdate);
TQ_tab.amp_NP_Z(TQ_tab.start < refdate) = TQ_tab.amp_HV_NPB_HHZ(TQ_tab.start < refdate);

TQ_tab.phase_NP_E(TQ_tab.start >= refdate) = TQ_tab.phase_HV_NPT_HHE(TQ_tab.start >= refdate);
TQ_tab.phase_NP_N(TQ_tab.start >= refdate) = TQ_tab.phase_HV_NPT_HHN(TQ_tab.start >= refdate);
TQ_tab.phase_NP_Z(TQ_tab.start >= refdate) = TQ_tab.phase_HV_NPT_HHZ(TQ_tab.start >= refdate);
TQ_tab.phase_NP_E(TQ_tab.start < refdate) = TQ_tab.phase_HV_NPB_HHE(TQ_tab.start < refdate);
TQ_tab.phase_NP_N(TQ_tab.start < refdate) = TQ_tab.phase_HV_NPB_HHN(TQ_tab.start < refdate);
TQ_tab.phase_NP_Z(TQ_tab.start < refdate) = TQ_tab.phase_HV_NPB_HHZ(TQ_tab.start < refdate);

TQ_tab.errphase_E(TQ_tab.start >= refdate) = TQ_tab.errphase_HV_NPT_HHE(TQ_tab.start >= refdate);
TQ_tab.errphase_N(TQ_tab.start >= refdate) = TQ_tab.errphase_HV_NPT_HHN(TQ_tab.start >= refdate);
TQ_tab.errphase_Z(TQ_tab.start >= refdate) = TQ_tab.errphase_HV_NPT_HHZ(TQ_tab.start >= refdate);
TQ_tab.errphase_E(TQ_tab.start < refdate) = TQ_tab.errphase_HV_NPB_HHE(TQ_tab.start < refdate);
TQ_tab.errphase_N(TQ_tab.start < refdate) = TQ_tab.errphase_HV_NPB_HHN(TQ_tab.start < refdate);
TQ_tab.errphase_Z(TQ_tab.start < refdate) = TQ_tab.errphase_HV_NPB_HHZ(TQ_tab.start < refdate);

TQ_tab.A_single_station_plot = TQ_tab.A_single_station;
TQ_tab.A_single_station_plot(isnan(TQ_tab.A_single_station)) = 0;
TQ_tab.A_single_station_plot = log10(TQ_tab.A_single_station_plot);
TQ_tab.A_single_station_plot(TQ_tab.A_single_station_plot < -7) = -7;

%calculate first motion
variablenames = TQ_tab.Properties.VariableNames;
fm_z_ind = false(size(variablenames));
for ii = 1:length(variablenames)
    if length(variablenames{ii}) == 13 || length(variablenames{ii}) == 14
        if strcmp(variablenames{ii}(1:3),"fm_") && strcmp(variablenames{ii}(end-3:end),"_HHZ")
            fm_z_ind(ii) = true;
        end
    end
end
TQ_tab.fm_z_mode = mode(sign(TQ_tab{:,fm_z_ind}),2);
TQ_tab.fm_z_mode(isnan(TQ_tab.fm_z_mode)) = 0;

'!!!setting poorly determined polarity to undetermined!!!';
VLP_tab_modes.fm_z_mode(TQ_tab.fm_STALTA < 2.0) = 0;
VLP_tab_modes.fm_z_mode(TQ_tab.fm_Afrac < 0.6) = 0;


%% event density and T,Q derivatives for cr mode
% label cr mode
TQ_tab.cr = false(height(TQ_tab),1);
%T and time blocks to include
TQ_tab.cr(TQ_tab.start > datetime(2011,10,1) & ...
    TQ_tab.start < datetime(2018,7,1) & ...
    TQ_tab.T > 34.8 & TQ_tab.T < 45) = true;
TQ_tab.cr(TQ_tab.start > datetime(2011,7,7) & ...
    TQ_tab.start < datetime(2011,10,1) & ...
    TQ_tab.T > 19 & TQ_tab.T < 24) = true;
TQ_tab.cr(TQ_tab.start > datetime(2011,2,1) & ...
    TQ_tab.start < datetime(2011,3,1) & ...
    TQ_tab.T > 32 & TQ_tab.T < 37) = true;
TQ_tab.cr(TQ_tab.start > datetime(2010,9,1) & ...
    TQ_tab.start < datetime(2011,2,1) & ...
    TQ_tab.T > 28 & TQ_tab.T < 34) = true;
TQ_tab.cr(TQ_tab.start > datetime(2010,3,1) & ...
    TQ_tab.start < datetime(2010,9,1) & ...
    TQ_tab.T > 22 & TQ_tab.T < 32) = true;
TQ_tab.cr(TQ_tab.start > datetime(2009,8,23) & ...
    TQ_tab.start < datetime(2010,3,1) & ...
    TQ_tab.T > 16 & TQ_tab.T < 24) = true;
TQ_tab.cr(TQ_tab.start > datetime(2009,1,15) & ...
    TQ_tab.start < datetime(2009,8,23) & ...
    TQ_tab.T > 10 & TQ_tab.T < 20) = true;
TQ_tab.cr(TQ_tab.start > datetime(2008,7,12) & ...
    TQ_tab.start < datetime(2008,10,15) & ...
    TQ_tab.T > 20.5 & TQ_tab.T < 25.1) = true;
%blocks to exclude
TQ_tab.cr(TQ_tab.start > datetime(2012,2,2) & ...
    TQ_tab.start < datetime(2012,2,3)) = false;

%make separate table for now
keepind = TQ_tab.cr &...
    TQ_tab.amp_abovenoise > log10(3.0) & ...
    TQ_tab.std_abovenoise > log10(3.0) & ...
    TQ_tab.Q > 6 & ...
    TQ_tab.mean_min50_errphase < 0.1 &...
    TQ_tab.channelCount > 2 & ...
    (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2);
VLP_tab_cr = TQ_tab(keepind,:);
VLP_tab_cr = sortrows(VLP_tab_cr,'start');
% event densities
cr_struct = struct();
cr_struct.date = VLP_tab_cr.start(1):days(2):VLP_tab_cr.start(end);
% plotdatenum = datenum(plotdatetime);
cr_struct.density_7 = NaN(size(cr_struct.date));
cr_struct.density_30 = NaN(size(cr_struct.date));
cr_struct.T_7 = NaN(size(cr_struct.date));
cr_struct.T_30 = NaN(size(cr_struct.date));
cr_struct.Q_7 = NaN(size(cr_struct.date));
cr_struct.Q_30 = NaN(size(cr_struct.date));
for ii = 1:length(cr_struct.date)
    cr_struct.density_7(ii) = length(find(VLP_tab_cr.start >= cr_struct.date(ii) - days(7/2) & ...
        VLP_tab_cr.start <= cr_struct.date(ii) + days(7/2)))/7;
    cr_struct.density_30(ii) = length(find(VLP_tab_cr.start >= cr_struct.date(ii) - days(30/2) & ...
        VLP_tab_cr.start <= cr_struct.date(ii) + days(30/2)))/30;
    
    cr_struct.T_7(ii) = mean(VLP_tab_cr.T(VLP_tab_cr.start >= cr_struct.date(ii) - days(7/2) & ...
        VLP_tab_cr.start <= cr_struct.date(ii) + days(7/2)));
    cr_struct.T_30(ii) = mean(VLP_tab_cr.T(VLP_tab_cr.start >= cr_struct.date(ii) - days(30/2) & ...
        VLP_tab_cr.start <= cr_struct.date(ii) + days(30/2)));
    
    cr_struct.Q_7(ii) = mean(VLP_tab_cr.Q(VLP_tab_cr.start >= cr_struct.date(ii) - days(7/2) & ...
        VLP_tab_cr.start <= cr_struct.date(ii) + days(7/2)));
    cr_struct.Q_30(ii) = mean(VLP_tab_cr.Q(VLP_tab_cr.start >= cr_struct.date(ii) - days(30/2) & ...
        VLP_tab_cr.start <= cr_struct.date(ii) + days(30/2)));
end

cr_struct.dT_dt_7 = diff(cr_struct.T_7)./days(diff(cr_struct.date));
VLP_tab_cr.dT_dt_7 = interp1(cr_struct.date(1:end-1) + (cr_struct.date(2:end) - cr_struct.date(1:end-1))/2,...
    cr_struct.dT_dt_7, VLP_tab_cr.start);
cr_struct.dT_dt_30 = diff(cr_struct.T_30)./days(diff(cr_struct.date));
VLP_tab_cr.dT_dt_30 = interp1(cr_struct.date(1:end-1) + (cr_struct.date(2:end) - cr_struct.date(1:end-1))/2,...
    cr_struct.dT_dt_30, VLP_tab_cr.start);
cr_struct.dQ_dt_7 = diff(cr_struct.Q_7)./days(diff(cr_struct.date));
VLP_tab_cr.dQ_dt_7 = interp1(cr_struct.date(1:end-1) + (cr_struct.date(2:end) - cr_struct.date(1:end-1))/2,...
    cr_struct.dQ_dt_7, VLP_tab_cr.start);
cr_struct.dQ_dt_30 = diff(cr_struct.Q_30)./days(diff(cr_struct.date));
VLP_tab_cr.dQ_dt_30 = interp1(cr_struct.date(1:end-1) + (cr_struct.date(2:end) - cr_struct.date(1:end-1))/2,...
    cr_struct.dQ_dt_30, VLP_tab_cr.start);
VLP_tab_cr.density_7 = interp1(cr_struct.date,cr_struct.density_7,VLP_tab_cr.start);
VLP_tab_cr.density_30 = interp1(cr_struct.date,cr_struct.density_30,VLP_tab_cr.start);

%merge back with main table
VLP_tab_noncr = TQ_tab(~keepind,:);
TQ_tab = outerjoin(VLP_tab_noncr, VLP_tab_cr,'MergeKeys',true);
clear VLP_tab_noncr VLP_tab_cr

%% lake mode
% label lake mode
TQ_tab.lake = false(height(TQ_tab),1);
%T and time blocks to include
TQ_tab.lake(TQ_tab.start > datetime(2011,11,1) & ...
    TQ_tab.start < datetime(2018,7,1) & ...
    TQ_tab.T > 14 & TQ_tab.T < 20) = true;
%blocks to exclude
TQ_tab.lake(TQ_tab.start > datetime(2015,8,1) & ...
    TQ_tab.start < datetime(2018,7,1) & ...
    TQ_tab.T < 16.5) = false;

%make separate table for now
keepind = TQ_tab.lake &...
    TQ_tab.amp_abovenoise > log10(3.0) & ...
    TQ_tab.std_abovenoise > log10(3.0) & ...
    TQ_tab.Q > 6 & ...
    TQ_tab.mean_min50_errphase < 0.1 &...
    TQ_tab.channelCount > 2 & ...
    (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2);
VLP_tab_lake = TQ_tab(keepind,:);
VLP_tab_lake = sortrows(VLP_tab_lake,'start');
% event densities
lake_struct = struct();
lake_struct.date = VLP_tab_lake.start(1):days(2):VLP_tab_lake.start(end);
% plotdatenum = datenum(plotdatetime);
lake_struct.density_60 = NaN(size(lake_struct.date));
lake_struct.density_120 = NaN(size(lake_struct.date));
lake_struct.T_60 = NaN(size(lake_struct.date));
lake_struct.T_120 = NaN(size(lake_struct.date));
lake_struct.Q_60 = NaN(size(lake_struct.date));
lake_struct.Q_120 = NaN(size(lake_struct.date));
for ii = 1:length(lake_struct.date)
    lake_struct.density_60(ii) = length(find(VLP_tab_lake.start >= lake_struct.date(ii) - days(60/2) & ...
        VLP_tab_lake.start <= lake_struct.date(ii) + days(60/2)))/60;
    lake_struct.density_120(ii) = length(find(VLP_tab_lake.start >= lake_struct.date(ii) - days(120/2) & ...
        VLP_tab_lake.start <= lake_struct.date(ii) + days(120/2)))/120;
    
    lake_struct.T_60(ii) = mean(VLP_tab_lake.T(VLP_tab_lake.start >= lake_struct.date(ii) - days(60/2) & ...
        VLP_tab_lake.start <= lake_struct.date(ii) + days(60/2)));
    lake_struct.T_120(ii) = mean(VLP_tab_lake.T(VLP_tab_lake.start >= lake_struct.date(ii) - days(120/2) & ...
        VLP_tab_lake.start <= lake_struct.date(ii) + days(120/2)));
    
    lake_struct.Q_60(ii) = mean(VLP_tab_lake.Q(VLP_tab_lake.start >= lake_struct.date(ii) - days(60/2) & ...
        VLP_tab_lake.start <= lake_struct.date(ii) + days(60/2)));
    lake_struct.Q_120(ii) = mean(VLP_tab_lake.Q(VLP_tab_lake.start >= lake_struct.date(ii) - days(120/2) & ...
        VLP_tab_lake.start <= lake_struct.date(ii) + days(120/2)));
end

lake_struct.dT_dt_60 = diff(lake_struct.T_60)./days(diff(lake_struct.date));
VLP_tab_lake.dT_dt_60 = interp1(lake_struct.date(1:end-1) + (lake_struct.date(2:end) - lake_struct.date(1:end-1))/2,...
    lake_struct.dT_dt_60, VLP_tab_lake.start);
lake_struct.dT_dt_120 = diff(lake_struct.T_120)./days(diff(lake_struct.date));
VLP_tab_lake.dT_dt_120 = interp1(lake_struct.date(1:end-1) + (lake_struct.date(2:end) - lake_struct.date(1:end-1))/2,...
    lake_struct.dT_dt_120, VLP_tab_lake.start);
lake_struct.dQ_dt_60 = diff(lake_struct.Q_60)./days(diff(lake_struct.date));
VLP_tab_lake.dQ_dt_60 = interp1(lake_struct.date(1:end-1) + (lake_struct.date(2:end) - lake_struct.date(1:end-1))/2,...
    lake_struct.dQ_dt_60, VLP_tab_lake.start);
lake_struct.dQ_dt_120 = diff(lake_struct.Q_120)./days(diff(lake_struct.date));
VLP_tab_lake.dQ_dt_120 = interp1(lake_struct.date(1:end-1) + (lake_struct.date(2:end) - lake_struct.date(1:end-1))/2,...
    lake_struct.dQ_dt_120, VLP_tab_lake.start);
VLP_tab_lake.density_60 = interp1(lake_struct.date,lake_struct.density_60,VLP_tab_lake.start);
VLP_tab_lake.density_120 = interp1(lake_struct.date,lake_struct.density_120,VLP_tab_lake.start);

%merge back with main table
VLP_tab_nonlake = TQ_tab(~keepind,:);
TQ_tab = outerjoin(VLP_tab_nonlake, VLP_tab_lake,'MergeKeys',true);
clear VLP_tab_nonlake VLP_tab_lake


%% modeled conduit reservoir mode nonlin solve
%{
addpath(genpath('conduit_reservoir_oscillator/mains'))
addpath(genpath('conduit_reservoir_oscillator/source'))

keepind = TQ_tab.cr &...
    TQ_tab.amp_abovenoise > log10(3.0) & ...
    TQ_tab.std_abovenoise > log10(3.0) & ...
    TQ_tab.Q > 6 & ...
    TQ_tab.mean_min50_errphase < 0.1 &...
    TQ_tab.channelCount > 0 & ...
    (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2) & ...
    TQ_tab.start > datetime(2009,6,1) & ...
    TQ_tab.start < datetime(2018,6,1);
% keepind = TQ_tab.cr &...
%     TQ_tab.amp_abovenoise > log10(3.0) & ...
%     TQ_tab.std_abovenoise > log10(3.0) & ...
%     TQ_tab.Q > 6 & ...
%     TQ_tab.mean_min50_errphase < 0.1 &...
%     TQ_tab.channelCount > 5 & ...
%     (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2);
VLP_tab_cr = TQ_tab(keepind,:);

% '!!!only keeping subset of events!!!'
% VLP_tab_cr = VLP_tab_cr(1:20:end,:);

VLP_tab_cr = sortrows(VLP_tab_cr,'start');

Zrescentroid = 1100-1940; %reservoir centroid elev (m asl) from kyle
Zlakebot = 700; %lake floor elev (m asl)
G = 3.08E9; %rock shear modulus (Pa)
conddip = 90;
Rres = 1250;
Rcondtop = 10;
Rcondbot = 10;
betam_res = 5E-10;

Zcondbot = Zrescentroid + Rres;
Lcond = Zlakebot - Zcondbot;

VLP_tab_cr.P_restop = interp1(P_table.Time,P_table.P,VLP_tab_cr.start,'linear');
% max_P_restop = max(VLP_tab_cr.lakeh - (Zrescentroid + Rres))*9.8*1600; %maximim possible pressure
% shift = max_P_restop - max(VLP_tab_cr.P_restop);
shift = (700 - (Zrescentroid + Rres))*9.8*800;
VLP_tab_cr.P_restop = VLP_tab_cr.P_restop + shift;

%y is [mu; rho_condbot; rho_condtop]
min_rho_laketop = 300;
direct_ymin = [2E-1; 800; min_rho_laketop];
direct_ymax = [2E4; 3000; 2600];
direct_yguess = [2E1; 2400; 800];
direct_yscale = [1;1;1];
direct_lsqnonlinoptions = optimset('TolFun',1e-7,... %recommend 1e-6 or less
    'TolX',1E-4,...%recommend 1e-4 or less
    'FinDiffType','forward',...
    'MaxFunEvals',3000,...
    'MaxIter',400,...
    'PlotFcns',[],...{'optimplotx','optimplotfval','optimplotresnorm','optimplotfunccount'},...
    'Display','off',...
    'UseParallel',true);


mu_cell = cell(height(VLP_tab_cr),1);
rho_condbot_cell = cell(height(VLP_tab_cr),1);
rho_condtop_cell = cell(height(VLP_tab_cr),1);
direct_resnorm_cell = cell(height(VLP_tab_cr),1);
direct_yprev = direct_yguess;
numcores = feature('numcores'); 
poolobj = gcp('nocreate'); 
if isempty(poolobj)
    parpool(numcores)
end
% 'not using parfor'
tic
for ii = 1:height(VLP_tab_cr)
    
    if mod(ii,100) == 1
        disp(ii)
        toc
    end
    
    Rlake = sqrt(VLP_tab_cr.lakeA(ii)*10^4/pi);
%     if isnan(Rlake)
%         Rlake = sqrt(nanmax(VLP_tab_cr.lakeA)*10^4/pi);
%     end
    Zlaketop = VLP_tab_cr.lakeh(ii);
    
    Hlake = Zlaketop - Zlakebot;
    Hcolumn = Hlake + Lcond;
    
    if isfinite(VLP_tab_cr.P_restop(ii)) && isfinite(Rlake) && isfinite(Hlake)
        [y,resnorm,residual,exitflag,output] = lsqnonlin(@(y) direct_costfun(y, direct_yscale,...
            VLP_tab_cr.T(ii),VLP_tab_cr.Q(ii), VLP_tab_cr.P_restop(ii),...
            conddip,...%conduit dip angle (deg)
            Rres,...%reservoir radius
            Rcondtop,... %top conduit radius
            Rcondbot,... %bottom conduit radius
            G,... %elastic wall rock shear modulus
            Rlake,...%lake radius
            Hcolumn,... %conduit depth (including lake)
            Hlake,...%lake height
            betam_res,... %reservoir compressibility
            min_rho_laketop),...
            direct_yprev, direct_ymin, direct_ymax, direct_lsqnonlinoptions);

        y = y.*direct_yscale;
        mu_cell{ii} = y(1);
        rho_condbot_cell{ii} = y(2);
        rho_condtop_cell{ii} = y(3);
        direct_resnorm_cell{ii} = resnorm;
        direct_yprev = y; %use previous solution as guess
    else
        mu_cell{ii} = NaN;
        rho_condbot_cell{ii} = NaN;
        rho_condtop_cell{ii} = NaN;
        direct_resnorm_cell{ii} = NaN;
    end
end
toc

VLP_tab_cr.mu = cell2mat(mu_cell);
VLP_tab_cr.rho_condbot = cell2mat(rho_condbot_cell);
VLP_tab_cr.rho_condtop = cell2mat(rho_condtop_cell);
VLP_tab_cr.direct_resnorm = cell2mat(direct_resnorm_cell);

% save('direct_inv_VLP_tab_cr_P1600_GPS_SCR_ar1_d4000_mu3_Rc20_Rr1250.mat','VLP_tab_cr');

%% nonlin sol plot preprocess
% VLP_tab_cr = load('direct_inv_VLP_tab_cr_P1600_GPS_SCR_ar1_d5000_mu3.mat');

cr_table = (VLP_tab_cr.start(1):days(1):VLP_tab_cr.start(end))';
cr_table = timetable([cr_table(:)],NaN(size(cr_table)),'VariableNames',{'T_7'});
% cr_table.T_7 = NaN(size(cr_table.Time));
cr_table.Q_7 = NaN(size(cr_table.Time));
cr_table.rho_condtop_7 = NaN(size(cr_table.Time));
cr_table.rho_condbot_7 = NaN(size(cr_table.Time));
cr_table.rho_7 = NaN(size(cr_table.Time));
cr_table.drho_7 = NaN(size(cr_table.Time));
cr_table.mu_7 = NaN(size(cr_table.Time));
cr_table.h_7 = NaN(size(cr_table.Time));
cr_table.P_7 = NaN(size(cr_table.Time));
Tspan = days(30/2);

VLP_tab_cr.rho = (VLP_tab_cr.rho_condbot + VLP_tab_cr.rho_condtop)/2;
VLP_tab_cr.drho = VLP_tab_cr.rho_condbot - VLP_tab_cr.rho_condtop;

for ii = 1:length(cr_table.Time)  
    datain = VLP_tab_cr.start >= cr_table.Time(ii) - Tspan & ...
        VLP_tab_cr.start <= cr_table.Time(ii) + Tspan;
    dist = abs(VLP_tab_cr.start(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    cr_table.T_7(ii) = sum(VLP_tab_cr.T(datain).*weight)/sum(weight);
    cr_table.Q_7(ii) = sum(VLP_tab_cr.Q(datain).*weight)/sum(weight);
    
    datain = datain & ~isnan(VLP_tab_cr.rho_condtop);
    dist = abs(VLP_tab_cr.start(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    cr_table.rho_condtop_7(ii) = sum(VLP_tab_cr.rho_condtop(datain).*weight)/sum(weight);
    cr_table.rho_condbot_7(ii) = sum(VLP_tab_cr.rho_condbot(datain).*weight)/sum(weight);
    cr_table.rho_7(ii) = sum(VLP_tab_cr.rho(datain).*weight)/sum(weight);
    cr_table.drho_7(ii) = sum(VLP_tab_cr.drho(datain).*weight)/sum(weight);
    cr_table.mu_7(ii) = sum(VLP_tab_cr.mu(datain).*weight)/sum(weight);
%     cr_table.T_7(ii) = mean(VLP_tab_cr.T(datain));
%     cr_table.Q_7(ii) = mean(VLP_tab_cr.Q(datain));
%     cr_table.rho_condtop_7(ii) = mean(VLP_tab_cr.rho_condtop(datain));
%     cr_table.rho_condbot_7(ii) = mean(VLP_tab_cr.rho_condbot(datain));
%     cr_table.mu_7(ii) = mean(VLP_tab_cr.mu(datain));
    
    datain = lakeh_resample.Time >= cr_table.Time(ii) - Tspan & ...
        lakeh_resample.Time <= cr_table.Time(ii) + Tspan;
    dist = abs(lakeh_resample.Time(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    cr_table.h_7(ii) = sum(lakeh_resample.h(datain).*weight)/sum(weight);
%     cr_table.h_7(ii) = mean(lakeh.h(datain));
    
    datain = P_table.Time >= cr_table.Time(ii) - Tspan & ...
        P_table.Time <= cr_table.Time(ii) + Tspan;
    dist = abs(P_table.Time(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    cr_table.P_7(ii) = sum(P_table.P(datain).*weight)/sum(weight)+shift;
%     cr_table.P_7(ii) = mean(P_table.P(datain))+shift;
end

%% nonlin sol plot
figure()
Tlim = [10,43];
Xlim = [datetime(2009,6,1),datetime(2018,6,1)];
heightincrease = 0.02;
plotL = 0.07;
plotw = 0.88;

ax1 = subplot('Position',[plotL,0.8,plotw,0.17]);
ax1.Position = ax1.Position+[0,0,0,heightincrease];
%fill(inflateplotx,inflateploty*100,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
yyaxis('left')
scatter(VLP_tab_cr.start,VLP_tab_cr.T,6,[0.5,0.5,1],'filled'), hold on
plot(cr_table.Time,cr_table.T_7,'-','LineWidth',1.5,'Color',[0,0,0.7])
grid on, grid minor, box on
ylabel('period (s)')
ylim(Tlim)
xlim(Xlim);%max(VLP_tab.start)])
yyaxis('right')
scatter(VLP_tab_cr.start,VLP_tab_cr.Q,6,[1,0.5,0.5],'filled'), hold on
plot(cr_table.Time,cr_table.Q_7,'-','LineWidth',1.5,'Color',[0.7,0,0])
grid on, grid minor, box on
ylabel('quality factor')
ylim([5,70])
xline(datetime(2008,3,19),'k-')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('a.'); ttl.Position(1) = ttl.Position(1)/20;

ax2 = subplot('Position',[plotL,0.6,plotw,0.17]);
ax2.Position = ax2.Position+[0,0,0,heightincrease];
%fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
yyaxis('left')
p1 = plot(lakeh.date,lakeh.h,'-','Color',[0.5,0.5,1],'MarkerSize',3); hold on
plot(cr_table.Time,cr_table.h_7,'-','Color',[0,0,0.7]);
ylabel('lava lake el. (m ASL)')
hold on
grid on, grid minor, box on
ylim([750,1040])
yyaxis('right')
%p2 = scatter(VLP_tab_cr.start,VLP_tab_cr.P_restop,'r.');
p2 = plot(P_table.Time,P_table.P+shift,'-','Color',[1,0.5,0.5],'MarkerSize',3); hold on
plot(cr_table.Time,cr_table.P_7,'-','Color',[0.7,0,0]);
% xline(datetime(2014,7,23,20,14,0),'Color','m','LineWidth',2)
% xline(datetime(2015,10,25,0,10,0),'Color','m','LineWidth',2)
ylabel('HMMR pressure (Pa)')
ylim([1.4E6,8.1E6])
xlim(Xlim)
xline(datetime(2008,3,19),'k-')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('b.'); ttl.Position(1) = ttl.Position(1)/20;

ax3 = subplot('Position',[plotL,0.4,plotw,0.17]);
ax3.Position = ax3.Position+[0,0,0,heightincrease];
%fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
p1 = plot(VLP_tab_cr.start,VLP_tab_cr.mu,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
plot(cr_table.Time,cr_table.mu_7,'k','LineWidth',1)
% scatter(VLP_tab_cr.start,VLP_tab_cr.mu,9,VLP_tab_cr.direct_resnorm,'filled','MarkerEdgeColor','k')
% colormap(ax3,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
ylabel('magma viscosity (Pas)')
hold on
grid on, grid minor, box on
xlim(Xlim)
xline(datetime(2008,3,19),'k-')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
set(gca,'YScale','Log')
ttl = title('c.'); ttl.Position(1) = ttl.Position(1)/20;

ax4 = subplot('Position',[plotL,0.05,plotw,0.32]);
ax4.Position = ax4.Position+[0,0,0,heightincrease];
%fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
plot(VLP_tab_cr.start,VLP_tab_cr.rho_condtop,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.4,0.4,1]); hold on
p2 = plot(cr_table.Time,cr_table.rho_condtop_7,'-','LineWidth',1,'Color',[0.2,0.2,1]);
plot(VLP_tab_cr.start,VLP_tab_cr.rho_condbot,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.7,0.7,1]); hold on
p3 = plot(cr_table.Time,cr_table.rho_condbot_7,'-','LineWidth',1,'Color',[0.5,0.5,1]);
plot(VLP_tab_cr.start,VLP_tab_cr.drho,'.','LineWidth',0.5,'MarkerSize',6,'Color',[1,0,0]); hold on
p4 = plot(cr_table.Time,cr_table.drho_7,'-','LineWidth',1,'Color',[0.5,0,0]);
plot(VLP_tab_cr.start,VLP_tab_cr.rho,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
p1 = plot(cr_table.Time,cr_table.rho_7,'k','LineWidth',1,'Color',[0,0,0]);
legend([p2,p1,p3,p4],{'conduit top','conduit average','conduit bottom','conduit difference (top-bottom)'},'AutoUpdate','off')
ylabel('magma density (kg/m^3)')
ylim([90,2600])
hold on
grid on, grid minor, box on
xlim(Xlim)
xline(datetime(2008,3,19),'k-')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
% xticklabels([])
ttl = title('d.'); ttl.Position(1) = ttl.Position(1)/20;

% ax5 = subplot('Position',[plotL,0.08,plotw,0.14]);
% ax5.Position = ax5.Position+[0,0,0,heightincrease];
% %fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
% %hold on
% p1 = plot(VLP_tab_cr.start,-VLP_tab_cr.drho,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
% plot(cr_table.Time,-cr_table.drho_7,'k','LineWidth',1)
% % scatter(VLP_tab_cr.start,VLP_tab_cr.rho_condtop,9,VLP_tab_cr.direct_resnorm,'filled','MarkerEdgeColor','k');
% % colormap(ax5,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
% ylabel('conduit density\newlinedifference (kg/m^3)')
% hold on
% grid on, grid minor, box on
% xlim(Xlim)
% xline(datetime(2008,3,19),'k-')%,{'Crater'},'LabelVerticalAlignment','bottom')
% xline(datetime(2010,2,1),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2012,5,25),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2015,10,15),'k:')%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,3,5),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,8,3),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,9,21),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2014,6,27),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2016,5,24),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2018,4,30),'k-')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2012,10,26),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
% xline(datetime(2014,5,10),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
% xline(datetime(2015,5,9),'k--')%,{'Int'},'LabelVerticalAlignment','bottom')
% set(gca,'Layer','top')
% % xticklabels([])
% ttl = title('e.'); ttl.Position(1) = ttl.Position(1)/20;
%}

%% volatile modeled conduit reservoir mode nonlin solve
%{
addpath(genpath('conduit_reservoir_oscillator/mains'))
addpath(genpath('conduit_reservoir_oscillator/source'))

keepind = TQ_tab.cr &...
    TQ_tab.amp_abovenoise > log10(3.0) & ...
    TQ_tab.std_abovenoise > log10(3.0) & ...
    TQ_tab.Q > 6 & ...
    TQ_tab.mean_min50_errphase < 0.1 &...
    TQ_tab.channelCount > 0 & ...
    (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2) & ...
    TQ_tab.start > datetime(2009,6,1) & ...
    TQ_tab.start < datetime(2018,7,1);
% keepind = TQ_tab.cr &...
%     TQ_tab.amp_abovenoise > log10(3.0) & ...
%     TQ_tab.std_abovenoise > log10(3.0) & ...
%     TQ_tab.Q > 6 & ...
%     TQ_tab.mean_min50_errphase < 0.1 &...
%     TQ_tab.channelCount > 5 & ...
%     (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2);
VLP_tab_cr = TQ_tab(keepind,:);

'!!!only keeping subset of events!!!'
VLP_tab_cr = VLP_tab_cr(1:30:end,:);

VLP_tab_cr = sortrows(VLP_tab_cr,'start');

Zrescentroid = 1100-1940; %reservoir centroid elev (m asl) from kyle
Zlakebot = 700; %lake floor elev (m asl)
G = 3.08E9; %rock shear modulus (Pa)
conddip = 90;
Rres = 1250;
Rcondtop = 10;
Rcondbot = 10;

vary_CO2 = false;
vary_H2O = false;
fixed_ratio = true;
vary_lake = false;
if vary_CO2
    H2O_condtop_wtp = 0.1; %wtp
    H2O_condbot_wtp = 0.1; %wtp
    H2O_laketop_wtp = 0.1; %wtp
    H2O_lakebot_wtp = 0.1; %wtp
elseif vary_H2O
    CO2_condtop_ppm = 1000; %ppm
    CO2_condbot_ppm = 1000; %ppm
    CO2_laketop_ppm = 1000; %ppm
    CO2_lakebot_ppm = 1000; %ppm
elseif fixed_ratio
    H2O_wtp_frac = 0.5; %mass fraction h20 (out of total vol)
    fixed_laketop = true;
    if fixed_laketop
        fixed_vol_laketop_wtp = 1.4; %wtp
    end
elseif vary_lake
    H2O_wtp_frac = 0.5;
end

Zcondbot = Zrescentroid + Rres;
Lcond = Zlakebot - Zcondbot;

VLP_tab_cr.P_restop = interp1(P_table.Time,P_table.P,VLP_tab_cr.start,'linear');
% max_P_restop = max(VLP_tab_cr.lakeh - (Zrescentroid + Rres))*9.8*1600; %maximim possible pressure
% shift = max_P_restop - max(VLP_tab_cr.P_restop);
shift = (700 - (Zrescentroid + Rres))*9.8*1000;
VLP_tab_cr.P_restop = VLP_tab_cr.P_restop + shift;

CO2solppm_table = csvread('new_Values_CO2solubility_ppm_comb.csv');
H2Osolwtp_table = csvread('new_Values_H2Osolubility_wtp_comb.csv');
dz = 0.4; %depth increment for integration, recommend 0.4 or less
TolX = 1E-9; %for fzero in omega calc, has minimal impact on run time, recommend 1E-8 or less

if vary_CO2
    %y is [TC; CO2_condbot; CO2_condtop]
    volatile_ymin = [900; 1E0; 1E0];
    volatile_ymax = [1500; 10E5; 10E5];
    volatile_yguess = [1200; 1E4; 1E4];
    volatile_yscale = [1; 1; 1];
elseif vary_H2O
    %y is [TC; h2o_condbot; h2o_condtop] 
    volatile_yscale = [1; 1E-3; 1E-3];
    volatile_ymin = [900; 1E-2; 1E-2]./volatile_yscale;
    volatile_ymax = [1500; 5; 5]./volatile_yscale;
    volatile_yguess = [1200; 0.5; 0.5]./volatile_yscale;
elseif fixed_ratio
    %y is [TC; vol_condbot; vol_condtop] 
    volatile_yscale = [1; 1E-3; 1E-3];
    volatile_ymin = [900; 1E-2; 1E-2]./volatile_yscale;
    volatile_ymax = [1500; 5; 5]./volatile_yscale;
    volatile_yguess = [1200; 0.5; 0.5]./volatile_yscale;
elseif vary_lake
    %y is [TC; vol_cond; vol_lake] 
    volatile_yscale = [1; 1E-3; 1E-3];
    volatile_ymin = [900; 1E-2; 1E-2]./volatile_yscale;
    volatile_ymax = [1500; 5; 5]./volatile_yscale;
    volatile_yguess = [1200; 0.5; 0.5]./volatile_yscale;
end
% 'not using parallell'
volatile_lsqnonlinoptions = optimset('TolFun',1e-6,... %recommend 1e-6 or less
    'TolX',1E-2,...%recommend 1e-2 or less
    'FinDiffType','forward',...
    'MaxFunEvals',3000,...
    'MaxIter',400,...
    'PlotFcns',[],...{'optimplotx','optimplotfval','optimplotresnorm','optimplotfunccount'},...
    'Display','off',...
    'UseParallel',true);

TC_cell = cell(height(VLP_tab_cr),1);
if vary_CO2
    CO2_condbot_cell = cell(height(VLP_tab_cr),1);
    CO2_condtop_cell = cell(height(VLP_tab_cr),1);
    % CO2_laketop_cell = cell(height(VLP_tab_cr),1);
elseif vary_H2O
    H2O_condbot_cell = cell(height(VLP_tab_cr),1);
    H2O_condtop_cell = cell(height(VLP_tab_cr),1);
    % H2O_laketop_cell = cell(height(VLP_tab_cr),1);
elseif fixed_ratio
    vol_condbot_cell = cell(height(VLP_tab_cr),1);
    vol_condtop_cell = cell(height(VLP_tab_cr),1);
    % vol_laketop_cell = cell(height(VLP_tab_cr),1);
elseif vary_lake
    vol_cond_cell = cell(height(VLP_tab_cr),1);
    vol_lake_cell = cell(height(VLP_tab_cr),1);
end

volatile_resnorm_cell = cell(height(VLP_tab_cr),1);
volatile_yprev = volatile_yguess;
numcores = feature('numcores'); 
poolobj = gcp('nocreate'); 
if isempty(poolobj)
    parpool(numcores)
end
% 'not using parfor'
t1 = tic;
for ii = 1:height(VLP_tab_cr)
    
    if mod(ii,100) == 1
        disp(ii)
        toc(t1)
    end
    
    Rlake = sqrt(VLP_tab_cr.lakeA(ii)*10^4/pi);
%     if isnan(Rlake)
%         Rlake = sqrt(nanmax(VLP_tab_cr.lakeA)*10^4/pi);
%     end
    Zlaketop = VLP_tab_cr.lakeh(ii);
    
    Hlake = Zlaketop - Zlakebot;
    Hcolumn = Hlake + Lcond;
    if vary_CO2
        if isfinite(VLP_tab_cr.P_restop(ii)) && isfinite(Rlake) && isfinite(Hlake)
            [y,resnorm,residual,exitflag,output] = lsqnonlin(@(y) volatile_costfun(y, volatile_yscale, ...
                VLP_tab_cr.T(ii), VLP_tab_cr.Q(ii), VLP_tab_cr.P_restop(ii),...
                conddip,...%conduit dip angle (deg)
                Rres,...%reservoir radius
                Rcondtop,... %conduit top radius
                Rcondbot,... %conduit bottom radius
                G,... %elastic wall rock shear modulus
                Rlake,... %lava lake radius
                Hcolumn,... %conduit depth (including lake)
                H2O_condtop_wtp,... %wtp
                H2O_condbot_wtp,... %wtp
                H2O_laketop_wtp,... %wtp
                H2O_lakebot_wtp,... %wtp
                Hlake,...%lake depth
                false,...
                CO2solppm_table, H2Osolwtp_table,dz,TolX),...
                volatile_yprev, volatile_ymin, volatile_ymax, volatile_lsqnonlinoptions);

            y = y.*volatile_yscale;
            TC_cell{ii} = y(1);
            CO2_condbot_cell{ii} = y(2);
            CO2_condtop_cell{ii} = y(3);
            % CO2_laketop_cell{ii} = y(3);
            volatile_resnorm_cell{ii} = resnorm;
            volatile_yprev = y; %use previous solution as guess
        else
            TC_cell{ii} = NaN;
            CO2_condbot_cell{ii} = NaN;
            CO2_condtop_cell{ii} = NaN;
            % CO2_laketop_cell{ii} = NaN;
            volatile_resnorm_cell{ii} = NaN;
        end
    elseif vary_H2O
        if isfinite(VLP_tab_cr.P_restop(ii)) && isfinite(Rlake) && isfinite(Hlake)
            [y,resnorm,residual,exitflag,output] = lsqnonlin(@(y) volatile_H2O_costfun(y, volatile_yscale, ...
                VLP_tab_cr.T(ii), VLP_tab_cr.Q(ii), VLP_tab_cr.P_restop(ii),...
                conddip,...%conduit dip angle (deg)
                Rres,...%reservoir radius
                Rcondtop,... %conduit top radius
                Rcondbot,... %conduit bottom radius
                G,... %elastic wall rock shear modulus
                Rlake,... %lava lake radius
                Hcolumn,... %conduit depth (including lake)
                CO2_condtop_ppm,... %ppm
                CO2_condbot_ppm,... %ppm
                CO2_laketop_ppm,... %ppm
                CO2_lakebot_ppm,... %ppm
                Hlake,...%lake depth
                false,...
                CO2solppm_table, H2Osolwtp_table,dz,TolX),...
                volatile_yprev, volatile_ymin, volatile_ymax, volatile_lsqnonlinoptions);

            y = y.*volatile_yscale;
            TC_cell{ii} = y(1);
            H2O_condbot_cell{ii} = y(2);
            H2O_condtop_cell{ii} = y(3);
            % H2O_laketop_cell{ii} = y(3);
            volatile_resnorm_cell{ii} = resnorm;
            volatile_yprev = y; %use previous solution as guess
        else
            TC_cell{ii} = NaN;
            H2O_condbot_cell{ii} = NaN;
            H2O_condtop_cell{ii} = NaN;
            % H2O_laketop_cell{ii} = NaN;
            volatile_resnorm_cell{ii} = NaN;
        end
    elseif fixed_ratio
        if isfinite(VLP_tab_cr.P_restop(ii)) && isfinite(Rlake) && isfinite(Hlake)
            [y,resnorm,residual,exitflag,output] = lsqnonlin(@(y) volatile_ratio_costfun(y, volatile_yscale, ...
                VLP_tab_cr.T(ii), VLP_tab_cr.Q(ii), VLP_tab_cr.P_restop(ii),...
                conddip,...%conduit dip angle (deg)
                Rres,...%reservoir radius
                Rcondtop,... %conduit top radius
                Rcondbot,... %conduit bottom radius
                G,... %elastic wall rock shear modulus
                Rlake,... %lava lake radius
                Hcolumn,... %conduit depth (including lake)
                H2O_wtp_frac,...
                Hlake,...%lake depth
                false,...
                CO2solppm_table, H2Osolwtp_table,dz,TolX,...
                fixed_laketop,fixed_vol_laketop_wtp),...
                volatile_yprev, volatile_ymin, volatile_ymax, volatile_lsqnonlinoptions);

            y = y.*volatile_yscale;
            TC_cell{ii} = y(1);
            vol_condbot_cell{ii} = y(2);
            vol_condtop_cell{ii} = y(3);
            % vol_laketop_cell{ii} = y(3);
            volatile_resnorm_cell{ii} = resnorm;
            volatile_yprev = y; %use previous solution as guess
        else
            TC_cell{ii} = NaN;
            vol_condbot_cell{ii} = NaN;
            vol_condtop_cell{ii} = NaN;
            % vol_laketop_cell{ii} = NaN;
            volatile_resnorm_cell{ii} = NaN;
        end
    elseif vary_lake
        if isfinite(VLP_tab_cr.P_restop(ii)) && isfinite(Rlake) && isfinite(Hlake)
            [y,resnorm,residual,exitflag,output] = lsqnonlin(@(y) volatile_lake_costfun(y, volatile_yscale, ...
                VLP_tab_cr.T(ii), VLP_tab_cr.Q(ii), VLP_tab_cr.P_restop(ii),...
                conddip,...%conduit dip angle (deg)
                Rres,...%reservoir radius
                Rcondtop,... %conduit top radius
                Rcondbot,... %conduit bottom radius
                G,... %elastic wall rock shear modulus
                Rlake,... %lava lake radius
                Hcolumn,... %conduit depth (including lake)
                H2O_wtp_frac,...
                Hlake,...%lake depth
                false,...
                CO2solppm_table, H2Osolwtp_table,dz,TolX),...
                volatile_yprev, volatile_ymin, volatile_ymax, volatile_lsqnonlinoptions);

            y = y.*volatile_yscale;
            TC_cell{ii} = y(1);
            vol_cond_cell{ii} = y(2);
            vol_lake_cell{ii} = y(3);
            % vol_laketop_cell{ii} = y(3);
            volatile_resnorm_cell{ii} = resnorm;
            volatile_yprev = y; %use previous solution as guess
        else
            TC_cell{ii} = NaN;
            vol_cond_cell{ii} = NaN;
            vol_lake_cell{ii} = NaN;
            % vol_laketop_cell{ii} = NaN;
            volatile_resnorm_cell{ii} = NaN;
        end
    end
end
toc(t1)

VLP_tab_cr.TC = cell2mat(TC_cell);
if vary_CO2
    VLP_tab_cr.CO2_condbot = cell2mat(CO2_condbot_cell);
    VLP_tab_cr.CO2_condtop = cell2mat(CO2_condtop_cell);
    % VLP_tab_cr.CO2_laketop = cell2mat(CO2_laketop_cell);
elseif vary_H2O
    VLP_tab_cr.H2O_condbot = cell2mat(H2O_condbot_cell);
    VLP_tab_cr.H2O_condtop = cell2mat(H2O_condtop_cell);
    % VLP_tab_cr.H2O_laketop = cell2mat(H2O_laketop_cell);
elseif fixed_ratio
    VLP_tab_cr.vol_condbot = cell2mat(vol_condbot_cell);
    VLP_tab_cr.vol_condtop = cell2mat(vol_condtop_cell);
    % VLP_tab_cr.vol_laketop = cell2mat(vol_laketop_cell);
    VLP_tab_cr.vol = (VLP_tab_cr.vol_condbot + VLP_tab_cr.vol_condtop)/2;
    VLP_tab_cr.dvol = VLP_tab_cr.vol_condbot - VLP_tab_cr.vol_condtop;
elseif vary_lake
    VLP_tab_cr.vol_cond = cell2mat(vol_cond_cell);
    VLP_tab_cr.vol_lake = cell2mat(vol_lake_cell);
end
VLP_tab_cr.volatile_resnorm = cell2mat(volatile_resnorm_cell);

% save('volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250.mat','VLP_tab_cr');
%}

%% volatile nonlin sol plot preprocess
if ~exist('vary_CO2','var')
    vary_CO2 = false;
    vary_H2O = false;
    fixed_ratio = true;
    vary_lake = false;
    load('volatile_inv_VLP_tab_cr_GPS_SCR_ar1_d5000_mu3_HMM_ar1-23_V3-94_P1000_Rc10_Rr1250_fixedlaketop1_8.mat');
    shift = (700 - (-1940+1100 + 1250))*9.8*1000;
end

cr_table = (VLP_tab_cr.start(1):days(1):VLP_tab_cr.start(end))';
cr_table = timetable([cr_table(:)],NaN(size(cr_table)),'VariableNames',{'T_7'});
% cr_table.T_7 = NaN(size(cr_table.Time));
cr_table.Q_7 = NaN(size(cr_table.Time));
if vary_CO2
    cr_table.CO2_condbot_7 = NaN(size(cr_table.Time));
    cr_table.CO2_condtop_7 = NaN(size(cr_table.Time));
    % cr_table.CO2_laketop_7 = NaN(size(cr_table.Time));
elseif vary_H2O
    cr_table.H2O_condbot_7 = NaN(size(cr_table.Time));
    cr_table.H2O_condtop_7 = NaN(size(cr_table.Time));
    % cr_table.H2O_laketop_7 = NaN(size(cr_table.Time));
elseif fixed_ratio
    cr_table.vol_condbot_7 = NaN(size(cr_table.Time));
    cr_table.vol_condtop_7 = NaN(size(cr_table.Time));
    % cr_table.vol_laketop_7 = NaN(size(cr_table.Time));
elseif vary_lake
    cr_table.vol_cond_7 = NaN(size(cr_table.Time));
    cr_table.vol_lake_7 = NaN(size(cr_table.Time));
end
cr_table.TC_7 = NaN(size(cr_table.Time));
cr_table.h_7 = NaN(size(cr_table.Time));
cr_table.P_7 = NaN(size(cr_table.Time));
cr_table.SCR_P_7 = NaN(size(cr_table.Time));

Tspan = days(30/2);

for ii = 1:length(cr_table.Time)  
    datain = VLP_tab_cr.start >= cr_table.Time(ii) - Tspan & ...
        VLP_tab_cr.start <= cr_table.Time(ii) + Tspan;
    dist = abs(VLP_tab_cr.start(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    cr_table.T_7(ii) = sum(VLP_tab_cr.T(datain).*weight)/sum(weight);
    cr_table.Q_7(ii) = sum(VLP_tab_cr.Q(datain).*weight)/sum(weight);
    
    datain = datain & ~isnan(VLP_tab_cr.TC);
    dist = abs(VLP_tab_cr.start(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    if vary_CO2
        cr_table.CO2_condbot_7(ii) = sum(VLP_tab_cr.CO2_condbot(datain).*weight)/sum(weight);
        cr_table.CO2_condtop_7(ii) = sum(VLP_tab_cr.CO2_condtop(datain).*weight)/sum(weight);
        % cr_table.CO2_laketop_7(ii) = sum(VLP_tab_cr.CO2_laketop(datain).*weight)/sum(weight);
    elseif vary_H2O
        cr_table.H2O_condbot_7(ii) = sum(VLP_tab_cr.H2O_condbot(datain).*weight)/sum(weight);
        cr_table.H2O_condtop_7(ii) = sum(VLP_tab_cr.H2O_condtop(datain).*weight)/sum(weight);
        % cr_table.H2O_laketop_7(ii) = sum(VLP_tab_cr.H2O_laketop(datain).*weight)/sum(weight);
    elseif fixed_ratio
        cr_table.vol_condbot_7(ii) = sum(VLP_tab_cr.vol_condbot(datain).*weight)/sum(weight);
        cr_table.vol_condtop_7(ii) = sum(VLP_tab_cr.vol_condtop(datain).*weight)/sum(weight);
        cr_table.vol_7(ii) = sum(VLP_tab_cr.vol(datain).*weight)/sum(weight);
        cr_table.dvol_7(ii) = sum(VLP_tab_cr.dvol(datain).*weight)/sum(weight);
        % cr_table.vol_laketop_7(ii) = sum(VLP_tab_cr.vol_laketop(datain).*weight)/sum(weight);
    elseif vary_lake
        cr_table.vol_cond_7(ii) = sum(VLP_tab_cr.vol_cond(datain).*weight)/sum(weight);
        cr_table.vol_lake_7(ii) = sum(VLP_tab_cr.vol_lake(datain).*weight)/sum(weight);
    end
    cr_table.TC_7(ii) = sum(VLP_tab_cr.TC(datain).*weight)/sum(weight);
    
    datain = lakeh_resample.Time >= cr_table.Time(ii) - Tspan & ...
        lakeh_resample.Time <= cr_table.Time(ii) + Tspan;
    dist = abs(lakeh_resample.Time(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    cr_table.h_7(ii) = sum(lakeh_resample.h(datain).*weight)/sum(weight);
    
    datain = P_table.Time >= cr_table.Time(ii) - Tspan & ...
        P_table.Time <= cr_table.Time(ii) + Tspan;
    dist = abs(P_table.Time(datain) - cr_table.Time(ii));
    weight = 1-dist/Tspan;
    cr_table.P_7(ii) = sum(P_table.P(datain).*weight)/sum(weight)+shift;
    cr_table.P_SCR_7(ii) = sum(P_table.SCR_P(datain).*weight)/sum(weight)+shift;
end

%% volatile nonlin sol plots
figure()
Tlim = [15,43];
Xlim = [datetime(2009,8,1),datetime(2018,6,1)];
heightincrease = 0.02;
plotL = 0.03;
plotw = 0.94;

ax1 = subplot('Position',[plotL,0.84,plotw,0.15]);
yyaxis('left')
scatter(VLP_tab_cr.start,VLP_tab_cr.T,6,[0.5,0.5,1],'filled'), hold on
plot(cr_table.Time,cr_table.T_7,'-','LineWidth',1,'Color',[0,0,0.7])
grid on, grid minor, box on
ylabel('period (s)')
ytickangle(90)
ylim(Tlim)
xlim(Xlim);%max(VLP_tab.start)])
yyaxis('right')
scatter(VLP_tab_cr.start,VLP_tab_cr.Q,6,[1,0.5,0.5],'filled'), hold on
plot(cr_table.Time,cr_table.Q_7,'-','LineWidth',1,'Color',[0.7,0,0])
grid on, grid minor, box on
ylabel('quality factor')
ytickangle(90)
ylim([6,70])
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('a. conduit-reservoir VLP magma resonance');
set(gca,'FontSize',11)

ax2 = subplot('Position',[plotL,0.68,plotw,0.15]);
yyaxis('left')
p1 = plot(lakeh.date,lakeh.h,'-','Color',[0.5,0.5,1],'MarkerSize',3); hold on
plot(cr_table.Time,cr_table.h_7,'-','Color',[0,0,0.7]);
ylabel('elevation (m ASL)')
hold on
grid on, grid minor, box on
ylim([750,1040])
ytickangle(90)
yyaxis('right')
p2 = plot([lakeA.date;datetime(2018,5,4)],sqrt([lakeA.Area;lakeA.Area(end)]*10^4/pi),'.-','Color',[0.7,0,0],'MarkerSize',9); hold on
ylabel('radius (m)')
hold on
grid on, grid minor, box on
ylim([0,145])
ytickangle(90)
xlim(Xlim)
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('b. lava lake');
set(gca,'FontSize',11)

ax3 = subplot('Position',[plotL,0.52,plotw,0.15]);
% yyaxis('left')
% p1 = plot(lakeh.date,lakeh.h,'-','Color',[0.5,0.5,1],'MarkerSize',3); hold on
% plot(cr_table.Time,cr_table.h_7,'-','Color',[0,0,0.7]);
% ylabel('lava lake\newlineelevation (m ASL)')
% hold on
% grid on, grid minor, box on
% ylim([750,1040])
% ytickangle(90)
% yyaxis('right')
%p2 = scatter(VLP_tab_cr.start,VLP_tab_cr.P_restop,'r.');
p4 = plot(P_table.Time,(P_table.int_P)*1e-6,'-','Color',[0.6,0,0.8],'MarkerSize',3); hold on
p3 = plot(P_table.Time,(P_table.SCR_P/2)*1e-6,'-','Color',[1,0.5,0.3],'MarkerSize',3); hold on
% plot(cr_table.Time,cr_table.P_SCR_7*1e-6,'-','Color',[0.7,0,0.7]);
plot(P_table.Time,(P_table.P)*1e-6,'-','Color',[0.5,0.5,0.5],'MarkerSize',3); hold on
p2 = plot(cr_table.Time,(cr_table.P_7-shift)*1e-6,'-','Color',[0,0,0]);
    legend([p2,p3,p4],{'Halema`uma`u reservoir','South Caldera reservoir (\times1/2)',...
        '2015 intrusion'},'Location','SouthEast','AutoUpdate','off','FontSize',11)
ylabel('pressure (MPa)')
ytickangle(90)
% ylim([2.1,11.9])
ylim([min(P_table.SCR_P/2),max(P_table.P)]*1e-6)
xlim(Xlim)
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
grid on
set(gca,'Layer','top')
xticklabels([])
ttl = title('c. inverted reservoir pressure changes');
set(gca,'FontSize',11)

ax4 = subplot('Position',[plotL,0.36,plotw,0.15]);
p1 = plot(VLP_tab_cr.start,VLP_tab_cr.TC,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
plot(cr_table.Time,cr_table.TC_7,'k','LineWidth',1)
% scatter(VLP_tab_cr.start,VLP_tab_cr.TC,9,VLP_tab_cr.volatile_resnorm,'filled','MarkerEdgeColor','k')
% colormap(ax3,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
ylabel('temperature (C)')
hold on
grid on, grid minor, box on
ytickangle(90)
yticks([1000,1100,1200,1300,1400,1500]),yticklabels({'','1100','','1300','',''})
ylim([min(VLP_tab_cr.TC),max(VLP_tab_cr.TC)])
xlim(Xlim)
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('d. inverted magma temperature');
set(gca,'FontSize',11)

ax5 = subplot('Position',[plotL,0.20,plotw,0.15]);
if vary_CO2
    p1 = plot(VLP_tab_cr.start,VLP_tab_cr.CO2_condbot,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
    plot(cr_table.Time,cr_table.CO2_condbot_7,'k','LineWidth',1)
    ylabel('conduit bottom\newlineCO_2 (ppm)')
elseif vary_H2O
    p1 = plot(VLP_tab_cr.start,VLP_tab_cr.H2O_condbot,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
    plot(cr_table.Time,cr_table.H2O_condbot_7,'k','LineWidth',1)
    ylabel('conduit bottom\newlineH_2O (wt%)')
elseif fixed_ratio
%     yyaxis left
%     plot(VLP_tab_cr.start,VLP_tab_cr.vol_condtop,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.4,0.4,1]); hold on
%     plot(VLP_tab_cr.start,VLP_tab_cr.vol_condbot,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.8,0.8,1]); hold on
%     yyaxis right
%     plot(VLP_tab_cr.start,-VLP_tab_cr.dvol,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
%     yyaxis left
    plot(VLP_tab_cr.start,VLP_tab_cr.vol,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
%     p1 = plot(cr_table.Time,cr_table.vol_condtop_7,'-','LineWidth',1,'Color',[0,0,1]);
%     p2 = plot(cr_table.Time,cr_table.vol_condbot_7,'-','LineWidth',1,'Color',[0.4,0.4,1]);
%     yyaxis right
%     p3 = plot(cr_table.Time,-cr_table.dvol_7,'-','LineWidth',1,'Color',[0.7,0,0]);
%     yyaxis left
    p4 = plot(cr_table.Time,cr_table.vol_7,'-','LineWidth',1,'Color',[0,0,0]);
    ylabel('X^{avg} (wt%)')
    ytickangle(90)
    ylim([1.5,4.5])
    yticks([0,1,2,3,4])
%     yyaxis right
% %     legend([p1,p4,p2,p3],{'conduit top X^{top}','conduit average X^{avg}','conduit bottom X^{bot}','difference (bottom-top) -\Delta X'},'AutoUpdate','off')
%     ylabel('conduit volatile content\newlinedifference (top-bottom) \Delta X (wt%)')
%     ylim([0,4.5])
    %yticks([-4,-3,-2,-1,0])
    ytickangle(90)
elseif vary_lake
    p1 = plot(VLP_tab_cr.start,VLP_tab_cr.vol_lake,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
    plot(cr_table.Time,cr_table.vol_lake_7,'k','LineWidth',1)
    ylabel('lava lake\newlinevolatiles (wt%)')
end
% scatter(VLP_tab_cr.start,VLP_tab_cr.CO2_condbot,9,VLP_tab_cr.volatile_resnorm,'filled','MarkerEdgeColor','k');
% colormap(ax4,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
% p2 = plot(VLP_tab_cr.start,VLP_tab_cr.rho_laketop,'Color',[0.6,0.6,0.6],'LineWidth',1.5);
% scatter(VLP_tab_cr.start,VLP_tab_cr.rho_laketop,9,VLP_tab_cr.direct_resnorm,'filled');
% legend([p1,p2],{'conduit bottom','lake top'},'AutoUpdate','off')
hold on
grid on, grid minor, box on
xlim(Xlim)
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
% xticklabels([])
ttl = title('e. inverted conduit average volatile contents');
set(gca,'FontSize',11)

ax6 = subplot('Position',[plotL,0.04,plotw,0.15]);
if vary_CO2
    p1 = plot(VLP_tab_cr.start,VLP_tab_cr.CO2_condbot,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
    plot(cr_table.Time,cr_table.CO2_condbot_7,'k','LineWidth',1)
    ylabel('conduit bottom\newlineCO_2 (ppm)')
elseif vary_H2O
    p1 = plot(VLP_tab_cr.start,VLP_tab_cr.H2O_condbot,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
    plot(cr_table.Time,cr_table.H2O_condbot_7,'k','LineWidth',1)
    ylabel('conduit bottom\newlineH_2O (wt%)')
elseif fixed_ratio
%     yyaxis left
%     plot(VLP_tab_cr.start,VLP_tab_cr.vol_condtop,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.4,0.4,1]); hold on
%     plot(VLP_tab_cr.start,VLP_tab_cr.vol_condbot,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.8,0.8,1]); hold on
%     yyaxis right
    plot(VLP_tab_cr.start,-VLP_tab_cr.dvol,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
%     yyaxis left
%     plot(VLP_tab_cr.start,VLP_tab_cr.vol,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,1]); hold on
%     p1 = plot(cr_table.Time,cr_table.vol_condtop_7,'-','LineWidth',1,'Color',[0,0,1]);
%     p2 = plot(cr_table.Time,cr_table.vol_condbot_7,'-','LineWidth',1,'Color',[0.4,0.4,1]);
%     yyaxis right
    p3 = plot(cr_table.Time,-cr_table.dvol_7,'-','LineWidth',1,'Color',[0,0,0]);
%     yyaxis left
%     p4 = plot(cr_table.Time,cr_table.vol_7,'-','LineWidth',1,'Color',[0,0,0.7]);
%     ylabel('conduit avgerage volatile\newlinecontents X^{avg} (wt%)')
%     ytickangle(90)
%     ylim([0,4.5])
%     yticks([0,1,2,3,4])
%     yyaxis right
%     legend([p1,p4,p2,p3],{'conduit top X^{top}','conduit average X^{avg}','conduit bottom X^{bot}','difference (bottom-top) -\Delta X'},'AutoUpdate','off')
    ylabel('\Delta X (wt%)')
    ylim([0,4.5])
    %yticks([-4,-3,-2,-1,0])
    ytickangle(90)
elseif vary_lake
    p1 = plot(VLP_tab_cr.start,VLP_tab_cr.vol_lake,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
    plot(cr_table.Time,cr_table.vol_lake_7,'k','LineWidth',1)
    ylabel('lava lake\newlinevolatiles (wt%)')
end
% scatter(VLP_tab_cr.start,VLP_tab_cr.CO2_condbot,9,VLP_tab_cr.volatile_resnorm,'filled','MarkerEdgeColor','k');
% colormap(ax4,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
% p2 = plot(VLP_tab_cr.start,VLP_tab_cr.rho_laketop,'Color',[0.6,0.6,0.6],'LineWidth',1.5);
% scatter(VLP_tab_cr.start,VLP_tab_cr.rho_laketop,9,VLP_tab_cr.direct_resnorm,'filled');
% legend([p1,p2],{'conduit bottom','lake top'},'AutoUpdate','off')
hold on
grid on, grid minor, box on
xlim(Xlim)
xline(datetime(2008,3,19),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),':','Color',[0,0.5,0],'LineWidth',0.95)%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'-','Color',[0,0.5,0],'LineWidth',0.75)%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'--','Color',[0,0.5,0],'LineWidth',0.75)%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
% xticklabels([])
ttl = title('f. inverted conduit volatile content difference (top-bottom)');
set(gca,'FontSize',11)

% ax5 = subplot(5,1,5);
% ax5.Position = ax5.Position+[0,0,0,heightincrease];
% %fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
% %hold on
% if vary_CO2
%     p1 = plot(VLP_tab_cr.start,VLP_tab_cr.CO2_condtop,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
%     plot(cr_table.Time,cr_table.CO2_condtop_7,'k','LineWidth',1)
%     ylabel('conduit top\newlineCO_2 (ppm)')
%     % ylabel('lake top\newlineCO_2 (ppm)')
% elseif vary_H2O
%     p1 = plot(VLP_tab_cr.start,VLP_tab_cr.H2O_condtop,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
%     plot(cr_table.Time,cr_table.H2O_condtop_7,'k','LineWidth',1)
%     ylabel('conduit top\newlineH_2O (wt%)')
%     % ylabel('lake top\newlineH_2O (wt%)')
% elseif fixed_ratio
%     p1 = plot(VLP_tab_cr.start,-VLP_tab_cr.dvol,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
%     plot(cr_table.Time,-cr_table.dvol_7,'k','LineWidth',1)
%     ylabel('conduit volatile\newlinedifference (wt%)')
% elseif vary_lake
%     p1 = plot(VLP_tab_cr.start,VLP_tab_cr.vol_cond,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
%     plot(cr_table.Time,cr_table.vol_cond_7,'k','LineWidth',1)
%     ylabel('conduit\newlinevolatiles (wt%)')
% end
% % p1 = plot(VLP_tab_cr.start,VLP_tab_cr.CO2_laketop,'.','LineWidth',0.5,'MarkerSize',6,'Color',[0.5,0.5,0.5]); hold on
% % plot(cr_table.Time,cr_table.CO2_laketop_7,'k','LineWidth',1)
% % scatter(VLP_tab_cr.start,VLP_tab_cr.CO2_condtop,9,VLP_tab_cr.volatile_resnorm,'filled','MarkerEdgeColor','k');
% % colormap(ax5,'turbo'), cbar = colorbar; ylabel(cbar,'residual')
% hold on
% grid on, grid minor, box on
% xlim(Xlim)
% xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
% xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
% xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
% xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
% xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
% set(gca,'Layer','top')
% % xticklabels([])
% ttl = title('e.'); ttl.Position(1) = ttl.Position(1)/12;
%}

%% volatile nonlin sol moving corr
%{
skipin = VLP_tab_cr.start>datetime(2011,6,1) & VLP_tab_cr.start<datetime(2011,9,1);
VLP_tab_corr = VLP_tab_cr(~skipin,:);
corr_table = (VLP_tab_corr.start(1):days(1):VLP_tab_corr.start(end))';
corr_table = timetable([corr_table(:)],NaN(size(corr_table)),'VariableNames',{'blank'});

% corr_table.T_condbot = NaN(size(corr_table.Time));
% corr_table.Q_condbot = NaN(size(corr_table.Time));
% corr_table.h_condbot = NaN(size(corr_table.Time));
% corr_table.P_condbot = NaN(size(corr_table.Time));
% corr_table.condtop_condbot = NaN(size(corr_table.Time));
% corr_table.TC_condbot = NaN(size(corr_table.Time));
corr_table.T_vol = NaN(size(corr_table.Time));
corr_table.Q_vol = NaN(size(corr_table.Time));
corr_table.h_vol = NaN(size(corr_table.Time));
corr_table.P_vol = NaN(size(corr_table.Time));
corr_table.dvol_vol = NaN(size(corr_table.Time));
corr_table.TC_vol = NaN(size(corr_table.Time));

% corr_table.T_condtop = NaN(size(corr_table.Time));
% corr_table.Q_condtop = NaN(size(corr_table.Time));
% corr_table.h_condtop = NaN(size(corr_table.Time));
% corr_table.P_condtop = NaN(size(corr_table.Time));
% corr_table.condbot_condtop = NaN(size(corr_table.Time));
% corr_table.TC_condtop = NaN(size(corr_table.Time));
corr_table.T_dvol = NaN(size(corr_table.Time));
corr_table.Q_dvol = NaN(size(corr_table.Time));
corr_table.h_dvol = NaN(size(corr_table.Time));
corr_table.P_dvol = NaN(size(corr_table.Time));
corr_table.vol_dvol = NaN(size(corr_table.Time));
corr_table.TC_dvol = NaN(size(corr_table.Time));

corr_table.T_TC = NaN(size(corr_table.Time));
corr_table.Q_TC = NaN(size(corr_table.Time));
corr_table.h_TC = NaN(size(corr_table.Time));
corr_table.P_TC = NaN(size(corr_table.Time));
% corr_table.condbot_TC = NaN(size(corr_table.Time));
% corr_table.condtop_TC = NaN(size(corr_table.Time));
corr_table.vol_TC = NaN(size(corr_table.Time));
corr_table.dvol_TC = NaN(size(corr_table.Time));

% corr_table.p_T_condbot = NaN(size(corr_table.Time));
% corr_table.p_Q_condbot = NaN(size(corr_table.Time));
% corr_table.p_h_condbot = NaN(size(corr_table.Time));
% corr_table.p_P_condbot = NaN(size(corr_table.Time));
% corr_table.p_condtop_condbot = NaN(size(corr_table.Time));
% corr_table.p_TC_condbot = NaN(size(corr_table.Time));
corr_table.p_T_vol = NaN(size(corr_table.Time));
corr_table.p_Q_vol = NaN(size(corr_table.Time));
corr_table.p_h_vol = NaN(size(corr_table.Time));
corr_table.p_P_vol = NaN(size(corr_table.Time));
corr_table.p_dvol_vol = NaN(size(corr_table.Time));
corr_table.p_TC_vol = NaN(size(corr_table.Time));

% corr_table.p_T_condtop = NaN(size(corr_table.Time));
% corr_table.p_Q_condtop = NaN(size(corr_table.Time));
% corr_table.p_h_condtop = NaN(size(corr_table.Time));
% corr_table.p_P_condtop = NaN(size(corr_table.Time));
% corr_table.p_condbot_condtop = NaN(size(corr_table.Time));
% corr_table.p_TC_condtop = NaN(size(corr_table.Time));
corr_table.p_T_dvol = NaN(size(corr_table.Time));
corr_table.p_Q_dvol = NaN(size(corr_table.Time));
corr_table.p_h_dvol = NaN(size(corr_table.Time));
corr_table.p_P_dvol = NaN(size(corr_table.Time));
corr_table.p_vol_dvol= NaN(size(corr_table.Time));

corr_table.p_T_TC = NaN(size(corr_table.Time));
corr_table.p_Q_TC = NaN(size(corr_table.Time));
corr_table.p_h_TC = NaN(size(corr_table.Time));
corr_table.p_P_TC = NaN(size(corr_table.Time));
% corr_table.p_condbot_TC = NaN(size(corr_table.Time));
% corr_table.p_condtop_TC = NaN(size(corr_table.Time));
corr_table.p_vol_TC = NaN(size(corr_table.Time));
corr_table.p_dvol_TC = NaN(size(corr_table.Time));

Tspan = days(90/2);
corrtype = 'Pearson';
use_smoothed = false;

for ii = 1:length(corr_table.Time) 
    if ~use_smoothed
        datain = VLP_tab_corr.start >= corr_table.Time(ii) - Tspan & ...
            VLP_tab_corr.start <= corr_table.Time(ii) + Tspan;
    else
        datain = cr_table.Time >= corr_table.Time(ii) - Tspan & ...
            cr_table.Time <= corr_table.Time(ii) + Tspan;
    end
    if sum(datain) >= 6
        if ~use_smoothed
            T_data = VLP_tab_corr.T(datain);
            Q_data = VLP_tab_corr.Q(datain);
            h_data = VLP_tab_corr.lakeh(datain);
            P_data = VLP_tab_corr.P_restop(datain);
%             condbot_data = VLP_tab_corr.condbot(datain);
%             condtop_data = VLP_tab_corr.condtop(datain);
            vol_data = VLP_tab_corr.vol(datain);
            dvol_data = -VLP_tab_corr.dvol(datain);
            TC_data = VLP_tab_corr.TC(datain);
        else
            T_data = cr_table.T_7(datain);
            Q_data = cr_table.Q_7(datain);
            h_data = cr_table.h_7(datain);
            P_data = cr_table.P_7(datain);
%             condbot_data = cr_table.condbot_7(datain);
%             condtop_data = cr_table.condtop_7(datain);
            vol_data = cr_table.vol_7(datain);
            dvol_data = -cr_table.dvol_7(datain);
            TC_data = cr_table.TC_7(datain);
        end

%         [corr_table.T_condbot(ii),corr_table.p_T_condbot(ii)] = corr(T_data,condbot_data,'rows','complete','Type',corrtype);
%         [corr_table.Q_condbot(ii),corr_table.p_Q_condbot(ii)] = corr(Q_data,condbot_data,'rows','complete','Type',corrtype);
%         [corr_table.h_condbot(ii),corr_table.p_h_condbot(ii)] = corr(h_data,condbot_data,'rows','complete','Type',corrtype);
%         [corr_table.P_condbot(ii),corr_table.p_P_condbot(ii)] = corr(P_data,condbot_data,'rows','complete','Type',corrtype);
%         [corr_table.condtop_condbot(ii),corr_table.p_condtop_condbot(ii)] = corr(condtop_data,condbot_data,'rows','complete','Type',corrtype);
%         [corr_table.TC_condbot(ii),corr_table.p_TC_condbot(ii)] = corr(TC_data,condbot_data,'rows','complete','Type',corrtype);
        [corr_table.T_vol(ii),corr_table.p_T_vol(ii)] = corr(T_data,vol_data,'rows','complete','Type',corrtype);
        [corr_table.Q_vol(ii),corr_table.p_Q_vol(ii)] = corr(Q_data,vol_data,'rows','complete','Type',corrtype);
        [corr_table.h_vol(ii),corr_table.p_h_vol(ii)] = corr(h_data,vol_data,'rows','complete','Type',corrtype);
        [corr_table.P_vol(ii),corr_table.p_P_vol(ii)] = corr(P_data,vol_data,'rows','complete','Type',corrtype);
        [corr_table.dvol_vol(ii),corr_table.p_dvol_vol(ii)] = corr(dvol_data,vol_data,'rows','complete','Type',corrtype);
        [corr_table.TC_vol(ii),corr_table.p_TC_vol(ii)] = corr(TC_data,vol_data,'rows','complete','Type',corrtype);

%         [corr_table.T_condtop(ii),corr_table.p_T_condtop(ii)] = corr(T_data,condtop_data,'rows','complete','Type',corrtype);
%         [corr_table.Q_condtop(ii),corr_table.p_Q_condtop(ii)] = corr(Q_data,condtop_data,'rows','complete','Type',corrtype);
%         [corr_table.h_condtop(ii),corr_table.p_h_condtop(ii)] = corr(h_data,condtop_data,'rows','complete','Type',corrtype);
%         [corr_table.P_condtop(ii),corr_table.p_P_condtop(ii)] = corr(P_data,condtop_data,'rows','complete','Type',corrtype);
%         [corr_table.condbot_condtop(ii),corr_table.p_condbot_condtop(ii)] = corr(condbot_data,condtop_data,'rows','complete','Type',corrtype);
%         [corr_table.TC_condtop(ii),corr_table.p_TC_condtop(ii)] = corr(TC_data,condtop_data,'rows','complete','Type',corrtype);
        [corr_table.T_dvol(ii),corr_table.p_T_dvol(ii)] = corr(T_data,dvol_data,'rows','complete','Type',corrtype);
        [corr_table.Q_dvol(ii),corr_table.p_Q_dvol(ii)] = corr(Q_data,dvol_data,'rows','complete','Type',corrtype);
        [corr_table.h_dvol(ii),corr_table.p_h_dvol(ii)] = corr(h_data,dvol_data,'rows','complete','Type',corrtype);
        [corr_table.P_dvol(ii),corr_table.p_P_dvol(ii)] = corr(P_data,dvol_data,'rows','complete','Type',corrtype);
        [corr_table.vol_dvol(ii),corr_table.p_vol_dvol(ii)] = corr(vol_data,dvol_data,'rows','complete','Type',corrtype);
        [corr_table.TC_dvol(ii),corr_table.p_TC_dvol(ii)] = corr(TC_data,dvol_data,'rows','complete','Type',corrtype);

        [corr_table.T_TC(ii),corr_table.p_T_TC(ii)] = corr(T_data,TC_data,'rows','complete','Type',corrtype);
        [corr_table.Q_TC(ii),corr_table.p_Q_TC(ii)] = corr(Q_data,TC_data,'rows','complete','Type',corrtype);
        [corr_table.h_TC(ii),corr_table.p_h_TC(ii)] = corr(h_data,TC_data,'rows','complete','Type',corrtype);
        [corr_table.P_TC(ii),corr_table.p_P_TC(ii)] = corr(P_data,TC_data,'rows','complete','Type',corrtype);
%         [corr_table.condbot_TC(ii),corr_table.p_condbot_TC(ii)] = corr(condbot_data,TC_data,'rows','complete','Type',corrtype);
%         [corr_table.condtop_TC(ii),corr_table.p_condtop_TC(ii)] = corr(condtop_data,TC_data,'rows','complete','Type',corrtype);
        [corr_table.vol_TC(ii),corr_table.p_vol_TC(ii)] = corr(vol_data,TC_data,'rows','complete','Type',corrtype);
        [corr_table.dvol_TC(ii),corr_table.p_dvol_TC(ii)] = corr(dvol_data,TC_data,'rows','complete','Type',corrtype);
    end
end

%discard low p-values
p_value_thresh = 0.05;

% corr_table.T_condbot(corr_table.p_T_condbot > p_value_thresh) = NaN;
% corr_table.Q_condbot(corr_table.p_Q_condbot > p_value_thresh) = NaN;
% corr_table.h_condbot(corr_table.p_h_condbot > p_value_thresh) = NaN;
% corr_table.P_condbot(corr_table.p_P_condbot > p_value_thresh) = NaN;
% corr_table.condtop_condbot(corr_table.p_condtop_condbot > p_value_thresh) = NaN;
% corr_table.TC_condbot(corr_table.p_TC_condbot > p_value_thresh) = NaN;
corr_table.T_vol(corr_table.p_T_vol > p_value_thresh) = NaN;
corr_table.Q_vol(corr_table.p_Q_vol > p_value_thresh) = NaN;
corr_table.h_vol(corr_table.p_h_vol > p_value_thresh) = NaN;
corr_table.P_vol(corr_table.p_P_vol > p_value_thresh) = NaN;
corr_table.dvol_vol(corr_table.p_dvol_vol > p_value_thresh) = NaN;
corr_table.TC_vol(corr_table.p_TC_vol > p_value_thresh) = NaN;

% corr_table.T_condtop(corr_table.p_T_condtop > p_value_thresh) = NaN;
% corr_table.Q_condtop(corr_table.p_Q_condtop > p_value_thresh) = NaN;
% corr_table.h_condtop(corr_table.p_h_condtop > p_value_thresh) = NaN;
% corr_table.P_condtop(corr_table.p_P_condtop > p_value_thresh) = NaN;
% corr_table.condbot_condtop(corr_table.p_condbot_condtop > p_value_thresh) = NaN;
% corr_table.TC_condtop(corr_table.p_TC_condtop > p_value_thresh) = NaN;
corr_table.T_dvol(corr_table.p_T_dvol > p_value_thresh) = NaN;
corr_table.Q_dvol(corr_table.p_Q_dvol > p_value_thresh) = NaN;
corr_table.h_dvol(corr_table.p_h_dvol > p_value_thresh) = NaN;
corr_table.P_dvol(corr_table.p_P_dvol > p_value_thresh) = NaN;
corr_table.vol_dvol(corr_table.p_vol_dvol > p_value_thresh) = NaN;
corr_table.TC_dvol(corr_table.p_TC_dvol > p_value_thresh) = NaN;

corr_table.T_TC(corr_table.p_T_TC > p_value_thresh) = NaN;
corr_table.Q_TC(corr_table.p_Q_TC > p_value_thresh) = NaN;
corr_table.h_TC(corr_table.p_h_TC > p_value_thresh) = NaN;
corr_table.P_TC(corr_table.p_P_TC > p_value_thresh) = NaN;
% corr_table.condbot_TC(corr_table.p_condbot_TC > p_value_thresh) = NaN;
% corr_table.condtop_TC(corr_table.p_condtop_TC > p_value_thresh) = NaN;
corr_table.vol_TC(corr_table.p_vol_TC > p_value_thresh) = NaN;
corr_table.dvol_TC(corr_table.p_dvol_TC > p_value_thresh) = NaN;

%% volatile nonlin sol moving corr plot
figure()
plotheight = 0.18;
plotleft = 0.035;
plotwidth = 0.79;
legleft = 0.86;
legwidth = 0.12;

%temp
subplot('Position',[plotleft,0.76,plotwidth,plotheight])
hold on
p1 = plot(corr_table.Time,corr_table.T_TC,'-k','LineWidth',1.5,'Color',[0,0,1]);
p2 = plot(corr_table.Time,corr_table.Q_TC,'-k','LineWidth',1.5,'Color',[1,0,0]);
p3 = plot(corr_table.Time,corr_table.h_TC,'-k','LineWidth',1.5,'Color',[0,0,0]);
p4 = plot(corr_table.Time,corr_table.P_TC,'-k','LineWidth',1.5,'Color',[0.5,0.5,0.5]);
% p5 = plot(corr_table.Time,corr_table.condbot_TC,'-k','LineWidth',1.5,'Color',[0.6,0.6,0.6]);
% p6 = plot(corr_table.Time,corr_table.condtop_TC,'-k','LineWidth',1.5,'Color',[0.3,0.3,0.3]);
% legend([p1,p2,p3,p4,p5,p6],...
%     {'T','Q','lake el.','reservoir\newlinepressure', 'conduit\newlinebot CO2','conduit\newlinetop CO2'},...
%     'AutoUpdate','off','Location','NorthEastOutside')
legend([p1,p2,p3,p4],...
    {'T','Q','lake el.','reservoir\newlinepressure'},...
    'AutoUpdate','off','Position',[legleft,0.76,legwidth,plotheight])
xlim([datetime(2009,10,1),datetime(2018,6,1)])
ylim([-1,1])
xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
title('a. inverted temperature correlations')
% ylabel('correlation coefficient')
yline(0,'k')
set(gca,'FontSize',12)
% xlabel('Time (UTC)')
xticklabels([])
grid on, box on

%co2 condbot
subplot('Position',[plotleft,0.54,plotwidth,plotheight])
hold on
p1 = plot(corr_table.Time,corr_table.T_vol,'-k','LineWidth',1.5,'Color',[0,0,1]);
p2 = plot(corr_table.Time,corr_table.Q_vol,'-k','LineWidth',1.5,'Color',[1,0,0]);
p3 = plot(corr_table.Time,corr_table.h_vol,'-k','LineWidth',1.5,'Color',[0,0,0]);
p4 = plot(corr_table.Time,corr_table.P_vol,'-k','LineWidth',1.5,'Color',[0.5,0.5,0.5]);
% p6 = plot(corr_table.Time,corr_table.condtop_condbot,'-k','LineWidth',1.5,'Color',[0.3,0.3,0.3]);
% p7 = plot(corr_table.Time,corr_table.TC_condbot,'-k','LineWidth',1.5,'Color',[0,0,0]);
% legend([p1,p2,p3,p4,p6,p7],...
%     {'T','Q','lake el.','reservoir\newlinepressure','conduit\newlinetop CO2','temp'},...
%     'AutoUpdate','off','Location','NorthEastOutside')
legend([p1,p2,p3,p4],...
    {'T','Q','lake el.','reservoir\newlinepressure'},...
    'AutoUpdate','off','Position',[legleft,0.54,legwidth,plotheight])
xlim([datetime(2009,10,1),datetime(2018,6,1)])
ylim([-1,1])
xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
title('b. inverted conduit average volatile correlation coefficients')
% ylabel('correlation coefficient')
yline(0,'k')
set(gca,'FontSize',12)
% xlabel('Time (UTC)')
xticklabels([])
grid on, box on

%co2 condtop
subplot('Position',[plotleft,0.32,plotwidth,plotheight])
hold on
p1 = plot(corr_table.Time,corr_table.T_dvol,'-k','LineWidth',1.5,'Color',[0,0,1]);
p2 = plot(corr_table.Time,corr_table.Q_dvol,'-k','LineWidth',1.5,'Color',[1,0,0]);
p3 = plot(corr_table.Time,corr_table.h_dvol,'-b','LineWidth',1.5,'Color',[0,0,0]);
p4 = plot(corr_table.Time,corr_table.P_dvol,'-r','LineWidth',1.5,'Color',[0.5,0.5,0.5]);
% p5 = plot(corr_table.Time,corr_table.condbot_condtop,'-k','LineWidth',1.5,'Color',[0.6,0.6,0.6]);
% p7 = plot(corr_table.Time,corr_table.TC_condtop,'-k','LineWidth',1.5,'Color',[0,0,0]);
% legend([p1,p2,p3,p4,p5,p7],...
%     {'T','Q','lake el.','reservoir\newlinepressure', 'conduit\newlinebot CO2','temp'},...
%     'AutoUpdate','off','Location','NorthEastOutside')
legend([p1,p2,p3,p4],...
    {'T','Q','lake el.','reservoir\newlinepressure'},...
    'AutoUpdate','off','Position',[legleft,0.32,legwidth,plotheight])
xlim([datetime(2009,10,1),datetime(2018,6,1)])
ylim([-1,1])
xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
title('c. inverted conduit volatile difference correlation coefficients')
% ylabel('correlation coefficient')
yline(0,'k')
set(gca,'FontSize',12)
xticklabels([])
grid on, box on

%all inverted parameters
subplot('Position',[plotleft,0.1,plotwidth,plotheight])
hold on
p5 = plot(corr_table.Time,corr_table.vol_dvol,'-k','LineWidth',1.5,'Color',[0.5,0,0.5]);
p6 = plot(corr_table.Time,corr_table.TC_vol,'-k','LineWidth',1.5,'Color',[0,1,0]);
p7 = plot(corr_table.Time,corr_table.TC_dvol,'-k','LineWidth',1.5,'Color',[0,0.5,0]);
legend([p5,p6,p7],...
    {'avg volatile\newline vs volatile diff','avg volatile\newline vs temp','volatile diff\newline vs temp'},...
    'AutoUpdate','off','Position',[legleft,0.1,legwidth,plotheight])
xlim([datetime(2009,10,1),datetime(2018,6,1)])
ylim([-1,1])
xline(datetime(2008,3,19),'k',{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k',{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k',{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k',{'Int'},'LabelVerticalAlignment','bottom')
% ylabel('correlation coefficient')
title('d. inverted magma properties correlation coefficients')
yline(0,'k')
set(gca,'FontSize',12)
xlabel('Time (UTC)')
grid on, box on
%}

%% wavelet coherence volatile nonlin
%
wcoh_table = timetable(VLP_tab_cr.start,VLP_tab_cr.TC,'VariableNames',{'TC'});
wcoh_table.condbot = VLP_tab_cr.vol_condbot;
wcoh_table.condtop = VLP_tab_cr.vol_condtop;
wcoh_table.vol = VLP_tab_cr.vol;
wcoh_table.dvol = -VLP_tab_cr.dvol;
wcoh_table.T = VLP_tab_cr.T;
wcoh_table.Q = VLP_tab_cr.Q;
wcoh_table = rmmissing(wcoh_table);

ts = days(1/4);
'skipping start'
wcoh_table = wcoh_table(wcoh_table.Time>datetime(2011,12,1),:);
wcoh_table = retime(wcoh_table,'regular','linear','TimeStep',ts);

lakeh_resample = timetable(lakeh.date,lakeh.h,'VariableNames',{'h'});
lakeh_resample = smoothdata(lakeh_resample,'movmean',ts);
lakeh_resample = retime(lakeh_resample,wcoh_table.Time,'linear');
wcoh_table.h = lakeh_resample.h;
P_resample = smoothdata(P_table,'movmean',ts);
P_resample = retime(P_resample,wcoh_table.Time,'linear');
wcoh_table.P = P_resample.P;
wcoh_table.P_SCR = P_resample.SCR_P;



%
'skipping start'
wcoh_table = wcoh_table(wcoh_table.Time>datetime(2009,12,1),:);

'skipping middle region'
skipreg1 = datetime(2011,2,16);
skipreg2 = datetime(2011,12,1);
wcoh_table = wcoh_table(wcoh_table.Time<skipreg1 | wcoh_table.Time>skipreg2,:);
skipreg = days([skipreg1,skipreg2] - wcoh_table.Time(1));

ts = days(1/4);
wcoh_table = retime(wcoh_table,'regular','linear','TimeStep',ts);

PeriodLimits = [days(7),days(600)];
VoicesPerOctave = 12;
NumScalesToSmooth = 9;
PhaseDisplayThreshold = 0.5;

plotleft1 = 0.05;
plotleft2 = 0.54;
plotwidth = 0.42;
plotheight = 0.28;
poscell = {[plotleft1,0.05+2*0.31,plotwidth,plotheight];
    [plotleft1,0.05+0.31,plotwidth,plotheight];
    [plotleft1,0.05,plotwidth,plotheight];
    [plotleft2,0.05+2*0.31,plotwidth,plotheight];
    [plotleft2,0.05+0.31,plotwidth,plotheight];
    [plotleft2,0.05,plotwidth,plotheight]};

v1cell = {wcoh_table.h;
    wcoh_table.h;
    wcoh_table.h;
    wcoh_table.TC;
    wcoh_table.TC;
    wcoh_table.vol};
v2cell = {wcoh_table.TC;
    wcoh_table.vol;
    wcoh_table.dvol;
    wcoh_table.vol;
    wcoh_table.dvol;
    wcoh_table.dvol};

titlecell = {'a. lava lake elevation vs temperature';
    'b. lava lake elevation vs X^{avg}';
    'c. lava lake elevation vs \Delta X';
    'd. temperature vs X^{avg}';
    'e. temperature vs \Delta X';
    'f. X^{avg} vs \Delta X'};

fig = figure();
screensize = get(0,'screensize');
fig.OuterPosition = [1,1,screensize(3)*0.8,screensize(4)];
xticklist = days(datetime((2010:2018),ones(1,9),ones(1,9)) - wcoh_table.Time(1));
xticklabellist = num2str((2010:2018)');
for ii = 1:length(titlecell)
    subplot('Position',poscell{ii})
    % [wcoh,wcs,period] = wcoherence(wcoh_table.TC,wcoh_table.condbot,ts,'PeriodLimits',PeriodLimits,...
    %     'VoicesPerOctave',VoicesPerOctave,'NumScalesToSmooth',NumScalesToSmooth,...
    %     'PhaseDisplayThreshold',PhaseDisplayThreshold);
    % plotcoherenceperiod_josh(wtc,crosspec,dtFunc(f),t,dtFunc(coitmp),...
    %             nov,mc,plotstring);
    % p = pcolor(datenum(wcoh_table.Time),days(period),wcoh); hold on
    % p.EdgeColor = 'none';p.FaceColor = 'interp';
    % cbar = colorbar; ylabel(cbar,'coherence'); caxis([0,1])
    % set(gca,'YScale','Log'); ylabel('Period (days)')
    % grid on, box on, set(gca,'Layer','top')
    % quiv_xinc = 99;
    % quiv_yinc = 11;
    % Ksmooth = (1/(quiv_xinc*quiv_yinc))*ones(quiv_yinc,quiv_xinc);
    % wcs_sub = conv2(wcs,Ksmooth,'same');
    % wcoh_sub = conv2(wcoh,Ksmooth,'same');
    % wcs_sub(wcoh_sub < PhaseDisplayThreshold) = NaN;
    % quiver(datenum(wcoh_table.Time(floor(quiv_xinc/2):quiv_xinc:end)),...
    %     days(period(floor(quiv_yinc/2):quiv_yinc:end)),...
    %     real(wcoh_sub(floor(quiv_yinc/2):quiv_yinc:end,floor(quiv_xinc/2):quiv_xinc:end)),...
    %     imag(wcoh_sub(floor(quiv_yinc/2):quiv_yinc:end,floor(quiv_xinc/2):quiv_xinc:end)),...
    %     'k')
    % XLim(datenum([wcoh_table.Time(1),wcoh_table.Time(end)]))
    % XTicks(datenum(datetime(2009,1,1):years(1):datetime(2019,1,1)))
    % XTickLabels(num2str((2009:2019)'))
    wcoherence_josh(v1cell{ii},v2cell{ii},ts,'PeriodLimits',PeriodLimits,...
        'VoicesPerOctave',VoicesPerOctave,'NumScalesToSmooth',NumScalesToSmooth,...
        'PhaseDisplayThreshold',PhaseDisplayThreshold,'skipreg',skipreg);
    rectangle('Position',[days(skipreg1 - wcoh_table.Time(1)),log2(days(PeriodLimits(1))),...
        days(skipreg2 - skipreg1), log2(days(PeriodLimits(2))) - log2(days(PeriodLimits(1)))],...
        'EdgeColor','none','FaceColor','w');
    title(titlecell{ii})
    xticks(xticklist);
    if ii == 3 || ii == 6
        xticklabels(xticklabellist)
    else
        xticklabels([])
    end
    grid on
    xlabel([])
    xline(days(datetime(2008,3,19) - wcoh_table.Time(1)),'k','LineWidth',1)%,{'Crater'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2010,2,1) - wcoh_table.Time(1)),'k:','LineWidth',1)%,{'SSE'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2012,5,25) - wcoh_table.Time(1)),'k:','LineWidth',1)%,{'SSE'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2015,10,15) - wcoh_table.Time(1)),'k:','LineWidth',1)%,{'SSE'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2011,3,5) - wcoh_table.Time(1)),'k-','LineWidth',1)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2011,8,3) - wcoh_table.Time(1)),'k-','LineWidth',1)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2011,9,21) - wcoh_table.Time(1)),'k-','LineWidth',1)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2014,6,27) - wcoh_table.Time(1)),'k-','LineWidth',1)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2016,5,24) - wcoh_table.Time(1)),'k-','LineWidth',1)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2018,4,30) - wcoh_table.Time(1)),'k-','LineWidth',1)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2012,10,26) - wcoh_table.Time(1)),'k--','LineWidth',1)%,{'Int'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2014,5,10) - wcoh_table.Time(1)),'k--','LineWidth',1)%,{'Int'},'LabelVerticalAlignment','bottom')
    xline(days(datetime(2015,5,9) - wcoh_table.Time(1)),'k--','LineWidth',1)%,{'Int'},'LabelVerticalAlignment','bottom')
    set(gca,'FontSize',12)
end
%}

figure()
PeriodLimits = [days(7),days(600)];
VoicesPerOctave = 32;
NumScalesToSmooth = 9;
plotx = 0.08;
ploty = 0.6;
plotw = 0.6;
ploth = 0.38;
plotlegw = 0.3;
plotlegdw = 0.001;

varcell = {'lava lake el.\newline\times10^{-1} (m)';
    'HMMR pressure\newline(MPa)'
    'SCR pressure\newline(MPa)';
    'temp\newline\times10^{-1} (C)';%'conduit top X^{top} (wt%)'
    'conduit avg volatile\newlineX^{avg} (wt%)';%'conduit bottom X^{bot} (wt%)';
    'conduit volatile diff\newline\Delta X (wt%)'};
volscale = 10^0;
vcell = {wcoh_table.h*10^-1;
    wcoh_table.P*10e-6;
    wcoh_table.P_SCR*10e-6;
    wcoh_table.TC*10^-1;% wcoh_table.condtop*volscale;
    wcoh_table.vol*volscale; % wcoh_table.condbot*volscale;
    wcoh_table.dvol*volscale};
% vsymcell = {'-','--','-.',':','o'};
vsymcell = {'--','-.',':','-','-','-'};
% vccell = {[0,0,0],[1,0,0],[0,0,1],[0,0,1]};
% vccell = {[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]};
vccell = {[0,0.5,0],[0,0.5,0],[0,0.5,0],[0,0,0],[0,0,0.7],[0.7,0,0]};
vscatcell = {'o','o','o','o','o','o'};
ax1 = subplot('position',[plotx,ploty,plotw,ploth]);
hold on
grid on
box on
set(gca,'FontSize',11)
xlim(days(PeriodLimits))
xticklabels([])
xticks(10.^(0:3))
ylabel('amplitude')
pcell = gobjects(numel(vscatcell),1);
for ii = 1:length(varcell)
    [wt,period,coi] = cwt(vcell{ii},'amor',ts,'PeriodLimits',PeriodLimits,...
        'VoicesPerOctave',VoicesPerOctave,'ExtendSignal',false);
    mask = ones(size(wt));
    for jj = 1:length(coi)
        mask(period>coi(jj),jj) = 0;
    end
    weight = mask;
    data = sum(abs(wt).*weight,2)./sum(weight,2);
    cdata = NaN(size(period));
    for jj = 1:length(period)
        tempdata = abs(wt(jj,mask(jj,:)>0)).^2;
        cdata(jj) = std(tempdata)/mean(tempdata);
    end
    periodinterp = logspace(log10(days(period(1))),log10(days(period(end))),5000);
    datainterp = interp1(period,data,periodinterp);
    cdatainterp = interp1(period,cdata,periodinterp);
%     scatter(ax1,periodinterp,datainterp,9,cdatainterp,vscatcell{ii},'filled')
    pcell(ii) = plot(ax1,days(period),data,vsymcell{ii},'Color',vccell{ii},'LineWidth',1.0);
end
set(ax1,'XScale','log')
set(ax1,'Yscale','log')
leg = legend(ax1,pcell,varcell,'Location','EastOutside');
leg.Position = [plotx+plotw+plotlegdw,ploty,plotlegw,ploth];
title('a.')
ylim([2.7e-2,3e0])
% cbar = colorbar;
% ylabel(cbar,'normalized std of amp over time')
% caxis([0,3])
% cmapr = [linspace(0.9,0,20),linspace(0,0.5,20)]';
% cmapg = [linspace(0.9,0.5,20),linspace(0.5,0,5,20)]';
% cmapb = [linspace(0.9,0.5,20),linspace(0,0,20)]';
% cmap = [cmapr,cmapg,cmapb];
cmap = parula;
cmap = cmap+0.15;
cmap(cmap>1) = 1;
colormap(ax1,cmap)


%generate significance thresholds
% s1 = randn(height(wcoh_table),1);
% numruns = 1000;
% for ii = 1:numruns
%     s2 = randn(height(wcoh_table),1);
%     [wcoh,~,period,coi] = wcoherence(s1,s2,ts,'PeriodLimits',PeriodLimits,...
%         'VoicesPerOctave',VoicesPerOctave,'NumScalesToSmooth',NumScalesToSmooth);
%     mask = ones(size(wcoh));
%     for jj = 1:length(coi)
%         mask(period>coi(jj),jj) = 0;
%     end
%     weight = mask;
%     data = sum(wcoh.*weight,2)./sum(weight,2);
%     s1 = s2;
%     if ii == 1
%         datamat = NaN(numruns,length(data));
%     end
%     datamat(ii,:) = data;
% end
% sig95 = quantile(datamat,0.95);
% sig75 = quantile(datamat,0.75);
load('sig75.mat')
load('sig95.mat')

% legcell = flipud({'HMMR pressure vs\newlineSCR pressure';
%     'lava lake el. vs\newlineHMMR pressure';
%     'lava lake el. vs\newlinetemp';
%     'lava lake el. vs\newlineX^{avg}';
%     'lava lake el. vs\newline\Delta X';
%     'temp vs\newlineX^{avg}';
%     'temp vs\newline\Delta X';
%     'X^{avg} vs\newline\Delta X'});
% v1cell = flipud({wcoh_table.P;
%     wcoh_table.h;
%     wcoh_table.h;
%     wcoh_table.h;
%     wcoh_table.h;
%     wcoh_table.TC;
%     wcoh_table.TC;
%     wcoh_table.vol});
% v2cell = flipud({wcoh_table.P_SCR;
%     wcoh_table.P;
%     wcoh_table.TC;
%     wcoh_table.vol;
%     wcoh_table.dvol;
%     wcoh_table.vol;
%     wcoh_table.dvol;
%     wcoh_table.dvol});
% vccell = {[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]};
% vsymcell = {'-','-','-',':',':',':'};
% vscatcell = {'o','d','s','v','^','p'};

legcell = flipud({'SCR pressure vs\newlinetemp';
    'SCR pressure vs\newlineX^{avg}';
    'SCR pressure vs\newline\Delta X';
    'HMMR pressure vs\newlinetemp';
    'HMMR pressure vs\newlineX^{avg}';
    'HMMR pressure vs\newline\Delta X';
    'period vs\newlinetemp';
    'period vs\newlineX^{avg}';
    'period vs\newline\Delta X';
    'quality factor vs\newlinetemp';
    'quality factor vs\newlineX^{avg}';
    'quality factor vs\newline\Delta X';});
v1cell = flipud({wcoh_table.P_SCR;
    wcoh_table.P_SCR;
    wcoh_table.P_SCR;
    wcoh_table.P;
    wcoh_table.P;
    wcoh_table.P;
    wcoh_table.T;
    wcoh_table.T;
    wcoh_table.T;
    wcoh_table.Q;
    wcoh_table.Q;
    wcoh_table.Q});
v2cell = flipud({wcoh_table.TC;
    wcoh_table.vol;
    wcoh_table.dvol;
    wcoh_table.TC;
    wcoh_table.vol;
    wcoh_table.dvol;
    wcoh_table.TC;
    wcoh_table.vol;
    wcoh_table.dvol;
    wcoh_table.TC;
    wcoh_table.vol;
    wcoh_table.dvol});
vccell = {[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0], [0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]};
vsymcell = {':',':',':',':',':',':',':',':',':',':',':',':'};
vscatcell = {'o','d','s','v','^','p','o','d','s','v','^','p'};

% legcell2 = {'lava lake el. vs temp';
%     'lava lake el. vs X^{avg}';
%     'lava lake el. vs \Delta X'};
% legcell3 = {'temp vs X^{avg}';
%     'temp vs \Delta X';
%     'X^{avg} vs \Delta X'};
% v1cell = {wcoh_table.h;
%     wcoh_table.h;
%     wcoh_table.h;
%     wcoh_table.TC;
%     wcoh_table.TC;
%     wcoh_table.condtop};
% v2cell = {wcoh_table.TC;
%     wcoh_table.condtop;
%     wcoh_table.condbot;
%     wcoh_table.condtop;
%     wcoh_table.condbot;
%     wcoh_table.condbot};
% legcell2 = {'lava lake el. vs temp';
%     'lava lake el. vs X^{top}';
%     'lava lake el. vs X^{bot}'};
% legcell3 = {'temp vs X^{top}';
%     'temp vs X^{bot}';
%     'X^{top} vs X^{bot}'};
% 
% vscatcell2 = {'o','o','o'};
% vscatcell3 = {'o','o','o'};
% % vsymcell = {'--','-.',':','-.',':',':'};
% 
% vsymcell2 = {'-','--',':'};
% vsymcell3 = {'-','--',':'};
% % vccell = {[0.5,0,0],[0,0,0.5],[0,0.5,0],[0.5,0,0.5],[0.5,0.5,0],[0,0.5,0.5]};
% %vccell = {[0,0,0],[0,0,0],[0,0,0],[1,0,0],[1,0,0],[0,0,1]};
% 
% vccell2 = {[0,0,0],[0,0,0],[0,0,0]};
% vccell3 = {[0,0,0],[0,0,0],[0,0,0]};
% ax2 = subplot('position',[0.06,0.32,0.6,0.23]);
% hold on
% grid on
% box on
% set(gca,'FontSize',11)
% xlim(days(PeriodLimits))
% xticklabels([])
% xticks(10.^(0:3))
% ylabel('coherence')
% title('b.')
% ax3 = subplot('position',[0.06,0.06,0.6,0.23]);
% hold on
% grid on
% box on
% set(gca,'FontSize',11)
% xlim(days(PeriodLimits))
% xlabel('period (days)')
% xticks(10.^(0:3))
% ylabel('coherence')
% title('c.')
% ax3 = subplot('position',[0.08,0.1,0.9,0.28]);
% hold on
% grid on
% box on
% xlim(days(PeriodLimits))
% ylabel(ax3,'phase lag (deg)')
% xlabel('period (days)')
% xticks(10.^(0:3))
% pcell = gobjects(numel(v1cell),1);
% pcell2 = gobjects(numel(v1cell2),1);
% pcell3 = gobjects(numel(v1cell3),1);
ploth = 0.065;
plotdh = 0;
for ii = 1:length(legcell)
    ploty = 0.05+(ploth+plotdh)*(ii-1);
    ax = subplot('position',[plotx,ploty,plotw,ploth]);
    hold on
    grid on
    box on
    set(gca,'FontSize',11)
    xlim(days(PeriodLimits))
    xticks(10.^(0:3))

    [wcoh,wcsh,period,coi] = wcoherence(v1cell{ii},v2cell{ii},ts,'PeriodLimits',PeriodLimits,...
        'VoicesPerOctave',VoicesPerOctave,'NumScalesToSmooth',NumScalesToSmooth);
    mask = ones(size(wcoh));
    for jj = 1:length(coi)
        mask(period>coi(jj),jj) = 0;
    end
    weight = mask;
    data = sum(abs(wcsh).^2.*weight,2)./sum(weight,2);
    weight = mask.*abs(wcsh).^2;
    wcsh = wcsh./abs(wcsh); %scale to magnitude 1
    cdata = wrapTo180(rad2deg(angle(sum(wcsh.*weight,2)))); % ./sum(weight,2))); % circular mean 
    periodinterp = logspace(log10(days(period(1))),log10(days(period(end))),5000);
    datainterp = interp1(period,data,periodinterp);
    cdatainterp = interp1(period,cdata,periodinterp,'nearest');
    sig95interp = interp1(period,sig95,periodinterp);
    sig75interp = interp1(period,sig75,periodinterp);
%     if ii <=3
%     %     plot(ax3,days(period),cdata,...
%     %         vsymcell{ii},'Color',vccell{ii},'LineWidth',1)
%         scatter(ax2,periodinterp,datainterp,9,cdatainterp,vscatcell2{ii},'filled');
%         pcell2(ii) = plot(ax2,days(period),data,vsymcell2{ii},'Color',vccell2{ii},'LineWidth',1.0);
%     else
%     %     plot(ax3,days(period),cdata,...
%     %         vsymcell{ii},'Color',vccell{ii},'LineWidth',1)
%         scatter(ax3,periodinterp,datainterp,9,cdatainterp,vscatcell3{ii-3},'filled');
%         pcell3(ii-3) = plot(ax3,days(period),data,vsymcell3{ii-3},'Color',vccell3{ii-3},'LineWidth',1.0);
%     end
    %plot(ax,periodinterp,sig95interp,'k');
    area(ax,periodinterp,sig95interp,'EdgeColor','none','FaceColor',[0.7,0.7,0.7])
    set(ax,'Layer','top')
%     plot(ax,periodinterp,sig75interp,'k--');
    scatter(ax,periodinterp,datainterp,9,cdatainterp,'o','filled');
    p = plot(ax,days(period),data,':','Color','k','LineWidth',1.0);
    
    
    ylim(ax,[0.2,1])
    xticks([10,50,100,500])
    set(ax,'XScale','log')
    annotation('textbox',[plotx+plotw+plotlegdw,ploty,plotlegw,ploth],'String',legcell{ii},...
        'FitBoxToText','on','EdgeColor','none');
    caxis(ax,[-180,180])
    cmapr = [linspace(0,0,10),linspace(0,1,10),linspace(1,1,10),linspace(1,0,10)]';
    cmapg = [linspace(0,0,10),linspace(0,1,10),linspace(1,0,10),linspace(0,0,10)]';
    cmapb = [linspace(0,1,10),linspace(1,1,10),linspace(1,0,10),linspace(0,0,10)]';
    cmap = [cmapr,cmapg,cmapb];
    colormap(ax,cmap)
    if ii == 1
        cbar = colorbar(ax);
        ylabel(cbar,'phase lag (deg)','Rotation',90)
        cbar.XTick = [-90,0,90];
        xlabel('period (days)')
        ylabel('coherence')
        yticks([0.4,0.6,0.8])
    else
        xticklabels([])
        yticks([0.4,0.6,0.8])
    end
    if ii == length(legcell)
        title('b.')
    end
end
% ylim(ax2,[0,1])
% set(ax2,'XScale','log')
% legend(ax2,pcell2,legcell2)
% cbar = colorbar(ax2);
% ylabel(cbar,'phase lag (deg)','Rotation',0)
% cbar.XTick = [-90,0,90];
% caxis(ax2,[-180,180])
% cmapr = [linspace(0.4,0.4,10),linspace(0.4,0.9,10),linspace(0.9,1,10),linspace(1,0.4,10)]';
% cmapg = [linspace(0.4,0.4,10),linspace(0.4,0.9,10),linspace(0.9,0.4,10),linspace(0.4,0.4,10)]';
% cmapb = [linspace(0.4,1,10),linspace(1,0.9,10),linspace(0.9,0.4,10),linspace(0.4,0.4,10)]';
% cmap = [cmapr,cmapg,cmapb];
% colormap(ax2,cmap)
% ylim(ax3,[0,1])
% set(ax3,'XScale','log')
% legend(ax3,pcell3,legcell3)
% cbar = colorbar(ax3);
% ylabel(cbar,'phase lag (deg)','Rotation',0)
% cbar.XTick = [-90,0,90];
% caxis(ax3,[-180,180])
% colormap(ax3,cmap)
% ylim(ax3,[-180,180])
% set(ax3,'XScale','log')

%}

%% modeled conduit reservoir mode grid search
%{
keepind = TQ_tab.cr &...
    TQ_tab.amp_abovenoise > log10(3.0) & ...
    TQ_tab.std_abovenoise > log10(3.0) & ...
    TQ_tab.Q > 6 & ...
    TQ_tab.mean_min50_errphase < 0.1 &...
    TQ_tab.channelCount > 0 & ...
    (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2) & ...
    TQ_tab.start > datetime(2009,6,1);% & ...
    %TQ_tab.start > datetime(2014,7,23,20,14,0);
% keepind = TQ_tab.cr &...
%     TQ_tab.amp_abovenoise > log10(3.0) & ...
%     TQ_tab.std_abovenoise > log10(3.0) & ...
%     TQ_tab.Q > 6 & ...
%     TQ_tab.mean_min50_errphase < 0.1 &...
%     TQ_tab.channelCount > 5 & ...
%     (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2);
VLP_tab_cr = TQ_tab(keepind,:);
VLP_tab_cr = sortrows(VLP_tab_cr,'start');

% 'only keeping 1 in 30 events'
% VLP_tab_cr = VLP_tab_cr(1:30:end,:);

%load('conduit_reservoir_oscillator/TQgrid_volatile_bessel_nonlin_fixedgeom_forinv.mat');
% load('conduit_reservoir_oscillator/TQgrid_volatile_bessel_nonlin_fixedgeom_forinv_justCO2.mat');
% load('conduit_reservoir_oscillator/TQgrid_bessel_nonlin_forinv_00.mat');
% load('conduit_reservoir_oscillator/TQgrid_bessel_nonlin_forinv_1seg.mat');
load('conduit_reservoir_oscillator/TQgrid_volatile_bessel_nonlin_fixedgeom_forinv_ratio_00.mat');

% 'converting temp to C'
% in = find(strcmp('TK',p.gridaxes));
% if ~isempty(in)
%     p.gridaxes{in} = 'T';
%     p.gridaxes_val{in} = p.gridaxes_val{in} - 273.15;
%     p.gridaxes_text{in} = 'Temp (C)';
% end
% 
% 'removing high CO2 contents'
% p.thermo_unstable_grid(:,p.gridaxes_val{2}>5E4,:,:,:,:) = true;
% p.thermo_unstable_grid(:,:,p.gridaxes_val{3}>5E4,:,:,:) = true;
% p.thermo_unstable_grid(:,:,:,p.gridaxes_val{4}>5E4,:,:) = true;

% 'removing H2O_laketop'
% ind = 4;
% p.T_grid = squeeze(p.T_grid(:,ind,:,:,:,:,:));
% p.Q_grid = squeeze(p.Q_grid(:,ind,:,:,:,:,:));
% p.phimax_grid = squeeze(p.phimax_grid(:,ind,:,:,:,:,:));
% p.thermo_unstable_grid = squeeze(p.thermo_unstable_grid(:,ind,:,:,:,:,:));
% p.P_restop_grid = squeeze(p.P_restop_grid(:,ind,:,:,:,:,:));
% p.gridaxes = p.gridaxes([1,3,4,5,6,7]);
% p.gridaxes_val = p.gridaxes_val([1,3,4,5,6,7]);
% p.gridaxes_text = p.gridaxes_text([1,3,4,5,6,7]);


Rlakein = find(strcmp(p.gridaxes,'Rlake'));
Zlakein = find(strcmp(p.gridaxes,'Zlaketop'));
Rlakevec = p.gridaxes_val{Rlakein};
Zlakevec = p.gridaxes_val{Zlakein};
keepgrid_in = setdiff(1:length(p.gridaxes),[Rlakein,Zlakein]);
gridaxes_keep = p.gridaxes(keepgrid_in);
gridaxes_val_keep = p.gridaxes_val(keepgrid_in);
gridaxes_text_keep = p.gridaxes_text(keepgrid_in);
griddim_keep = zeros(size(gridaxes_val_keep));
for ii = 1:length(griddim_keep)
    griddim_keep(ii) = numel(gridaxes_val_keep{ii});
end
gridin = cell(size(p.gridaxes));
for ii = 1:length(p.gridaxes)
    gridin{ii} = 1:length(p.gridaxes_val{ii});
end

for ii = 1:length(gridaxes_text_keep)
    gridaxes_text_keep{ii} = strrep(gridaxes_text_keep{ii},' ','\newline');
end

% 'downscaling bigger than 1000'
% for ii = 1:length(gridaxes_val_keep)
%     if max(gridaxes_val_keep{ii}) > 1800
%         gridaxes_text_keep{ii} = strcat(gridaxes_text_keep{ii},' \times10^3');
%         gridaxes_val_keep{ii} = gridaxes_val_keep{ii}/1000;
%     end
% end

% 'shifting P_restop'
% max_P_restop = max(VLP_tab_cr.lakeh - (p.Zrescentroid + p.Rres))*9.8*2000; %maximim possible pressure
% in = VLP_tab_cr.start >= datetime(2014,7,23,20,14,0);
% shift = max_P_restop - max(VLP_tab_cr.P_restop(in));
% VLP_tab_cr.P_restop(in) = VLP_tab_cr.P_restop(in) + shift;
% in = P_table.Time >= datetime(2014,7,23,20,14,0);
% P_table.P(in) = P_table.P(in) + shift;
% 
% 'different P_restop shift where disp change 2015'
% max_P_restop = max(VLP_tab_cr.lakeh - (p.Zrescentroid + p.Rres))*9.8*2000; %maximim possible pressure
% in = VLP_tab_cr.start < datetime(2015,10,25,0,10,0);
% shift = max_P_restop - max(VLP_tab_cr.P_restop(in));
% VLP_tab_cr.P_restop(in) = VLP_tab_cr.P_restop(in) + shift;
% in = P_table.Time < datetime(2015,10,25,0,10,0);
% P_table.P(in) = P_table.P(in) + shift;
% 
% 'different P_restop shift where disp change 2014'
% max_P_restop = max(VLP_tab_cr.lakeh - (p.Zrescentroid + p.Rres))*9.8*2000; %maximim possible pressure
% in = VLP_tab_cr.start < datetime(2014,7,23,20,14,0);
% shift = max_P_restop - max(VLP_tab_cr.P_restop(in));
% VLP_tab_cr.P_restop(in) = VLP_tab_cr.P_restop(in) + shift;
% in = P_table.Time < datetime(2014,7,23,20,14,0);
% P_table.P(in) = P_table.P(in) + shift;

%{
'filtering P_restop'
VLP_tab_cr.P_restop_old = VLP_tab_cr.P_restop;
P_table.P_old = P_table.P;
filt = designfilt('highpassfir', ...       % Response type
       'StopbandFrequency',1/(6*365), ...     % Frequency constraints
       'PassbandFrequency',1/(12*30), ...
       'StopbandAttenuation',40, ...    % Magnitude constraints
       'PassbandRipple',1, ...
       'DesignMethod','kaiserwin', ...  % Design method
   'SampleRate',1/days(P_table.Time(2)-P_table.Time(1)));               % Sample rate (days)
%fvtool(filt,'MagnitudeDisplay','zero-phase','Fs',1/days(P_table.Time(2)-P_table.Time(1)))
delay = mean(grpdelay(filt));
P_filt = P_table.P - mean(P_table.P);
% P_filt2 = smooth(P_filt,61,'lowess');
% P_filt2 = P_filt - P_filt2;
% P_filt3 = fit((1:length(P_filt))',P_filt,'smoothingspline','SmoothingParam',0.01);
% P_filt3 = P_filt3((1:length(P_filt))');
% P_filt3 = P_filt - P_filt3;
cutin = find(P_table.Time > datetime(2018,5,5,0,0,0),1);
P_filt(cutin:end) = P_filt(cutin);
padlen = floor(length(P_filt)/2);
P_filt = [zeros(padlen,1)+P_filt(1);P_filt;zeros(padlen,1)+P_filt(end)];
taper = tukeywin(length(P_filt),0.25);
P_filt = P_filt.*taper;
P_filt = filter(filt,P_filt);
P_filt = P_filt(1+padlen+delay:end-padlen+delay);
P_filt(cutin:end) = P_table.P_old(cutin:end) + P_filt(cutin)-P_table.P_old(cutin);
P_table.P = P_filt;
% figure(),plot(P_table.Time,P_table.P_old,...
%     P_table.Time,P_table.P)%,...
% %     P_table.Time,P_filt2,...
% %     P_table.Time,P_filt3)
%}
VLP_tab_cr.P_restop = interp1(P_table.Time,P_table.P,VLP_tab_cr.start,'linear');
max_P_restop = max(VLP_tab_cr.lakeh - (p.Zrescentroid + p.Rres))*9.8*1750; %maximim possible pressure
shift = (700 - (p.Zrescentroid + p.Rres))*9.8*1400;
VLP_tab_cr.P_restop = VLP_tab_cr.P_restop + shift;

s = table2struct(VLP_tab_cr);

'normalizing pdf-means cant compare between times'
normalizepdf = false;

% numcores = feature('numcores'); 
% poolobj = gcp('nocreate'); 
% if isempty(poolobj)
%     parpool(numcores)
% end
'not using parfor'
for ii = 1:height(VLP_tab_cr)
    
    Rlake = sqrt(VLP_tab_cr.lakeA(ii)*10^4/pi);
%     if isnan(Rlake)
%         Rlake = sqrt(nanmax(VLP_tab_cr.lakeA)*10^4/pi);
%     end
    Zlake = VLP_tab_cr.lakeh(ii);
    
    %interpolation
    Rlakein_low = find(Rlakevec <= Rlake,1,'last');
    if isempty(Rlakein_low) %if Rlake below smallest vec value
        Rlakein_low = 1;
        Rlakefrac = 0;
    elseif Rlakein_low == length(Rlakevec) %if Rlake above largest vec value
        Rlakein_low = length(Rlakevec) - 1;
        Rlakefrac = 1;
    else
        Rlakefrac = (Rlake-Rlakevec(Rlakein_low))/(Rlakevec(Rlakein_low+1)-Rlakevec(Rlakein_low));
    end
    lowin = gridin;
    lowin{Rlakein} = [Rlakein_low];
    highin = gridin;
    highin{Rlakein} = [Rlakein_low+1];
    T_grid = p.T_grid(lowin{:})*Rlakefrac + p.T_grid(highin{:})*(1-Rlakefrac);
    Q_grid = p.Q_grid(lowin{:})*Rlakefrac + p.Q_grid(highin{:})*(1-Rlakefrac);
    P_restop_grid = p.P_restop_grid(lowin{:})*Rlakefrac + p.P_restop_grid(highin{:})*(1-Rlakefrac);
    phimax_grid = p.phimax_grid(lowin{:})*Rlakefrac + p.phimax_grid(highin{:})*(1-Rlakefrac);
    unstable_grid = p.thermo_unstable_grid(lowin{:})*Rlakefrac + p.thermo_unstable_grid(highin{:})*(1-Rlakefrac) > 0.9;
    
    Zlakein_low = find(Zlakevec <= Zlake,1,'last');
    if isempty(Zlakein_low) %if Rlake below smallest vec value
        Zlakein_low = 1;
        Zlakefrac = 0;
    elseif Zlakein_low == length(Zlakevec) %if Rlake above largest vec value
        Zlakein_low = length(Zlakevec) - 1;
        Zlakefrac = 1;
    else
        Zlakefrac = (Zlake-Zlakevec(Zlakein_low))/(Zlakevec(Zlakein_low+1)-Zlakevec(Zlakein_low));
    end
    lowin = gridin(setdiff(1:length(gridin),Rlakein));
    if Zlakein > Rlakein
        Zlakein_temp = Zlakein - 1;
    else
        Zlakein_temp = Zlakein;
    end
    lowin{Zlakein_temp} = [Zlakein_low];
    highin = gridin(setdiff(1:length(gridin),Rlakein));
    highin{Zlakein_temp} = [Zlakein_low+1];
    T_grid = T_grid(lowin{:})*Zlakefrac + T_grid(highin{:})*(1-Zlakefrac);
    Q_grid = Q_grid(lowin{:})*Zlakefrac + Q_grid(highin{:})*(1-Zlakefrac);
    P_restop_grid = P_restop_grid(lowin{:})*Zlakefrac + P_restop_grid(highin{:})*(1-Zlakefrac);
    phimax_grid = phimax_grid(lowin{:})*Zlakefrac + phimax_grid(highin{:})*(1-Zlakefrac);
    unstable_grid = unstable_grid(lowin{:})*Zlakefrac + unstable_grid(highin{:})*(1-Zlakefrac) > 0.9;
    
    misfitgrid = abs((T_grid-VLP_tab_cr.T(ii))/VLP_tab_cr.T(ii)).^1 + ...
    abs((Q_grid-VLP_tab_cr.Q(ii))/VLP_tab_cr.Q(ii)).^1 + ...
    10*abs((P_restop_grid-VLP_tab_cr.P_restop(ii))/VLP_tab_cr.P_restop(ii)).^1;
%     misfitgrid = (abs((T_grid-VLP_tab_cr.T(ii))/1).^1 + ...
%         abs((Q_grid-VLP_tab_cr.Q(ii))/1).^1 + ...
%         abs((P_restop_grid-VLP_tab_cr.P_restop(ii))/1E5).^1);

    misfitgrid(logical(unstable_grid)) = inf;
%     misfitgrid(phimax_grid > 0.9) = inf;
    
    s(ii).minmisfit = cell(size(gridaxes_keep));
    s(ii).gridaxes_val = gridaxes_val_keep;
    s(ii).gridaxes_text = gridaxes_text_keep;
    for jj = 1:length(gridaxes_keep)
        s(ii).minmisfit{jj} = squeeze(nanmin(misfitgrid,[],setdiff(1:length(gridaxes_keep),jj)));
    end
    
    pdfgrid = exp(-misfitgrid);
    
    %don't want to normalize each time because then can't compare between times
%     pdfgrid = pdfgrid/sum(pdfgrid(:),'omitnan');

    s(ii).pdf = cell(size(gridaxes_keep));
    for jj = 1:length(gridaxes_keep)
        temp_pdf = squeeze(sum(pdfgrid,setdiff(1:length(gridaxes_keep),jj),'omitnan'));
        total_numel = prod(griddim_keep(setdiff(1:length(gridaxes_keep),jj)));
        s(ii).pdf{jj} = temp_pdf/total_numel;
        
        if normalizepdf
            s(ii).pdf{jj} = s(ii).pdf{jj}/max(s(ii).pdf{jj});
        end
    end
end

% FileName = 'catalog_grid_inv_struct.mat';
% FileName = avoidOverwrite(FileName,pwd,2,0);
% save(FileName, 's','-v7.3')


%% grid plots
% moving average
Tinterp = days(2);
Tavg = days(7);
sa = struct('date',num2cell(datetime(2008,0,1):Tinterp:datetime(2018,7,1)));
sstart = [s.start];
for ii = 1:length(sa)
    sa(ii).minmisfit = cell(size(gridaxes_keep));
    sa(ii).pdf = cell(size(gridaxes_keep));
    sa(ii).gridaxes_val = gridaxes_val_keep;
    sa(ii).gridaxes_text = gridaxes_text_keep;
    
    dataind = find(sstart < sa(ii).date + Tavg/2 & ...
        sstart > sa(ii).date - Tavg/2);
    if length(dataind) == 1
        for jj = 1:length(gridaxes_keep)
            sa(ii).minmisfit{jj} = s(dataind).minmisfit{jj};
            sa(ii).pdf{jj} = s(dataind).pdf{jj};
        end
    elseif length(dataind) > 1
        for kk = 1:length(dataind)
            for jj = 1:length(gridaxes_keep)
                if kk == 1
                    sa(ii).minmisfit{jj} = s(dataind(kk)).minmisfit{jj}/length(dataind);
                    sa(ii).pdf{jj} = s(dataind(kk)).pdf{jj}/length(dataind);
                else
                    sa(ii).minmisfit{jj} = sa(ii).minmisfit{jj} + s(dataind(kk)).minmisfit{jj}/length(dataind);
                    sa(ii).pdf{jj} = sa(ii).pdf{jj} + s(dataind(kk)).pdf{jj}/length(dataind);
                end
            end
        end
    else
        for jj = 1:length(gridaxes_keep)
            sa(ii).minmisfit{jj} = NaN*s(1).minmisfit{jj};
            sa(ii).pdf{jj} = NaN*s(1).pdf{jj};
        end
    end
end

%plots
figure()
Tlim = [5,43];
Xlim = [datetime(2009,6,1),datetime(2018,7,1)];
heightincrease = 0.01;

ax1 = subplot(2+length(gridaxes_keep),1,1);
ax1.Position = ax1.Position+[0,0,0,heightincrease];
%fill(inflateplotx,inflateploty*100,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
yyaxis('left')
scatter(VLP_tab_cr.start,VLP_tab_cr.T,6,[0,0,1],'filled'), hold on
grid on, grid minor, box on
ylabel('T (s)')
ylim(Tlim)
xlim(Xlim);%max(VLP_tab.start)])
yyaxis('right')
scatter(VLP_tab_cr.start,VLP_tab_cr.Q,6,[1,0,0],'filled'), hold on
grid on, grid minor, box on
ylabel('Q')
ylim([5,70])
xline(datetime(2008,3,19),'k',{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k',{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k',{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k',{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
ttl = title('a.'); ttl.Position(1) = ttl.Position(1)/12;

ax2 = subplot(2+length(gridaxes_keep),1,2);
ax2.Position = ax2.Position+[0,0,0,heightincrease];
%fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
yyaxis('left')
p1 = plot(lakeh.date,lakeh.h,'b');
ylabel('lava-lake el. (m)')
hold on
grid on, grid minor
ylim([750,1050])
yyaxis('right')
%p2 = scatter(VLP_tab_cr.start,VLP_tab_cr.P_restop,'r.');
p2 = plot(P_table.Time,P_table.P+shift,'r');
% xline(datetime(2014,7,23,20,14,0),'Color','m','LineWidth',2)
% xline(datetime(2015,10,25,0,10,0),'Color','m','LineWidth',2)
ylabel(' inverted reservoir\newlinepressure (Pa)','HorizontalAlignment','center')
ylim([0E6,8.5E6])
xlim(Xlim)
xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
% xticklabels([])

plotpdf = false;
if ~plotpdf
%     cmap = flipud(cmocean('haline',256));
%     N = 60;
%     cmapend = [linspace(cmap(end,1),1,N)',...
%         linspace(cmap(end,2),1,N)',...
%         linspace(cmap(end,3),1,N)'];
%     cmap = [cmap;cmapend];
%     cmapstart = repmat(cmap(1,:),40,1);
%     cmap = [cmapstart;cmap];
    cmap = [flipud(parula(3));1,1,1];
else
    cmap = (cmocean('rain',5));
end
for ii = 1:length(gridaxes_keep)
    ax = subplot(2+length(gridaxes_keep),1,2+ii);
    ax.Position = ax.Position+[0,0,0,heightincrease];
    %fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
    %hold on
    %{
    Tspan = days(6);
    pcolx = NaT(length(s(1).gridaxes_val{ii}),1);
    pcoly = NaN(length(s(1).gridaxes_val{ii}),1);
    pcolc = NaN(length(s(1).gridaxes_val{ii}),1);
    counter = 1;
    for jj = 1:length(s)
%         scatter(s(jj).start+0*s(jj).gridaxes_val{ii}, s(jj).gridaxes_val{ii},...
%             4, s(jj).minmisfit{ii}, 'filled', 's')
%         hold on
        pcolx(:,counter) = s(jj).start+seconds(0*s(jj).gridaxes_val{ii});
        pcoly(:,counter) = s(jj).gridaxes_val{ii};
        pcolc(:,counter) = s(jj).minmisfit{ii};
        counter = counter + 1;
        if jj == length(s)
            extra = true;
        elseif s(jj).start + Tspan < s(jj+1).start
            extra = true;
        else
            extra = false;
        end
        if extra
            pcolx(:,counter) = s(jj).start+seconds(0*s(jj).gridaxes_val{ii}) + Tspan;
            pcoly(:,counter) = s(jj).gridaxes_val{ii};
            pcolc(:,counter) = s(jj).minmisfit{ii};
            counter = counter + 1;
            pcolx(:,counter) = s(jj).start+seconds(0*s(jj).gridaxes_val{ii}) + Tspan + seconds(1);
            pcoly(:,counter) = s(jj).gridaxes_val{ii};
            pcolc(:,counter) = NaN*s(jj).minmisfit{ii};
            counter = counter + 1;
        end
    end
    %}
    pcolx = NaT(length(sa(1).gridaxes_val{ii}),2*length(sa));
    pcoly = NaN(length(sa(1).gridaxes_val{ii}),2*length(sa));
    pcolc = NaN(length(sa(1).gridaxes_val{ii}),2*length(sa));
    for jj = 1:length(sa)
        pcolx(:,2*jj-1) = sa(jj).date-Tinterp+seconds(1);
        pcoly(:,2*jj-1) = sa(jj).gridaxes_val{ii};
        if ~plotpdf
            pcolc(:,2*jj-1) = sa(jj).minmisfit{ii};
        else
            pcolc(:,2*jj-1) = sa(jj).pdf{ii};
        end
        pcolx(:,2*jj) = sa(jj).date+Tinterp-seconds(1);
        pcoly(:,2*jj) = sa(jj).gridaxes_val{ii};
        if ~plotpdf
            pcolc(:,2*jj) = sa(jj).minmisfit{ii};
        else
            pcolc(:,2*jj) = sa(jj).pdf{ii};
        end
    end
    pcol = pcolor(pcolx, pcoly, pcolc);
    pcol.EdgeColor = 'none';
    pcol.FaceColor = 'interp';
    colormap(ax,cmap)
    if ~plotpdf
        caxis([0,1])
    else
        caxis([0,max(pcolc(:))])
    end
    grid on
    ylabel(gridaxes_text_keep{ii})
    xlim(Xlim)
    if range(diff(gridaxes_val_keep{ii})) > min(diff(gridaxes_val_keep{ii}))
        set(gca,'YScale','Log')
    end
    xline(datetime(2008,3,19),'k','LineWidth',0.8)%,{'Crater'},'LabelVerticalAlignment','bottom')
    xline(datetime(2010,2,1),'k','LineWidth',0.8)%,{'SSE'},'LabelVerticalAlignment','bottom')
    xline(datetime(2012,5,25),'k','LineWidth',0.8)%,{'SSE'},'LabelVerticalAlignment','bottom')
    xline(datetime(2015,10,15),'k','LineWidth',0.8)%,{'SSE'},'LabelVerticalAlignment','bottom')
    xline(datetime(2011,3,5),'k','LineWidth',0.8)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(datetime(2011,8,3),'k','LineWidth',0.8)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(datetime(2011,9,21),'k','LineWidth',0.8)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(datetime(2014,6,27),'k','LineWidth',0.8)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(datetime(2016,5,24),'k','LineWidth',0.8)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(datetime(2018,4,30),'k','LineWidth',0.8)%,{'ERZ'},'LabelVerticalAlignment','bottom')
    xline(datetime(2012,10,26),'k','LineWidth',0.8)%,{'Int'},'LabelVerticalAlignment','bottom')
    xline(datetime(2014,5,10),'k','LineWidth',0.8)%,{'Int'},'LabelVerticalAlignment','bottom')
    xline(datetime(2015,5,9),'k','LineWidth',0.8)%,{'Int'},'LabelVerticalAlignment','bottom')
    set(gca,'Layer','top')
    cbar = colorbar;
    cbar.Position = cbar.Position + [0.09,0,0,0];
    cbar.Position(3) = 0.01;
    if ~plotpdf
        ylabel(cbar,'min misfit')
    else
        ylabel(cbar,'PDF')
    end
%     xline(datetime(2014,7,23,20,14,0),'Color','m','LineWidth',2)
%     xline(datetime(2015,10,25,0,10,0),'Color','m','LineWidth',2)
%     if ii < length(gridaxes_keep)
%         xticklabels([])
%     end
end
%}

%% direct costfun
function cost = direct_costfun(y, yscale, T, Q, P_restop,...
        conddip,...%conduit dip angle (deg)
        Rres,...%reservoir radius
        Rcondtop,... %top conduit radius
        Rcondbot,... %bottom conduit radius
        G,... %elastic wall rock shear modulus
        Rlake,...%lake radius
        Hcolumn,... %conduit depth (including lake)
        Hlake,...%lake height
        betam_res,... %reservoir compressibility
        min_rho_laketop)
    
    y = y.*yscale;
    mu = y(1);
    rho_condbot = y(2);
    rho_condtop = y(3);
    
    mu_condbot = mu;
    mu_condtop = mu;
    mu_lakebot = mu;
    mu_laketop = mu;
    
%     Lcond = Hcolumn - Hlake;
%     rho_condtop = rho_condbot + Lcond/Hcolumn...
%         *(rho_laketop - rho_condbot);
%     rho_lakebot = rho_condtop;
%     unstable = rho_laketop > rho_condbot;
    rho_laketop = min_rho_laketop;
    rho_lakebot = rho_condtop;
    unstable = rho_condtop > rho_condbot;
    
    if ~unstable
%         try
            [Tpred,Qpred,~,~,~,P_restoppred] = ...
              T_Q_driver_nonlin_bessel_fun(...
                conddip,...%conduit dip angle (deg)
                Rres,...%reservoir radius
                Rcondtop,... %top conduit radius
                Rcondbot,... %bottom conduit radius
                G,... %elastic wall rock shear modulus
                Rlake,...%lake radius
                Hcolumn,... %conduit depth (including lake)
                rho_laketop,... %lake top density
                rho_lakebot,... %lake bot density
                rho_condtop,... %cond top density
                rho_condbot,... %cond bot density
                Hlake,...%lake height
                mu_laketop,... %lake top magma visc
                mu_lakebot,... %lake bot magma visc
                mu_condtop,... %cond top magma visc
                mu_condbot,... %cond bot magma visc
                betam_res); %reservoir compressibility

            cost = [((T-Tpred)/T);...
                ((Q-Qpred)/Q);...
                10*((P_restop-P_restoppred)/P_restop)];
%         catch
%             cost = inf;
%         end
    else
        cost = [inf;inf;inf];
    end
end


%% volatile costfun
function cost = volatile_costfun(y, yscale, T, Q, P_restop,...
        conddip,...%conduit dip angle (deg)
        Rres,...%reservoir radius
        Rcondtop,... %conduit top radius
        Rcondbot,... %conduit bottom radius
        G,... %elastic wall rock shear modulus
        Rlake,... %lava lake radius
        Hcolumn,... %conduit depth (including lake)
        H2O_condtop_wtp,... %wtp
        H2O_condbot_wtp,... %wtp\
        H2O_laketop_wtp,... %wtp
        H2O_lakebot_wtp,... %wtp
        Hlake,...%lake depth
        plotprofiles,...
        CO2solppm_table, H2Osolwtp_table,dz,TolX)
    
    y = y.*yscale;
    TK = y(1) + 273.15;
    CO2_condbot_ppm = y(2);
    CO2_condtop_ppm = y(3);
%     CO2_laketop_ppm = y(3);
    
    TK_condbot = TK;
    TK_condtop = TK;
    TK_lakebot = TK;
    TK_laketop = TK;
    
    CO2_laketop_ppm = CO2_condtop_ppm;
%     Lcond = Hcolumn-Hlake;    
%     CO2_condtop_ppm = CO2_condbot_ppm + ...
%         Lcond/Hcolumn...
%         *(CO2_laketop_ppm - CO2_condbot_ppm);
    CO2_lakebot_ppm = CO2_condtop_ppm;

    
%     try
        [Tpred,Qpred,~,~,~,unstable,~,P_restoppred] = ...
          T_Q_driver_volatile_nonlin_bessel_fun(...
            conddip,...%conduit dip angle (deg)
            Rres,...%reservoir radius
            Rcondtop,... %conduit top radius
            Rcondbot,... %conduit bottom radius
            G,... %elastic wall rock shear modulus
            Rlake,... %lava lake radius
            Hcolumn,... %conduit depth (including lake)
            H2O_condtop_wtp,... %wtp
            H2O_condbot_wtp,... %wtp
            CO2_condtop_ppm,... %ppm
            CO2_condbot_ppm,... %ppm
            H2O_laketop_wtp,... %wtp
            H2O_lakebot_wtp,... %wtp
            CO2_laketop_ppm,... %ppm
            CO2_lakebot_ppm,... %ppm
            Hlake,...%lake depth
            TK_condbot,...%conduit bottom Temp (Kelvin)
            TK_condtop,...%conduit top Temp (Kelvin)
            TK_lakebot,...%lake bottom Temp (Kelvin)
            TK_laketop,...%lake top Temp (Kelvin)
            plotprofiles,...
            CO2solppm_table, H2Osolwtp_table,dz,TolX); 

        cost = [((T-Tpred)/T), ...
            ((Q-Qpred)/Q), ...
            10*((P_restop-P_restoppred)/P_restop)];

        if unstable
            cost = [inf,inf,inf];
        end
%     catch
%         cost = inf;
%     end
end


%% volatile (H2O) costfun
function cost = volatile_H2O_costfun(y, yscale, T, Q, P_restop,...
        conddip,...%conduit dip angle (deg)
        Rres,...%reservoir radius
        Rcondtop,... %conduit top radius
        Rcondbot,... %conduit bottom radius
        G,... %elastic wall rock shear modulus
        Rlake,... %lava lake radius
        Hcolumn,... %conduit depth (including lake)
        CO2_condtop_ppm,... %ppm
        CO2_condbot_ppm,... %ppm
        CO2_laketop_ppm,... %ppm
        CO2_lakebot_ppm,... %ppm
        Hlake,...%lake depth
        plotprofiles,...
        CO2solppm_table, H2Osolwtp_table,dz,TolX)
    
    y = y.*yscale;
    TK = y(1) + 273.15;
    H2O_condbot_wtp = y(2);
    H2O_condtop_wtp = y(3);
%     H2O_laketop_wtp = y(3);
    
    TK_condbot = TK;
    TK_condtop = TK;
    TK_lakebot = TK;
    TK_laketop = TK;
    
    H2O_laketop_wtp = H2O_condtop_wtp;
%     Lcond = Hcolumn-Hlake;    
%     H2O_condtop_wtp = H2O_condbot_wtp + ...
%         Lcond/Hcolumn...
%         *(H2O_laketop_wtp - H2O_condbot_wtp);
    H2O_lakebot_wtp = H2O_condtop_wtp;

    
%     try
        [Tpred,Qpred,~,~,~,unstable,~,P_restoppred] = ...
          T_Q_driver_volatile_nonlin_bessel_fun(...
            conddip,...%conduit dip angle (deg)
            Rres,...%reservoir radius
            Rcondtop,... %conduit top radius
            Rcondbot,... %conduit bottom radius
            G,... %elastic wall rock shear modulus
            Rlake,... %lava lake radius
            Hcolumn,... %conduit depth (including lake)
            H2O_condtop_wtp,... %wtp
            H2O_condbot_wtp,... %wtp
            CO2_condtop_ppm,... %ppm
            CO2_condbot_ppm,... %ppm
            H2O_laketop_wtp,... %wtp
            H2O_lakebot_wtp,... %wtp
            CO2_laketop_ppm,... %ppm
            CO2_lakebot_ppm,... %ppm
            Hlake,...%lake depth
            TK_condbot,...%conduit bottom Temp (Kelvin)
            TK_condtop,...%conduit top Temp (Kelvin)
            TK_lakebot,...%lake bottom Temp (Kelvin)
            TK_laketop,...%lake top Temp (Kelvin)
            plotprofiles,...
            CO2solppm_table, H2Osolwtp_table,dz,TolX); 

        cost = [((T-Tpred)/T), ...
            ((Q-Qpred)/Q), ...
            10*((P_restop-P_restoppred)/P_restop)];

        if unstable
            cost = [inf,inf,inf];
        end
%     catch
%         cost = inf;
%     end
end


%% volatile (ratio) costfun
function cost = volatile_ratio_costfun(y, yscale, T, Q, P_restop,...
        conddip,...%conduit dip angle (deg)
        Rres,...%reservoir radius
        Rcondtop,... %conduit top radius
        Rcondbot,... %conduit bottom radius
        G,... %elastic wall rock shear modulus
        Rlake,... %lava lake radius
        Hcolumn,... %conduit depth (including lake)
        H2O_wtp_frac,...
        Hlake,...%lake depth
        plotprofiles,...
        CO2solppm_table, H2Osolwtp_table,dz,TolX,...
        fixed_laketop,fixed_vol_laketop_wtp)
    
    y = y.*yscale;
    TK = y(1) + 273.15;
    vol_condbot_wtp = y(2);
    vol_condtop_wtp = y(3);
%     H2O_laketop_wtp = y(3);
    
    TK_condbot = TK;
    TK_condtop = TK;
    TK_lakebot = TK;
    TK_laketop = TK;
    
    vol_laketop_wtp = vol_condtop_wtp;
%     Lcond = Hcolumn-Hlake;    
%     vol_condtop_wtp = vol_condbot_wtp + ...
%         Lcond/Hcolumn...
%         *(vol_laketop_wtp -vol_condbot_wtp);
    vol_lakebot_wtp = vol_condtop_wtp;
    
    wtp_CO2_to_ppm = 1/(7.0778e-05); %approximate
    CO2_wtp_frac = 1-H2O_wtp_frac;
    
    H2O_condtop_wtp = vol_condtop_wtp*H2O_wtp_frac;
    H2O_condbot_wtp = vol_condbot_wtp*H2O_wtp_frac;
    CO2_condtop_ppm = vol_condtop_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;
    CO2_condbot_ppm = vol_condbot_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;  
    H2O_lakebot_wtp = vol_lakebot_wtp*H2O_wtp_frac;
    CO2_lakebot_ppm = vol_lakebot_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;
    if ~fixed_laketop
        H2O_laketop_wtp = vol_laketop_wtp*H2O_wtp_frac;
        CO2_laketop_ppm = vol_laketop_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;
    else
        H2O_laketop_wtp = fixed_vol_laketop_wtp*H2O_wtp_frac;
        CO2_laketop_ppm = fixed_vol_laketop_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;
    end
    
%     try
        [Tpred,Qpred,~,~,~,unstable,~,P_restoppred] = ...
          T_Q_driver_volatile_nonlin_bessel_fun(...
            conddip,...%conduit dip angle (deg)
            Rres,...%reservoir radius
            Rcondtop,... %conduit top radius
            Rcondbot,... %conduit bottom radius
            G,... %elastic wall rock shear modulus
            Rlake,... %lava lake radius
            Hcolumn,... %conduit depth (including lake)
            H2O_condtop_wtp,... %wtp
            H2O_condbot_wtp,... %wtp
            CO2_condtop_ppm,... %ppm
            CO2_condbot_ppm,... %ppm
            H2O_laketop_wtp,... %wtp
            H2O_lakebot_wtp,... %wtp
            CO2_laketop_ppm,... %ppm
            CO2_lakebot_ppm,... %ppm
            Hlake,...%lake depth
            TK_condbot,...%conduit bottom Temp (Kelvin)
            TK_condtop,...%conduit top Temp (Kelvin)
            TK_lakebot,...%lake bottom Temp (Kelvin)
            TK_laketop,...%lake top Temp (Kelvin)
            plotprofiles,...
            CO2solppm_table, H2Osolwtp_table,dz,TolX); 

        cost = [((T-Tpred)/T), ...
            ((Q-Qpred)/Q), ...
            10*((P_restop-P_restoppred)/P_restop)];

        if unstable
            cost = [inf,inf,inf];
        end
%     catch
%         cost = inf;
%     end
end


%% volatile (lake) costfun
function cost = volatile_lake_costfun(y, yscale, T, Q, P_restop,...
        conddip,...%conduit dip angle (deg)
        Rres,...%reservoir radius
        Rcondtop,... %conduit top radius
        Rcondbot,... %conduit bottom radius
        G,... %elastic wall rock shear modulus
        Rlake,... %lava lake radius
        Hcolumn,... %conduit depth (including lake)
        H2O_wtp_frac,...
        Hlake,...%lake depth
        plotprofiles,...
        CO2solppm_table, H2Osolwtp_table,dz,TolX)
    
    y = y.*yscale;
    TK = y(1) + 273.15;
    vol_cond_wtp = y(2);
    vol_lake_wtp = y(3);
    
    TK_condbot = TK;
    TK_condtop = TK;
    TK_lakebot = TK;
    TK_laketop = TK;
    
    vol_laketop_wtp = vol_lake_wtp;
    vol_lakebot_wtp = vol_cond_wtp;
    vol_condtop_wtp = vol_cond_wtp;
    vol_condbot_wtp = vol_cond_wtp;
    
    wtp_CO2_to_ppm = 1/(7.0778e-05); %approximate
    CO2_wtp_frac = 1-H2O_wtp_frac;
    
    H2O_condtop_wtp = vol_condtop_wtp*H2O_wtp_frac;
    H2O_condbot_wtp = vol_condbot_wtp*H2O_wtp_frac;
    CO2_condtop_ppm = vol_condtop_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;
    CO2_condbot_ppm = vol_condbot_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;
    H2O_laketop_wtp = vol_laketop_wtp*H2O_wtp_frac;
    H2O_lakebot_wtp = vol_lakebot_wtp*H2O_wtp_frac;
    CO2_laketop_ppm = vol_laketop_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;
    CO2_lakebot_ppm = vol_lakebot_wtp*CO2_wtp_frac*wtp_CO2_to_ppm;

    
%     try
        [Tpred,Qpred,~,~,~,unstable,~,P_restoppred] = ...
          T_Q_driver_volatile_nonlin_bessel_fun(...
            conddip,...%conduit dip angle (deg)
            Rres,...%reservoir radius
            Rcondtop,... %conduit top radius
            Rcondbot,... %conduit bottom radius
            G,... %elastic wall rock shear modulus
            Rlake,... %lava lake radius
            Hcolumn,... %conduit depth (including lake)
            H2O_condtop_wtp,... %wtp
            H2O_condbot_wtp,... %wtp
            CO2_condtop_ppm,... %ppm
            CO2_condbot_ppm,... %ppm
            H2O_laketop_wtp,... %wtp
            H2O_lakebot_wtp,... %wtp
            CO2_laketop_ppm,... %ppm
            CO2_lakebot_ppm,... %ppm
            Hlake,...%lake depth
            TK_condbot,...%conduit bottom Temp (Kelvin)
            TK_condtop,...%conduit top Temp (Kelvin)
            TK_lakebot,...%lake bottom Temp (Kelvin)
            TK_laketop,...%lake top Temp (Kelvin)
            plotprofiles,...
            CO2solppm_table, H2Osolwtp_table,dz,TolX); 

        cost = [((T-Tpred)/T), ...
            ((Q-Qpred)/Q), ...
            10*((P_restop-P_restoppred)/P_restop)];

        if unstable
            cost = [inf,inf,inf];
        end
%     catch
%         cost = inf;
%     end
end


%% modeled conduit reservoir mode
%{
keepind = TQ_tab.T > 34.8 & TQ_tab.T < 45 &...
    TQ_tab.amp_abovenoise > log10(3.0) & ...
    TQ_tab.std_abovenoise > log10(3.0) & ...
    TQ_tab.Q > 6 & ...
    TQ_tab.mean_min50_errphase < 0.1 &...
    TQ_tab.channelCount > 0 & ...
    (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2) & ...
    TQ_tab.start > datetime(2011,10,1);% & ...
    %TQ_tab.start > datetime(2014,7,23,20,14,0);
% keepind = TQ_tab.cr &...
%     TQ_tab.amp_abovenoise > log10(3.0) & ...
%     TQ_tab.std_abovenoise > log10(3.0) & ...
%     TQ_tab.Q > 6 & ...
%     TQ_tab.mean_min50_errphase < 0.1 &...
%     TQ_tab.channelCount > 5 & ...
%     (TQ_tab.Acwt - TQ_tab.maxamp) > log10(2);
VLP_tab_cr = TQ_tab(keepind,:);
VLP_tab_cr = sortrows(VLP_tab_cr,'start');


'only keeping 1 in 30 events'
VLP_tab_cr = VLP_tab_cr(1:30:end,:);


addpath('conduit_reservoir_oscillator/mains')
Zrescentroid = 1100-1940; %reservoir centroid elev (m asl) from kyle
Zlakebot = 700; %lake floor elev (m asl)
G = 3.08E9; %rock shear modulus (Pa)
conddip = 0;
Rres = 1250;
Rcondtop = 10; %conduit top radius (m)
Rcondbot = 10; %conduit bottom radius (m)
TK_vec = 273.15 + linspace(1000,1300,12); %temperature (K)

'shifting P_restop'
max_P_restop = max(VLP_tab_cr.lakeh - (Zrescentroid + Rres))*9.8*2200; %maximim possible pressure
in = VLP_tab_cr.start >= datetime(2014,7,23,20,14,0);
shift = max_P_restop - max(VLP_tab_cr.P_restop(in));
VLP_tab_cr.P_restop(in) = VLP_tab_cr.P_restop(in) + shift;

'different P_restop shift where disp change 2014'
max_P_restop = max(VLP_tab_cr.lakeh - (Zrescentroid + Rres))*9.8*2200; %maximim possible pressure
in = VLP_tab_cr.start < datetime(2014,7,23,20,14,0);
shift = max_P_restop - max(VLP_tab_cr.P_restop(in));
VLP_tab_cr.P_restop(in) = VLP_tab_cr.P_restop(in) + shift;

H2O_laketop = 0.5; %lake top H2O (wtp)
% H2O_lakebot = 0.5; %lake bottom H2O (wtp)
H2O_condtop = 0.5; %conduit top H2O (wtp)
H2O_condbot = 0.5; %conduit bottom H2O (wtp)

CO2_laketop = 1E4; %lake top CO2 (ppm)
% CO2_lakebot = 1E4; %lake bottom CO2 (ppm)
% CO2_condtop = 1E4; %conduit top CO2 (ppm)
% CO2_condbot = 1E4; %conduit bottom CO2 (ppm)


addpath('conduit_reservoir_oscillator')
addpath('conduit_reservoir_oscillator/source')
%http://calcul-isto.cnrs-orleans.fr/thermodynamics/applications/#/script/js_compute~H2O-CO2%20systems~H2O%20and%20CO2%20solubility.js
CO2solppm_table = readmatrix('new_Values_CO2solubility_ppm_comb.csv');
H2Osolwtp_table = readmatrix('new_Values_H2Osolubility_wtp_comb.csv');

CO2_condbot_pred = cell(height(VLP_tab_cr),1);
CO2_condtop_pred = cell(height(VLP_tab_cr),1);
TK_pred = cell(height(VLP_tab_cr),1);
cost_pred = cell(height(VLP_tab_cr),1);

numcores = feature('numcores'); 
poolobj = gcp('nocreate'); 
if isempty(poolobj)
    parpool(numcores)
end

'not using parfor'
for ii = 1:height(VLP_tab_cr)
    Rlake = sqrt(VLP_tab_cr.lakeA(ii)*10^4/pi);
%     if isnan(Rlake)
%         Rlake = sqrt(nanmax(VLP_tab_cr.lakeA)*10^4/pi);
%     end
    Hlake = VLP_tab_cr.lakeh(ii)-Zlakebot;
    if Hlake < 0
        Hlake = 0;
    end
    L_column = VLP_tab_cr.lakeh(ii) - (Zrescentroid + Rres);
    L_cond = L_column - Hlake;
    
    yscale = [1000,10000,10000];
    y0 = [1200+273.15, 2E3, 2E3]./yscale;
    ylower = [980+273.15, 1E1, 1E1]./yscale;
    yupper = [1600+273.15, 5E4, 5E4]./yscale;
    fminconoptions = optimoptions('fmincon','ConstraintTolerance',1e-4,...
        'MaxFunctionEvaluations',3000,...
        'MaxIterations',1000,...
        'OptimalityTolerance',1e-4,...
        'StepTolerance',1e-4,...
        'DiffMaxChange',0.1,...
        'TypicalX',[y0],...
        'Display','off',...
        'PlotFcn',[]);%{'optimplotx','optimplotfval'});
    [y,cost,exitflag,fminconoutputstruct] = fmincon(@(y) costfun(y,yscale,...
        VLP_tab_cr.T(ii),...
        VLP_tab_cr.Q(ii),...
        VLP_tab_cr.P_restop(ii),...
        Rres,...%reservoir radius
        Rcondtop,... %conduit top radius
        Rcondbot,...
        G,... %elastic wall rock shear modulus
        Rlake,... %lava lake radius
        L_column,... %conduit length
        H2O_condtop,... %wtp
        H2O_condbot,... %wtp
        H2O_laketop,... %wtp
        CO2_laketop,... %ppm
        Hlake,... %lake depth
        CO2solppm_table, H2Osolwtp_table),y0,[],[],[],[],...
        ylower,yupper,[],fminconoptions);
    cost_pred{ii} = cost;
    y = y.*yscale;
    TK_pred{ii} = y(1);
    CO2_condbot_pred{ii} = y(2);
    CO2_condtop_pred{ii} = y(3);
    [num2str(ii),'_  _',datestr(VLP_tab_cr.start(ii)),'_  _',num2str(cost)]
end

VLP_tab_cr.cost_pred = cell2mat(cost_pred);
VLP_tab_cr.TK_pred = cell2mat(TK_pred);
VLP_tab_cr.CO2_condbot_pred = cell2mat(CO2_condbot_pred);
VLP_tab_cr.CO2_condtop_pred = cell2mat(CO2_condtop_pred);

FileName = 'VLP_tab_cr_H2Opred.mat';
FileName = avoidOverwrite(FileName,pwd,2,0);
save(FileName, 'VLP_tab_cr','-v7.3')
%}
%% plots
%{
figure()
Tlim = [34.8,43];
Xlim = [datetime(2011,10,1),datetime(2018,7,1)];

ax1 = subplot(4,1,1);
%fill(inflateplotx,inflateploty*100,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
scatter(VLP_tab_cr.start,VLP_tab_cr.T,9,VLP_tab_cr.Q,'filled')
grid on, grid minor
ylabel('T (s)')
cbar = colorbar;
cbar.Position = cbar.Position + [0.09,0,0,0];
cbar.Position(3) = 0.01;
colormap(ax1,cool)
caxis([6,40])
ylabel(cbar, 'Q')
xlim(Xlim);%max(VLP_tab.start)])
ylim(Tlim)
xline(datetime(2008,3,19),'k',{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k',{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k',{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k',{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k',{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k',{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])

ax2 = subplot(4,1,2);
%fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
p1 = scatter(VLP_tab_cr.start,VLP_tab_cr.TK_pred,9,VLP_tab_cr.cost_pred,'filled');
hold on
grid on, grid minor
ylabel('T (K)')
xlim(Xlim)
xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
cbar = colorbar;
cbar.Position = cbar.Position + [0.09,0,0,0];
cbar.Position(3) = 0.01;
ylabel(cbar,'cost')
colormap(ax2,'jet')

ax3 = subplot(4,1,3);
%fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
p1 = scatter(VLP_tab_cr.start,VLP_tab_cr.CO2_condbot_pred,9,VLP_tab_cr.cost_pred,'s');
hold on
p2 = scatter(VLP_tab_cr.start,VLP_tab_cr.CO2_condtop_pred,9,VLP_tab_cr.cost_pred,'filled');
grid on, grid minor
ylabel('CO2 (ppm)')
xlim(Xlim)
xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
cbar = colorbar;
cbar.Position = cbar.Position + [0.09,0,0,0];
cbar.Position(3) = 0.01;
ylabel(cbar,'cost')
colormap(ax3,'jet')

ax4 = subplot(4,1,4);
%fill(inflateplotx,inflateploty*3000,[0.8,0.8,0.8],'EdgeColor','none')
%hold on
yyaxis('left')
p1 = scatter(VLP_tab_cr.start,VLP_tab_cr.lakeh,'b.');
ylabel('lava-lake el. (m)')
hold on
grid on, grid minor
yyaxis('right')
p2 = scatter(VLP_tab_cr.start,VLP_tab_cr.P_restop,'r.');
ylabel('conduit base pressure (Pa)')
xlim(Xlim)
xline(datetime(2008,3,19),'k')%,{'Crater'},'LabelVerticalAlignment','bottom')
xline(datetime(2010,2,1),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,5,25),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,10,15),'k')%,{'SSE'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,3,5),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,8,3),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2011,9,21),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,6,27),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2016,5,24),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2018,4,30),'k')%,{'ERZ'},'LabelVerticalAlignment','bottom')
xline(datetime(2012,10,26),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2014,5,10),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
xline(datetime(2015,5,9),'k')%,{'Int'},'LabelVerticalAlignment','bottom')
set(gca,'Layer','top')
xticklabels([])
%}