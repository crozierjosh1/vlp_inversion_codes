%"jacobian" grid of how parameters effect T and Q for both
%conduit-reservoir and lake sloshing
%josh crozier 2020

addpath(genpath('mains'))
addpath(genpath('source'))

N = 99; %number of variations of each param
lograt = 3500; %parameter range ratio at which to use log scale
Tmin = 0;
Tmax = 80;
Qmin = 0;
Qmax = 80;
lambdamin = 1E-7;
lambdamax = 1E0;
dz = 0.4; %depth increment for integration
TolX = 1e-9;
H2O_wtp_frac = 0.5;
wtp_CO2_to_ppm = 1/(7.0778e-05); %approximate
CO2_wtp_frac = 1-H2O_wtp_frac;

fixed_laketop = true;
fixed_vol_laketop = 1.8; %wtp


%% fixed parameters
tab = table('Size',[1,6],...
    'VariableTypes',{'string','string','double','logical','double','double'},...
    'VariableNames',{'name','text','default','vary','min','max'});

tab(1,:) = {'Zrescentroid', '', 1100-1940, false, NaN, NaN,}; %reservoir centroid elev (m asl)
tab(end+1,:) = {'Zlakebot', '', 700, false, NaN, NaN}; %lake floor elev (m asl)
tab(end+1,:) = {'G', '', 3.08E9, false, NaN, NaN}; %rock shear modulus (Pa)
tab(end+1,:) = {'theta_c', '', 0, false, NaN, NaN}; %angle of conduit from lake center
% tab(end+1,:) = {'r_c', 'conduit offset (m)', 50, false, 0, 90}; %conduit top radius (m)

tab(end+1,:) = {'Rres', 'reservoir radius (m)', 1250, false, 1000, 1550}; %reservoir radius (m)

tab(end+1,:) = {'Rcondtop', 'conduit top radius (m)', 10, false, 1, 30}; %conduit top radius (m)
tab(end+1,:) = {'Rcondbot', 'conduit bottom radius (m)', 10, false, 1, 30}; %conduit top radius (m)
tab(end+1,:) = {'conddip', 'conduit dip (deg)', 90, false, 45, 90}; %conduit dip (might not work)


%% varied parameters
tab(end+1,:) = {'TC', 'a. magma temperature (C)', 1200, true, 900, 1400}; %lake Temp (C)
tab(end+1,:) = {'Zlaketop', 'd. lava lake top el. (m ASL)', 950, true, 750, 1050}; %lake surface elev (m asl)
tab(end+1,:) = {'vol_avg', 'b. conduit average volatiles X^{avg} (wt%)', 3.0, true, 0, 6}; %lake radius (m)
tab(end+1,:) = {'Rlake', 'e. lava lake radius (m)', 100, true, 10, 140}; %lake radius (m)
tab(end+1,:) = {'vol_diff', 'c. conduit volatile difference (top-bottom) \DeltaX (wt%)', 1, true, -2, 4}; %lake radius (m)


%% precalculated parameters
%http://calcul-isto.cnrs-orleans.fr/thermodynamics/applications/#/script/js_compute~H2O-CO2%20systems~H2O%20and%20CO2%20solubility.js
CO2solppm_table = readmatrix('new_Values_CO2solubility_ppm_comb.csv');
H2Osolwtp_table = readmatrix('new_Values_H2Osolubility_wtp_comb.csv');


%% loop through parameters
numplots = sum(tab.vary);
numplotcols = 2;%ceil(sqrt(numplots));
numplotrows = ceil(numplots/numplotcols);
figure()
%tiledlayout(numplotrows,numplotcols,'TileSpacing','compact','Padding','compact')
plotcount = 0;
for ii = 1:height(tab)
    if tab.vary(ii)
        plotcount = plotcount + 1;
        %structure of default param values
        p = struct();
        for jj = 1:height(tab)
            p.(tab.name(jj)) = tab.default(jj);
        end

        %loop over param values
        Tvec = NaN(1,N);
        Qvec = NaN(1,N);
        Pvec = NaN(1,N);
        if tab.max(ii)/tab.min(ii) > lograt && sign(tab.max(ii)) == sign(tab.min(ii))
            logplot = true;
            pvec = logspace(log10(tab.min(ii)), log10(tab.max(ii)), N);
        else
            logplot = false;
            pvec = linspace(tab.min(ii), tab.max(ii), N);
        end
        
        if strcmp(tab.name(ii),'Rres')
            pvec_alt = NaN*pvec;
        end
        
        for jj = 1:N
            %change param value
            p.(tab.name(ii)) = pvec(jj);
            
            %calculate dependent parameters
            p.Hcolumn = p.Zlaketop - (p.Zrescentroid + p.Rres);
            p.Hlake = p.Zlaketop - p.Zlakebot;
            p.Lcond = p.Hcolumn - p.Hlake;
            p.TK_condbot = p.TC + 273.15;
            p.TK_condtop = p.TC + 273.15;
            p.TK_laketop = p.TC + 273.15;
            
            vol_condtop = p.vol_avg + p.vol_diff/2;
            vol_condbot = p.vol_avg - p.vol_diff/2;
            vol_lakebot = vol_condtop;
            if fixed_laketop
                vol_laketop = fixed_vol_laketop;
            else
                vol_laketop = vol_condtop;
            end
            
            p.H2O_condtop_wtp = vol_condtop*H2O_wtp_frac;
            p.H2O_condbot_wtp = vol_condbot*H2O_wtp_frac;
            p.CO2_condtop_ppm = vol_condtop*CO2_wtp_frac*wtp_CO2_to_ppm;
            p.CO2_condbot_ppm = vol_condbot*CO2_wtp_frac*wtp_CO2_to_ppm;
            p.H2O_laketop_wtp = vol_laketop*H2O_wtp_frac;
            p.H2O_lakebot_wtp = vol_lakebot*H2O_wtp_frac;
            p.CO2_laketop_ppm = vol_laketop*CO2_wtp_frac*wtp_CO2_to_ppm;
            p.CO2_lakebot_ppm = vol_lakebot*CO2_wtp_frac*wtp_CO2_to_ppm;
            
            if strcmp(tab.name(ii),'Rres')
                pvec_alt(jj) = (p.Zrescentroid + p.Rres);
            end

            %try
                [T,Q,~,~,~,unstable,phimax,P_restop] = ...
                    T_Q_driver_volatile_nonlin_bessel_fun(...
                    p.conddip,...%conduit dip angle
                    p.Rres,...%reservoir radius
                    p.Rcondtop,... %conduit top radius
                    p.Rcondbot,... %conduit bottom radius
                    p.G,... %elastic wall rock shear modulus
                    p.Rlake,... %lava lake radius
                    p.Hcolumn,... %magma column height (including lake)
                    p.H2O_condtop_wtp,... %wtp
                    p.H2O_condbot_wtp,... %wtp
                    p.CO2_condtop_ppm,... %ppm
                    p.CO2_condbot_ppm,... %ppm
                    p.H2O_laketop_wtp,... %wtp
                    p.H2O_condtop_wtp,... %wtp
                    p.CO2_laketop_ppm,... %ppm
                    p.CO2_condtop_ppm,... %ppm
                    p.Hlake,... %lake height
                    p.TK_condbot,... %conduit bottom temp
                    p.TK_condtop,...%conduit top temp
                    p.TK_condtop,...%lake bottom temp
                    p.TK_laketop,... %lake top temp
                    false,...
                    CO2solppm_table, H2Osolwtp_table, dz,TolX); 
                if vol_condbot < 0 || vol_condtop < 0
                    unstable = true;
                end
                if ~unstable
                    Tvec(jj) = T;
                    Qvec(jj) = Q;
                    Pvec(jj) = P_restop;
                end
            %catch
            %    'error' 
            %    jj
            %end
        end
        %plot
%         ax = nexttile;
        ax = subplot(3,2,plotcount);
        if strcmp(tab.name(ii),'Rres')
            pvec = pvec_alt;
            tab.text(ii) = 'conduit bottom el. (m ASL)';
        end
        %
%         yyaxis('left')
        p1 = plot(pvec,Tvec,'b-','Linewidth',1);
        hold on
        p4 = plot(pvec,Pvec*1e-5,':','Linewidth',1.5,'Color',[0.4,0,0.6]);
        if strcmp(tab.name(ii),'Rres')
            p3 = xline(p.Zrescentroid + tab.default(ii),'k--','Linewidth',2);
        else
            p3 = xline(tab.default(ii),'k--','Linewidth',2);
        end
        grid on
        grid minor
%         ylabel('T (s)')
        ylim([Tmin,Tmax])
        xlim([min(pvec),max(pvec)])
        xlabel(tab.text(ii))
%         yyaxis('right')
        p2 = plot(pvec,Qvec,'r-','Linewidth',1);
%         ylabel('Q')
%         ylim([Qmin,Qmax])
        if logplot
            set(gca,'Xscale','Log')
        end
        drawnow
        set(gca,'FontSize',12)
        if ii == height(tab)
            legend([p1,p2,p4,p3],{'resonance period T (s)','resonance quality factor Q','conduit bottom (reservoir top) pressure (\times10^5 Pa)','default value'})
        end
        yticks(0:20:80)
        
        Position = ax.Position;
        Position = [Position(1)-0.04,Position(2),0.42,0.2];
        ax.Position = Position;
        %}
    end
end
t = annotation('textbox',[0.005,0.005,0.49,0.99],'String','free parameters');
t.FontSize = 12;
t.FontWeight = 'bold';
t.HorizontalAlignment = 'center';
t = annotation('textbox',[0.505,0.305,0.49,0.69],'String','prescribed time-varying parameters');
t.FontSize = 12;
t.FontWeight = 'bold';
t.HorizontalAlignment = 'center';

