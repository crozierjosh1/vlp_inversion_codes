function output_struct = magma_prop_from_volatiles_nodepth(H2O_wtp,CO2_ppm,TK,P_fixed)


%% parameters
CO2_noexsol = false; %no exsolution of CO2 in compressibility calc 
%(due to low diffusivity), doesn't seem to matter in practice
Rc = 8.314;%ideal gas const
g = 9.81;
% Ptop = linspace(1,100,100)*1E5;%top reservoir pressure
%linear total H2O-CO2 between top and bottom values

deltaP = 1E5;%pressure step for compressibility

%% solubility tables:
if ~exist('CO2solppm_table','var')
    %http://calcul-isto.cnrs-orleans.fr/thermodynamics/applications/#/script/js_compute~H2O-CO2%20systems~H2O%20and%20CO2%20solubility.js
    CO2solppm_table = readmatrix('new_Values_CO2solubility_ppm_comb.csv');
    H2Osolwtp_table = readmatrix('new_Values_H2Osolubility_wtp_comb.csv');
end
P_table = CO2solppm_table(2:end,1)*1E5;
H2O_gas_molfrac_table = CO2solppm_table(1,2:end);
delta_H2O_gas_molfrac_table = (H2O_gas_molfrac_table(2)-H2O_gas_molfrac_table(1));
CO2solppm_table = CO2solppm_table(2:end,2:end);
H2Osolwtp_table = H2Osolwtp_table(2:end,2:end);

%% composition
wtp_sum_nonH2O = 50.6+2.3+12.9+10.56+0.87+0.19+8.95+10.6+2.19+0.38+0.23+0;
wtp_scale = (100)/wtp_sum_nonH2O; %adjust so that % adds to 100
wtp_SiO2 = wtp_scale*50.6;
wtp_TiO2 = wtp_scale*2.3;
wtp_Al2O3 = wtp_scale*12.9;
wtp_FeO = wtp_scale*10.56; 
wtp_Fe2O3 = wtp_scale*0.87;
% wtp_FeO_Total = wtp_FeO + wtp_Fe2O3;
wtp_MnO = wtp_scale*0.19;
wtp_MgO = wtp_scale*8.95;
wtp_CaO = wtp_scale*10.6;
wtp_Na2O = wtp_scale*2.19;
wtp_K2O = wtp_scale*0.38;
wtp_P2O5 = wtp_scale*0.23;
wtp_F2O_1 = wtp_scale*0; %assuming

%% molar weights (kg/mol)
M_SiO2 = 60.09/1000;
M_TiO2 = 79.88/1000;
M_Al2O3 = 101.96/1000;
M_Fe2O3 = 159.7/1000;
M_FeO = 71.85/1000;
M_MnO = 70.94/1000;
M_MgO = 40.31/1000;
M_CaO = 56.08/1000;
M_Na2O = 61.98/1000;
M_K2O = 94.2/1000;
M_P2O5 = 141.94/1000;
M_H2O = 18.02/1000;
M_F2O_1 = 54.0/1000;
M_CO2 = 44.01/1000;

%% mol/kg
mol_kg_SiO2 = wtp_SiO2/100/(M_SiO2);
mol_kg_TiO2 = wtp_TiO2/100/(M_TiO2);
mol_kg_Al2O3 = wtp_Al2O3/100/(M_Al2O3);
mol_kg_Fe2O3 = wtp_Fe2O3/100/(M_Fe2O3);
mol_kg_FeO = wtp_FeO/100/(M_FeO);
%mol_kg_FeO_Total = mol_kg_Fe2O3 + mol_kg_FeO;
mol_kg_MnO = wtp_MnO/100/(M_MnO);
mol_kg_MgO = wtp_MgO/100/(M_MgO);
mol_kg_CaO = wtp_CaO/100/(M_CaO);
mol_kg_Na2O = wtp_Na2O/100/(M_Na2O);
mol_kg_K2O = wtp_K2O/100/(M_K2O);
mol_kg_P2O5 = wtp_P2O5/100/(M_P2O5);
% mol_kg_H2O = wtp_H2O/100/(M_H2O/1000);
mol_kg_F2O_1 = wtp_F2O_1/100/(M_F2O_1);

mol_kg_total_dry = mol_kg_SiO2 + mol_kg_TiO2 + mol_kg_Al2O3 + mol_kg_FeO + mol_kg_Fe2O3 +...
    +mol_kg_MnO + mol_kg_MgO + mol_kg_CaO + mol_kg_Na2O + mol_kg_K2O + mol_kg_P2O5;

%% convert ppm to wt%
% H2Osolwtp = H2Osolppm/1E6*M_H2O*mol_kg_total*100;
CO2solwtp_table = CO2solppm_table/1E6*M_CO2*mol_kg_total_dry*100;

CO2_wtp = CO2_ppm/1E6*M_CO2*mol_kg_total_dry*100;

    
%% find wtp exsolved gas at each pressure
%mol gas/kg melt
%     H2Osolwtp_temp = interp2(H2O_gas_molfrac_table, P_table, ...
%         H2Osolwtp_table, H2O_gas_molfrac_table, Pvec(i));
%     CO2solwtp_temp = interp2(H2O_gas_molfrac_table, P_table, ...
%         CO2solwtp_table, H2O_gas_molfrac_table, Pvec(i));
Pin_high = find(P_table>P_fixed, 1);
if Pin_high == 1
    Pin_high = 2;
elseif isempty(Pin_high)
    error('no pressure found in table')
end
stepfrac = (P_fixed-P_table(Pin_high-1))/(P_table(Pin_high)-P_table(Pin_high-1));
H2Osolwtp_temp = H2Osolwtp_table(Pin_high-1,:) + ...
    stepfrac*(H2Osolwtp_table(Pin_high,:)-H2Osolwtp_table(Pin_high-1,:));
CO2solwtp_temp = CO2solwtp_table(Pin_high-1,:) + ...
    stepfrac*(CO2solwtp_table(Pin_high,:)-CO2solwtp_table(Pin_high-1,:));      
temp_H2O_gas_mol = (H2O_wtp-H2Osolwtp_temp)/M_H2O;
temp_CO2_gas_mol = (CO2_wtp-CO2solwtp_temp)/M_CO2;
some_H2O_gas = temp_H2O_gas_mol > 0;
some_CO2_gas = temp_CO2_gas_mol > 0;
temp_H2O_gas_molfrac = NaN(size(temp_H2O_gas_mol));
temp_H2O_gas_molfrac(some_H2O_gas & some_CO2_gas)...
    = temp_H2O_gas_mol(some_H2O_gas & some_CO2_gas)...
    ./(temp_CO2_gas_mol(some_H2O_gas & some_CO2_gas)...
    + temp_H2O_gas_mol(some_H2O_gas & some_CO2_gas));
temp_H2O_gas_molfrac(some_H2O_gas & ~some_CO2_gas) = 1;
temp_H2O_gas_molfrac(~some_H2O_gas & some_CO2_gas) = 0;

%find first H2O gas ratio bigger than actual
bigind = find(temp_H2O_gas_molfrac >= H2O_gas_molfrac_table & ~isnan(temp_H2O_gas_molfrac),1);
%find first H2O gas ratio smaller than actual
smallind = find(temp_H2O_gas_molfrac <= H2O_gas_molfrac_table & ~isnan(temp_H2O_gas_molfrac),1);
%take whichever index appropriate
if isempty(bigind) && isempty(smallind)
    ind = NaN; %no valid points
elseif isempty(bigind)
    if smallind ~= 1
        ind = NaN;
    end
elseif isempty(smallind)
    if bigind ~= length(temp_H2O_gas_molfrac)
        ind = NaN;
    end
else
    ind = max([bigind,smallind]);
    if ind == 1
        ind = 2;%so don't have to add more cases to interpolation below
    end
end


if ~isnan(ind)
    if ~isnan(temp_H2O_gas_molfrac(ind-1)) && ~isnan(temp_H2O_gas_molfrac(ind))
        %linear interp to optimal gasmolfrac
        target_H2O_gas_molfrac = H2O_gas_molfrac_table(ind-1)...
            + (temp_H2O_gas_molfrac(ind-1) - H2O_gas_molfrac_table(ind-1))...
            /(1 - (temp_H2O_gas_molfrac(ind)-temp_H2O_gas_molfrac(ind-1))...
            /delta_H2O_gas_molfrac_table);
        %how far to step from undershoot value to overshoot value
        molfrac_step = (target_H2O_gas_molfrac - H2O_gas_molfrac_table(ind-1));
        %linear interp exsolved gasses at target gasmolfrac
        stepfrac = molfrac_step/delta_H2O_gas_molfrac_table;
        if stepfrac > 1
            %'error'
            stepfrac = 1;
        elseif stepfrac < 0
            %'error'
            stepfrac = 0;
        end
        H2O_gas = H2O_wtp - (H2Osolwtp_temp(ind-1) + stepfrac...
            *(H2Osolwtp_temp(ind)-H2Osolwtp_temp(ind-1)));
        CO2_gas = CO2_wtp - (CO2solwtp_temp(ind-1) + stepfrac...
            *(CO2solwtp_temp(ind)-CO2solwtp_temp(ind-1)));                           
    else
        %just use overshoot value
        H2O_gas = H2O_wtp - H2Osolwtp_temp(ind);
        CO2_gas = CO2_wtp - CO2solwtp_temp(ind);
    end
    %seems to be neccessary due to interp inaccuracy
    if H2O_gas < 0
        H2O_gas = 0;
    end
    if CO2_gas < 0
        CO2_gas = 0;
    end
else
    H2O_gas = 0;
    CO2_gas = 0;
end    
%necessary due to floating point or interp error
if H2O_gas > H2O_wtp
    H2O_gas = H2O_wtp;
end
if CO2_gas > CO2_wtp
    CO2_gas = CO2_wtp;
end


%% find wtp exsolved gas at each [pressure + delta P]  
%mol gas/kg melt
%     H2Osolwtp_temp = interp2(H2O_gas_molfrac_table, P_table, ...
%         H2Osolwtp_table, H2O_gas_molfrac_table, Pvec(i)+deltaP);
%     CO2solwtp_temp = interp2(H2O_gas_molfrac_table, P_table, ...
%         CO2solwtp_table, H2O_gas_molfrac_table, Pvec(i)+deltaP);
Pin_high = find(P_table>P_fixed+deltaP, 1);
if Pin_high == 1
    Pin_high = 2;
elseif isempty(Pin_high)
    error('no pressure found in table')
end
stepfrac = (P_fixed+deltaP-P_table(Pin_high-1))/(P_table(Pin_high)-P_table(Pin_high-1));
H2Osolwtp_temp = H2Osolwtp_table(Pin_high-1,:) + ...
    stepfrac*(H2Osolwtp_table(Pin_high,:)-H2Osolwtp_table(Pin_high-1,:));
CO2solwtp_temp = CO2solwtp_table(Pin_high-1,:) + ...
    stepfrac*(CO2solwtp_table(Pin_high,:)-CO2solwtp_table(Pin_high-1,:)); 
temp_H2O_gas_mol = (H2O_wtp-H2Osolwtp_temp)/M_H2O;
temp_CO2_gas_mol = (CO2_wtp-CO2solwtp_temp)/M_CO2;
some_H2O_gas = temp_H2O_gas_mol > 0;
some_CO2_gas = temp_CO2_gas_mol > 0;
temp_H2O_gas_molfrac = NaN(size(temp_H2O_gas_mol));
temp_H2O_gas_molfrac(some_H2O_gas & some_CO2_gas)...
    = temp_H2O_gas_mol(some_H2O_gas & some_CO2_gas)...
    ./(temp_CO2_gas_mol(some_H2O_gas & some_CO2_gas)...
    + temp_H2O_gas_mol(some_H2O_gas & some_CO2_gas));
temp_H2O_gas_molfrac(some_H2O_gas & ~some_CO2_gas) = 1;
temp_H2O_gas_molfrac(~some_H2O_gas & some_CO2_gas) = 0;

%find first H2O gas ratio bigger than actual
bigind = find(temp_H2O_gas_molfrac >= H2O_gas_molfrac_table & ~isnan(temp_H2O_gas_molfrac),1);
%find first H2O gas ratio smaller than actual
smallind = find(temp_H2O_gas_molfrac <= H2O_gas_molfrac_table & ~isnan(temp_H2O_gas_molfrac),1);
%take whichever index appropriate
if isempty(bigind) && isempty(smallind)
    ind = NaN; %no valid points
elseif isempty(bigind)
    if smallind ~= 1
        ind = NaN;
    end
elseif isempty(smallind)
    if bigind ~= length(temp_H2O_gas_molfrac)
        ind = NaN;
    end
else
    ind = max([bigind,smallind]);
    if ind == 1
        ind = 2;%so don't have to add more cases to interpolation below
    end
end


if ~isnan(ind)
    if ~isnan(temp_H2O_gas_molfrac(ind-1)) && ~isnan(temp_H2O_gas_molfrac(ind))
        %linear interp to optimal gasmolfrac
        target_H2O_gas_molfrac = H2O_gas_molfrac_table(ind-1)...
            + (temp_H2O_gas_molfrac(ind-1) - H2O_gas_molfrac_table(ind-1))...
            /(1 - (temp_H2O_gas_molfrac(ind)-temp_H2O_gas_molfrac(ind-1))...
            /delta_H2O_gas_molfrac_table);
        %how far to step from undershoot value to overshoot value
        molfrac_step = (target_H2O_gas_molfrac - H2O_gas_molfrac_table(ind-1));
        %linear interp exsolved gasses at target gasmolfrac
        stepfrac = molfrac_step/delta_H2O_gas_molfrac_table;
        if stepfrac > 1
            %'error'
            stepfrac = 1;
        elseif stepfrac < 0
            %'error'
            stepfrac = 0;
        end
        H2O_gas_p = H2O_wtp - (H2Osolwtp_temp(ind-1) + stepfrac...
            *(H2Osolwtp_temp(ind)-H2Osolwtp_temp(ind-1)));
        CO2_gas_p = CO2_wtp - (CO2solwtp_temp(ind-1) + stepfrac...
            *(CO2solwtp_temp(ind)-CO2solwtp_temp(ind-1)));                           
    else
        %just use overshoot value
        H2O_gas_p = H2O_wtp - H2Osolwtp_temp(ind);
        CO2_gas_p = CO2_wtp - CO2solwtp_temp(ind);
    end
    %seems to be neccessary due to interp inaccuracy
    if H2O_gas_p < 0
        H2O_gas_p = 0;
    end
    if CO2_gas_p < 0
        CO2_gas_p = 0;
    end
else
    H2O_gas_p = 0;
    CO2_gas_p = 0;
end  
%necessary due to floating point or interp error
if H2O_gas_p > H2O_wtp
    H2O_gas_p = H2O_wtp;
end
if CO2_gas_p > CO2_wtp
    CO2_gas_p = CO2_wtp;
end


%% melt density
wtp_H2O_dis = H2O_wtp - H2O_gas;
mol_kg_H2O = wtp_H2O_dis/100/(M_H2O);
%mol% (needed for melt visc model)
mol_total = mol_kg_SiO2 + mol_kg_TiO2 + mol_kg_Al2O3 + mol_kg_FeO + mol_kg_Fe2O3 +...
    +mol_kg_MnO + mol_kg_MgO + mol_kg_CaO + mol_kg_Na2O + mol_kg_K2O + mol_kg_P2O5 + mol_kg_H2O;
SiO2 = 100*mol_kg_SiO2/mol_total;
TiO2 = 100*mol_kg_TiO2/mol_total;
Al2O3 = 100*mol_kg_Al2O3/mol_total;
Fe2O3 = 100*mol_kg_Fe2O3/mol_total;
FeO = 100*mol_kg_FeO/mol_total;
FeO_Total = Fe2O3 + FeO;
MnO = 100*mol_kg_MnO/mol_total;
MgO = 100*mol_kg_MgO/mol_total;
CaO = 100*mol_kg_CaO/mol_total;
Na2O = 100*mol_kg_Na2O/mol_total;
K2O = 100*mol_kg_K2O/mol_total;
P2O5 = 100*mol_kg_P2O5/mol_total;
H2Omol = 100*mol_kg_H2O/mol_total;
F2O_1 = 100*mol_kg_F2O_1/mol_total;

%%%%melt density model (Lange and Carmichael 1987)
dVdP_SiO2 = -1.73E-15;
dVdP_TiO2 = -2.20E-15;
dVdP_Al2O3 = -1.82E-15;
dVdP_FeO = -0.54E-15;
dVdP_MgO = 0.20E-15;
dVdP_CaO = 0.08E-15;
dVdP_Na2O = -2.59E-15;
dVdP_K2O = -8.79E-15;
dVdP_Fe2O3 = 0; %assuming
dVdP_MnO = 0; %assuming
dVdP_P2O5 = 0;%assuming
dVdP_H20 = -3.15E-15; %Ochs and Lange 1999

dVdT_SiO2 = 0.00E-9;
dVdT_TiO2 = 7.24E-9;
dVdT_Al2O3 = 2.62E-9;
dVdT_Fe2O3 = 9.09E-9;
dVdT_FeO = 2.92E-9;
dVdT_MgO = 2.62E-9;
dVdT_CaO = 2.92E-9;
dVdT_Na2O = 7.41E-9;
dVdT_K2O = 5.25E-9;
dVdT_MnO = 0; %assuming
dVdT_P2O5 = (69-88E-6)*10^-9;%from Knoche et al 1995
dVdT_H20 = 9.46E-9; %Ochs and Lange 1999

%molar volume (m^3/mol)
V_SiO2 = 26.92E-6;
V_TiO2 = 23.89E-6;
V_Al2O3 = 37.37E-6;
V_Fe2O3 = 42.97E-6;
V_FeO = 13.97E-6;
V_MgO = 11.73E-6;
V_CaO = 16.85E-6;
V_Na2O = 29.51E-6;
V_K2O = 47.07E-6;
V_excess_Na2O_TiO2 = 20.21E-6;
V_MnO = 13.97E-6; %assuming similar to FeO
V_P2O5 = 69E-6;%from Knoche et al 1995
V_H20 = 22.89E-6 + 500*dVdT_H20;%Ochs and Lange 1999, corrected to 1773 K

%melt volume/mol at 1773 K and 10^5 Pa
V_melt_1773_10_5 = 1/100*(SiO2*V_SiO2 + TiO2*V_TiO2 + Al2O3*V_Al2O3 + FeO*V_FeO +...
    Fe2O3*V_Fe2O3 + MnO*V_MnO + MgO*V_MgO + CaO*V_CaO + Na2O*V_Na2O +...
    K2O*V_K2O + P2O5*V_P2O5 + H2Omol*V_H20 + Na2O*TiO2/100*V_excess_Na2O_TiO2);

dV_melt_dT = 1/100*...
    (SiO2*dVdT_SiO2 + TiO2*dVdT_TiO2 + Al2O3*dVdT_Al2O3 + FeO*dVdT_FeO +...
    Fe2O3*dVdT_Fe2O3 + MnO*dVdT_MnO + MgO*dVdT_MgO + CaO*dVdT_CaO + Na2O*dVdT_Na2O +...
    K2O*dVdT_K2O + P2O5*dVdT_P2O5 + H2Omol*dVdT_H20);

dV_melt_dP = 1/100*...
    (SiO2*dVdP_SiO2 + TiO2*dVdP_TiO2 + Al2O3*dVdP_Al2O3 + FeO*dVdP_FeO +...
    Fe2O3*dVdP_Fe2O3 + MnO*dVdP_MnO + MgO*dVdP_MgO + CaO*dVdP_CaO + Na2O*dVdP_Na2O +...
    K2O*dVdP_K2O + P2O5*dVdP_P2O5 + H2Omol*dVdP_H20);

%neglecting d^2V/dPDT
V_melt = V_melt_1773_10_5 + (TK - 1773)*dV_melt_dT + (P_fixed - 10^5)*dV_melt_dP;

%total kg/mol melt
melt_kg_mol = 1/100*(SiO2*M_SiO2 + TiO2*M_TiO2 + Al2O3*M_Al2O3 + FeO*M_FeO +...
    Fe2O3*M_Fe2O3 + MnO*M_MnO + MgO*M_MgO + CaO*M_CaO + Na2O*M_Na2O +...
    K2O*M_K2O + P2O5*M_P2O5 + H2Omol*M_H2O);

melt_density = melt_kg_mol/V_melt; 
dmeltdensity_dP = -melt_kg_mol/V_melt^2*dV_melt_dP; %from drho/dp = d(M/V)/dP 


%% find porosity and magma density at each pressure
wtp_H2O_dis = H2O_wtp - H2O_gas;
mol_kg_H2O_vec = wtp_H2O_dis/100/(M_H2O);
mol_kg_total = mol_kg_SiO2 + mol_kg_TiO2 + mol_kg_Al2O3 + mol_kg_FeO + mol_kg_Fe2O3 +...
    mol_kg_MnO + mol_kg_MgO + mol_kg_CaO + mol_kg_Na2O + mol_kg_K2O + mol_kg_P2O5 + mol_kg_H2O_vec;
gas_massfrac = (H2O_gas + CO2_gas)/100;
H2O_gas_mol = H2O_gas/(M_H2O*mol_kg_total*100);
CO2_gas_mol = CO2_gas/(M_CO2*mol_kg_total*100);  
H2O_gas_molfrac = H2O_gas_mol/(H2O_gas_mol + CO2_gas_mol);
%ideal gas law
gas_vol = (H2O_gas_mol + CO2_gas_mol)*Rc*TK/P_fixed;
melt_vol = (1-gas_massfrac)/melt_density;
porosity = gas_vol/(gas_vol + melt_vol);
density = 1/(gas_vol + melt_vol);
%                 gas_density_vec = gas_massfrac_vec./gas_vol_vec;
gas_density = (P_fixed*(M_H2O*H2O_gas_molfrac...
    + M_CO2*(1 - H2O_gas_molfrac)))/(Rc*TK);
%                 %just to prevent divide-by-0 errors in compressibility calc
if isnan(gas_density)
    gas_density = P_fixed*M_CO2/(Rc*TK);   
end

%exsolution at equilibrium solubility
dwtp_H2O_gas_dP = (H2O_gas_p - H2O_gas)/deltaP;
if CO2_noexsol
    %!!!!approximate, in reality H2O also effected if no
    %CO2 exsolution, and also wouldn't be zero just less
    %than equilibrium!!!!
    dwtp_CO2_gas_dP = 0;
else
    dwtp_CO2_gas_dP = (CO2_gas_p - CO2_gas)/deltaP;
end
dgasmassfrac_dP = (dwtp_H2O_gas_dP + dwtp_CO2_gas_dP)/100;
dgasdensity_dP = (H2O_gas_molfrac*M_H2O ...
    + (1 - H2O_gas_molfrac)*M_CO2)/(Rc*TK); %ideal gas law
%just to prevent divide-by-0 errors in compressibility calc
if isnan(dgasdensity_dP)
    dgasdensity_dP = M_CO2/(Rc*TK);
end
%general compressibility (derivative of eq in Kieffer 1977)
%                 ddensity_dP_vec = (melt_density*(gas_density_vec.*dgasmassfrac_dP_vec...
%                     .*(gas_density_vec - melt_density)...
%                     + gas_massfrac_vec*melt_density.*dgasdensity_dP_vec)...
%                     - (gas_massfrac_vec - 1).*gas_density_vec.^2*dmeltdensity_dP)...
%                     ./((-gas_massfrac_vec.*gas_density_vec + gas_massfrac_vec*melt_density + gas_density_vec).^2);
ddensity_dP =  -((dgasmassfrac_dP/gas_density...
    - dgasmassfrac_dP/melt_density - (gas_massfrac*dgasdensity_dP)...
    /gas_density^2 - ((1 - gas_massfrac)*dmeltdensity_dP)...
    /melt_density^2)/(gas_massfrac/gas_density...
    + (1 - gas_massfrac)/melt_density)^2);



%% viscosity
%%%%melt viscosity model, Giordano et al 2008
%model B coefficients and mol% formulas
M1 = SiO2 + TiO2; b1 = 159.6; 
M2 = Al2O3; b2 = -173.3;
M3 = FeO_Total + MnO + P2O5; b3 = 72.1;
M4 = MgO; b4 = 75.7; 
M5 = CaO; b5 = -39.0 ;
M6 = Na2O + H2Omol + F2O_1; b6 = -84.1;
M7 = H2Omol + F2O_1 + log(1 + H2Omol); b7 = 141.5;
M11 = (SiO2 + TiO2).*(FeO_Total + MnO + MgO); b11 = -2.43;
M12 = (SiO2 + TiO2 + Al2O3 + P2O5).*(Na2O + K2O + H2Omol); b12 = -0.91;
M13 = (Al2O3).*(Na2O + K2O); b13 = 17.6;

%model C coefficients and mol% formulas
N1 = SiO2; c1 = 2.75;
N2 = TiO2 + Al2O3; c2 = 15.7;
N3 = FeO_Total + MnO + MgO; c3 = 8.3;
N4 = CaO; c4 = 10.2;
N5 = Na2O + K2O; c5 = -12.3;
N6 = log(1 + H2Omol + F2O_1); c6 = -99.5;
N11 = (Al2O3 + FeO_Total + MnO + MgO + CaO - P2O5).*(Na2O + K2O + H2Omol + F2O_1); c11 = 0.30;

%melt viscosity model
A = -4.55;
B = b1*M1 + b2*M2 + b3*M3 + b4*M4 + b5*M5 + b6*M6 + b7*M7 + b11*M11 + b12*M12 + b13*M13;
C = c1*N1 + c2*N2 + c3*N3 + c4*N4 + c5*N5 + c6*N6 + c11*N11;
mu_melt = 10.^(A + B./(TK - C));

% Ca_num = melt_viscosity*strain_rate*bubble_radius/surface_tension; %capillary number
%   Ca, which is function of fluid velocity and thus
%   of "fluid wave amplitude", which is not known prior to completing the
%   inversion...
% Cd_num = melt_viscosity*bubble_radius/surface_tension*wave_speed; %
%   dynamic capillary number, see Llewellin and Manga 2005
%   Cd_num obtained by assuming average fluid velocity is of the form uz = Acos(z - ct), 
%   which then gives avg|strain_rate|/avg|strain_rate_rate| = c
% magma_viscosity_ratio_bubble = fsolve(@(magma_viscosity_ratio)
% magma_viscosity_res_fun(magma_viscosity_ratio, Ca_num, porosity, porosity_cr), 1); %full magma viscosity model from Llewellin and Manga 2005 or Gonnermann and Manga 2012
% magma_viscosity_ratio_bubble = (1 - porosity)^(5/3); % function from Manga 2004 for when Cd,Ca >> 1
magma_viscosity_ratio_bubble = (1 - porosity).^(-1); % function from Manga 2004 for when Cd,Ca << 1, which seems to be the case for reasonable ranges of halemaumau vlp wave parameters
% magma_viscosity_ratio_crystal = (1 - crystalvolfrac/crystalvolfrac_cr)^(-2.5); %Gonnermann and Manga 2012, Costa 2005 
mu = magma_viscosity_ratio_bubble.*mu_melt; 
% mu = mu_melt; %!!!ignoring bubbles!!!



%% output
output_struct = struct();
output_struct.density = density;
output_struct.porosity = porosity;
end
