%"jacobian" grid of how parameters effect T and Q for both
%conduit-reservoir and lake sloshing
%josh crozier 2020

addpath(genpath('mains'))
addpath(genpath('source'))

plotslosh = false;

N = 99; %number of variations of each param
lograt = 70; %parameter range ratio at which to use log scale
Tmin = 0;
Tmax = 80;
Qmin = 0;
Qmax = 80;
lambdamin = 1E-7;
lambdamax = 1E0;


%% fixed parameters
tab = table('Size',[1,6],...
    'VariableTypes',{'string','string','double','logical','double','double'},...
    'VariableNames',{'name','text','default','vary','min','max'});

tab(1,:) = {'theta_c', '', 0, false, NaN, NaN}; %angle of conduit from lake center
tab(end+1,:) = {'r_c', 'conduit offset (m)', 50, false, 0, 90}; %conduit top radius (m)
tab(end+1,:) = {'Zrescentroid', 'reservoir centroid el. (m ASL)', 1100-1940, false, 1100-2200, 1100-1800}; %reservoir centroid elev (m asl)
tab(end+1,:) = {'betam_res', '', 5E-10, false, 1E-11, 1E-8}; %rock shear modulus (Pa)



%% varied parameters
tab(end+1,:) = {'rho_laketop', 'lake top density (kg/m^3)', 400, true, 100, 1200}; %lake top
tab(end+1,:) = {'mu_laketop', 'lake top viscosity (Pas)', 40, true, 1, 10000}; %lake
tab(end+1,:) = {'Zlaketop', 'lake top el. (m ASL)', 950, true, 750, 1050}; %lake surface elev (m asl)
tab(end+1,:) = {'Rlake', 'lake radius (m)', 100, true, 10, 140}; %lake radius (m)
tab(end+1,:) = {'conddip', 'conduit dip (deg)', 90, true, 45, 90}; %conduit dip

tab(end+1,:) = {'rho_condtop', 'conduit top density (kg/m^3)', 1100, true, 300, 2400}; %conduit top
tab(end+1,:) = {'mu_condtop', 'conduit top viscosity (Pas)', 40, true, 1, 10000}; %conduit 
tab(end+1,:) = {'Zlakebot', 'lake bottom el. (m ASL)', 700, true, 500, 900}; %lake floor elev (m asl)
tab(end+1,:) = {'Rcondtop', 'conduit top radius (m)', 10, true, 1, 30}; %conduit top radius (m)
tab(end+1,:) = {'G', 'rock shear modulus (GPa)', 3.08, true, 0.5, 30}; %rock shear modulus (Pa)

tab(end+1,:) = {'rho_condbot', 'conduit bottom density (kg/m^3)', 2300, true, 1000, 2700}; %conduit bottom
tab(end+1,:) = {'mu_condbot', 'conduit bottom viscosity (Pas)', 40, true, 1, 10000}; %conduit 
tab(end+1,:) = {'Rres', 'reservoir radius (m)', 1250, true, 1000, 1500}; %reservoir radius (m)
tab(end+1,:) = {'Rcondbot', 'conduit bottom radius (m)', 10, true, 1, 30}; %conduit top radius (m)



%% loop through parameters
numplots = sum(tab.vary);
numplotcols = 5;%ceil(sqrt(numplots));
numplotrows = ceil(numplots/numplotcols);
figure()
tiledlayout(numplotrows,numplotcols,'TileSpacing','compact','Padding','compact')
plotcount = 0;
for ii = 1:height(tab)
    if tab.vary(ii)
        plotcount = plotcount + 1;
        %structure of default param values
        p = struct();
        for jj = 1:height(tab)
            p.(tab.name(jj)) = tab.default(jj);
        end

        %loop over param values
        Tvec = NaN(1,N);
        Qvec = NaN(1,N);
        Pvec = NaN(1,N);
        Tlakevec = NaN(1,N);
        Qlakevec = NaN(1,N);
        lambdatvec = NaN(1,N);
        lambdasvec = NaN(1,N);
        lambdabvec = NaN(1,N);
        lambdacvec = NaN(1,N);
        if tab.max(ii)/tab.min(ii) > lograt && sign(tab.max(ii)) == sign(tab.min(ii))
            logplot = true;
            pvec = logspace(log10(tab.min(ii)), log10(tab.max(ii)), N);
        else
            logplot = false;
            pvec = linspace(tab.min(ii), tab.max(ii), N);
        end
        
        if strcmp(tab.name(ii),'Rres')
            pvec_alt = NaN*pvec;
        end
        
        for jj = 1:N
            %change param value
            p.(tab.name(ii)) = pvec(jj);
            
            %calculate dependent parameters
            p.Hcolumn = p.Zlaketop - (p.Zrescentroid + p.Rres);
            p.Hlake = p.Zlaketop - p.Zlakebot;
            p.Lcond = p.Hcolumn - p.Hlake;
            
            if strcmp(tab.name(ii),'Rres')
                pvec_alt(jj) = (p.Zrescentroid + p.Rres);
            end

            %try
                rholist = [p.rho_laketop,...
                    p.rho_condtop,...
                    p.rho_condbot];
                unstable = rholist(1) > rholist(2) ||...
                    rholist(2) > rholist(3);
                if ~unstable
                    [T,Q,T0chao,Tchao,Qchao,P_restop] = T_Q_driver_nonlin_bessel_fun(p.conddip,...%conduit dip angle
                        p.Rres,...%reservoir radius
                        p.Rcondtop,... %top conduit radius
                        p.Rcondbot,... %bottom conduit radius
                        p.G*1E9,... %elastic wall rock shear modulus
                        p.Rlake,...%lake radius
                        p.Hcolumn,... %conduit depth (including lake)
                        p.rho_laketop,... %lake top density
                        p.rho_condtop,... %lake bot density
                        p.rho_condtop,... %cond top density
                        p.rho_condbot,... %cond bot density
                        p.Hlake,...%lake height
                        p.mu_laketop,... %lake top magma visc
                        p.mu_condtop,... %lake bot magma visc
                        p.mu_condtop,... %cond top magma visc
                        p.mu_condbot,... %cond bot magma visc
                        p.betam_res); %reservoir compressibility 
                        Tvec(jj) = T;
                        Qvec(jj) = Q;
                        Pvec(jj) = P_restop;
                    
                    if plotslosh
%                         [Tlake,Qlake,lambdat,lambdas,lambdab,lambdac] = ...
%                             lake_slosh_TQ_volatile_fun(p.Hlake,... %lake height (m)
%                             p.Rlake,... %lake radius (m)
%                             p.Rcondtop,... %top conduit radius (m)
%                             p.Rcondbot,... %bottom conduit radius (m)
%                             p.Lcond,... %conduit length [excluding lake] (m)
%                             p.H2O_condtop,... %wtp
%                             p.H2O_condbot,... %wtp
%                             p.CO2_condtop,... %ppm
%                             p.CO2_condbot,... %ppm
%                             p.H2O_laketop,... %wtp
%                             p.H2O_condtop,...p.H2O_lakebot,... %wtp
%                             p.CO2_laketop,... %ppm
%                             p.CO2_condtop,...p.CO2_lakebot,... %ppm
%                             p.TK_cond,...%Temp (Kelvin)
%                             p.TK_lake,...%Temp (Kelvin)
%                             p.Rres,...%reservoir radius (m)
%                             p.G*1E9,... %rock shear modulus
%                             p.r_c,... %radius to conduit from lake center (m)
%                             p.theta_c,... %angle to conduit
%                             false,...
%                             CO2solppm_table, H2Osolwtp_table);
%                         Tlakevec(jj) = Tlake;
%                         Qlakevec(jj) = Qlake;
%                         lambdatvec(jj) = lambdat;
%                         lambdasvec(jj) = lambdas;
%                         lambdabvec(jj) = lambdab;
%                         lambdacvec(jj) = lambdac;
                    end
                end
            %catch
            %    'error' 
            %    jj
            %end
        end
        %plot
        nexttile
        if strcmp(tab.name(ii),'Rres')
            pvec = pvec_alt;
            tab.text(ii) = 'conduit bottom el. (m ASL)';
        end
        %
%         yyaxis('left')
        p1 = plot(pvec,Tvec,'b','Linewidth',1);
        hold on
        p4 = plot(pvec,Pvec*1e-5,'g:','Linewidth',2);
        if plotslosh
            p2 = plot(pvec,Tlakevec,':b','Linewidth',1);
        end
        if strcmp(tab.name(ii),'Rres')
            p3 = xline(p.Zrescentroid + tab.default(ii),'k--','Linewidth',2);
        else
            p3 = xline(tab.default(ii),'k--','Linewidth',2);
        end
        grid on
        grid minor
%         ylabel('T (s)')
        ylim([Tmin,Tmax])
        xlim([min(pvec),max(pvec)])
%         xlabel(tab.text(ii))
%         yyaxis('right')
        p2 = plot(pvec,Qvec,'r','Linewidth',1);
        plot(pvec,Qlakevec,':r','Linewidth',1)
%         ylabel('Q')
%         ylim([Qmin,Qmax])
        if logplot
            set(gca,'Xscale','Log')
        end
        drawnow
        if plotcount == 1 && plotslosh
            legend([p1,p2],{'conduit-reservoir','lake-sloshing'})
        end
        set(gca,'FontSize',12)
        if ii == height(tab)
            legend([p1,p2,p4,p3],{'T (s)','Q','conduit bottom\newlinepressure (\times10^5 Pa)','default value'})
        end
        yticks(0:20:80)
        
        col = mod(plotcount-1,numplotcols)+1;
        row = floor(plotcount/numplotcols)+1;
        if col == 1
            if row == 1
                ylabel('lava lake top','FontWeight','bold')
            elseif row == 2
                ylabel('conduit top','FontWeight','bold')
            elseif row == 3
                ylabel('conduit bottom','FontWeight','bold')
            end
        end
        if row == 1
            if col == 1
                title('magma density\newline(kg/m^3)')
            elseif col == 2
                title('magma viscosity\newline(Pas)')
            elseif col == 3
                title('elevation\newline(m ASL)')
            elseif col == 4
                title('radius\newline(m)')
            end
        end
        if col == 5
           title(tab.text(ii))
        end
        
        %}
        %plot just lake sloshing damping components
        %{
        yyaxis('left')
        plot(pvec,Tlakevec,'b','Linewidth',1);
        hold on
        xline(tab.default(ii),'k','Linewidth',1);
        grid on
        grid minor
        ylabel('T (s)')
        ylim([Tmin,Tmax])
        xlim([min(pvec),max(pvec)])
        xlabel(tab.text(ii))
        yyaxis('right')
        p1 = plot(pvec,lambdatvec,'-.r','Linewidth',1);
        p2 = plot(pvec,lambdasvec,'-r','Linewidth',1);
        p3 = plot(pvec,lambdabvec,':r','Linewidth',1);
        p4 = plot(pvec,lambdacvec,'--r','Linewidth',1);
        ylabel('Decay Rate')      
        set(gca,'Yscale','log')
        ylim([lambdamin,lambdamax])
        if logplot
            set(gca,'Xscale','Log')
        end
        drawnow
        if plotcount == 1
            legend([p1,p2,p3,p4],{'lake top','lake sides','lake bottom','conduit'})
        end
        %}
    end
end


