conddip = 90;
Rres = 1400;
Zrescentroid = 1100-1940;
Zlaketop = 950;
Zlakebot = 750;
Zcondbot = Zrescentroid + Rres;
Hcolumn = Zlaketop - Zcondbot;
Hlake = Zlaketop - Zlakebot;
Rcondtop = 10;
Rcondbot = 10;
G = 3E9;
Rlake = 100;

rho_laketop = 400;
rho_lakebot = 1000;
rho_condtop = 1000;
rho_condbot = 2600;

mu_lake = 50;
mu_cond = 50;
betam_res = 5E-10;

[T,Q,T0chao,Tchao,Qchao,P_restop] = T_Q_driver_nonlin_bessel_fun(conddip,...%conduit dip angle
    Rres,...%reservoir radius
    Rcondtop,... %top conduit radius
    Rcondbot,... %bottom conduit radius
    G,... %elastic wall rock shear modulus
    Rlake,...%lake radius
    Hcolumn,... %conduit depth (including lake)
    rho_laketop,...
    rho_lakebot,...
    rho_condtop,...
    rho_condbot,...
    Hlake,...
    mu_lake,... %lake top magma visc
    mu_lake,... %lake bot magma visc
    mu_cond,... %cond top magma visc
    mu_cond,... %cond bot magma visc
    betam_res);