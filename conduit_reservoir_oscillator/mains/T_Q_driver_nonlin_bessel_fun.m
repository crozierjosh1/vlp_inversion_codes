function [T,Q,T0chao,Tchao,Qchao,P_restop] = T_Q_driver_nonlin_bessel_fun(conddip,...%conduit dip angle (deg)
    Rres,...%reservoir radius
    Rcondtop,... %top conduit radius
    Rcondbot,... %bottom conduit radius
    G,... %elastic wall rock shear modulus
    Rlake,...%lake radius
    L,... %conduit depth (including lake)
    rho_laketop,... %lake top density
    rho_lakebot,... %lake bot density
    rho_condtop,... %cond top density
    rho_condbot,... %cond bot density
    Hlake,...%lake height
    mu_laketop,... %lake top magma visc
    mu_lakebot,... %lake bot magma visc
    mu_condtop,... %cond top magma visc
    mu_condbot,... %cond bot magma visc
    betam_res) %reservoir compressibility
% conduit-reservoir mode model
% piece-wise linear properties
% josh crozier 2019, based off scripts by chao liang
source = '../source';
addpath(genpath(source));


%% parameters
int_RelTol = 1E-5; %integration tolerance, recommend 1E-4 to 1E-7
omega_guess = 2*pi./[1E4,1E0]; %interval for omega
tol_omega = 1E-5; %tolerance on omega in fzero, recommend 1E-4 or less

g = 9.81; %earth gravitational constant

Lcond = L - Hlake;
rhom_cond = (rho_condtop + rho_condbot)/2;
Acondtop = pi*Rcondtop^2;
if Hlake > 0
    Alake = pi*Rlake^2;
    epsilon = Acondtop/Alake;
else
    Alake = Acondtop;
    epsilon = 1;
end
betac = 3/(4*G); %reservoir compressibility
Vres = 4/3*pi*Rres^3;%reservoir volume


%% chao TQ calc
Tchao = NaN;
T0chao = NaN;
Qchao = NaN;
%{
'!!!doing chao calculations!!!!'
kr = 1./((betam_res + betac)*Vres); %reservoir stiffness eq 73 Liang2019
vm_cond = mu_cond./rhom_cond;%magma kinematic viscosity
deltarhobar = (rho_condbot - rho_condtop)*sin(conddip) + epsilon*rho_laketop; %eq 81 Liang 2019
gamma = kr*Acondtop./(deltarhobar*g);%dim param reservoir/gravity eq 82 Liang 2019
gprime = (1 + gamma).*deltarhobar./rhom_cond*g;%reduced gravity eq 80 Liang 2019
T0chao = 2*pi*sqrt((Lcond)./gprime);%invisid period eq 85 Liang 2019
tauvis = Rcondtop^2./vm_cond;%viscous diffusion time eq 92 Liang 2019
chis = T0chao/(2*pi)./tauvis; %eq 91 Liang 2019
nr = 40;
order = 8;
[A, Fp, op] = discretize(nr, chis, order);
omega = eigs(A, 1, 1i);
Ts = 1/imag(omega);
Qchao = abs(imag(omega)/real(omega))/2;
Tchao = Ts.*T0chao;
%}


%% variable radius TQ calc
%{
%debug plot
zvec = 0:1:L;
Avec = NaN(size(zvec));
rhovec = NaN(size(zvec));
drhodzvec = NaN(size(zvec));
muvec = NaN(size(zvec));
thetavec = NaN(size(zvec));
for ii = 1:length(zvec)
    Avec(ii) = Afun(zvec(ii),Rcondtop,Rcondbot,Rlake,Lcond,Hlake);
    [rhovec(ii), drhodzvec(ii)] = rhofun(zvec(ii),...
        rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
        Hlake,Lcond);
    muvec(ii) = mufun(zvec(ii),mu_laketop,mu_lakebot,mu_condtop,mu_condbot,Hlake,Lcond);
    thetavec(ii) = thetafun(zvec(ii),conddip,Hlake);
end
figure()
subplot(1,3,1)
plot(rhovec,zvec)
subplot(1,3,2)
plot(drhodzvec,zvec)
subplot(1,3,3)
plot(drhodzvec./Avec,zvec)
% figure()
% subplot(1,5,1)
% plot(Avec,zvec),grid on,title('A')
% subplot(1,5,2)
% plot(rhovec,zvec),grid on,title('rho')
% subplot(1,5,3)
% plot(drhodzvec,zvec),grid on,title('drhodz')
% subplot(1,5,4)
% plot(muvec,zvec),grid on,title('mu')
% subplot(1,5,5)
% plot(thetavec,zvec),grid on,title('theta')
%}

int_rho_over_A = integral(@(z) rhofun(z,rho_laketop,rho_lakebot,rho_condtop,rho_condbot,Hlake,Lcond)...
    /Afun(z,Rcondtop,Rcondbot,Rlake,Lcond,Hlake)...
    /sind(thetafun(z,conddip,Hlake)), ...
    0, L, 'ArrayValued', 1, 'AbsTol', 1E-7, 'RelTol', int_RelTol);
int_drhodz_over_A = integral(@(z) drhodz_over_A_fun(z,...
    rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
    Hlake,Lcond,Rcondtop,Rcondbot,Rlake), ...
    0, L, 'ArrayValued', 1, 'AbsTol',1E-9, 'RelTol', int_RelTol);
Ct = (betac + betam_res)*Vres;
Acondbot = pi*Rcondbot^2;
c1 = Acondbot*int_rho_over_A;
c3 = -Acondbot*(g*(int_drhodz_over_A - rho_laketop/Alake) - Ct^-1);

fzerooptions = optimset('Display','none',...
    'PlotFcns',[],...{@optimplotx,@optimplotfval},...
    'FunValCheck','off',...
    'TolX',tol_omega);

% debug plot
%{
omega_vec = 2*pi./linspace(-500,500,1000);
cost_vec = NaN(size(omega_vec));
for ii = 1:length(omega_vec)
    cost_vec(ii) = cost_fun(rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
        Rcondtop,Rcondbot,Rlake,Lcond,Hlake,omega_vec(ii),...
        mu_laketop,mu_lakebot,mu_condtop,mu_condbot,condip,c1,c3,int_RelTol);
end
figure(1),clf
plot(2*pi./(omega_vec),cost_vec,'-k.')
xlabel('T'),ylabel('cost')
ylim([-100,100])
grid on
%}

omega = fzero(@(omega) cost_fun(rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
        Rcondtop,Rcondbot,Rlake,Lcond,Hlake,omega,...
        mu_laketop,mu_lakebot,mu_condtop,mu_condbot,conddip,c1,c3,int_RelTol),...
        omega_guess, fzerooptions);

if ~isfinite(omega)
    error('omega not finite')
elseif omega <= 0
    error('omega <= 0')
end
    
int_besselterm = integral(@(z) int_besselterm_fun(z,...
    rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
    Rcondtop,Rcondbot,Rlake,Lcond,Hlake,omega,...
    mu_laketop,mu_lakebot,mu_condtop,mu_condbot,conddip),...
    0, L, 'ArrayValued', 1, 'AbsTol',10*eps, 'RelTol',int_RelTol);
c2 = 2*1i^(3/2)*Acondbot*int_besselterm;
%'!!!!!!!!!using real c2!!!!!!!!!!
c2 = real(c2);
lambda = real((-c2 + sqrt(c2^2 - 4*c1*c3))/(2*c1));


%alternate lambda, should be same
% zeta = c2/(2*sqrt(c1*c3));
% lambda_alt = omega*zeta;
% Q_alt = 1/(2*zeta);


T = 2*pi/omega;
Q = -pi/(T*lambda);

P_restop = (rho_laketop+rho_lakebot)/2*g*Hlake + (rho_condtop+rho_condbot)/2*g*Lcond;

end


%% cost function
function cost = cost_fun(rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
        Rcondtop,Rcondbot,Rlake,Lcond,Hlake,omega,...
        mu_laketop,mu_lakebot,mu_condtop,mu_condbot,conddip,c1,c3,int_RelTol)
    
    %debugplot
    %{
    zvec = 0:1:Lcond+Hlake;
    besseltermvec = NaN(size(zvec));
    for ii = 1:length(zvec)
        besseltermvec(ii) = int_besselterm_fun(zvec(ii),...
        rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
        Rcondtop,Rcondbot,Rlake,Lcond,Hlake,omega,mu_cond,mu_lake);
    end
    figure(2),clf
    plot(zvec,real(besseltermvec),'-b.'),hold on
    plot(zvec,imag(besseltermvec),'-r.')
    plot(zvec,abs(besseltermvec),'-g.')
    xlabel('z'),ylabel('besselint')
    grid on
    %}
    
    int_besselterm = integral(@(z) int_besselterm_fun(z,...
        rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
        Rcondtop,Rcondbot,Rlake,Lcond,Hlake,omega,...
        mu_laketop,mu_lakebot,mu_condtop,mu_condbot,conddip),...
        0, Lcond+Hlake, 'ArrayValued', 1, 'AbsTol',10*eps, 'RelTol',int_RelTol);
    
    Acondbot = pi*Rcondbot^2;
    c2 = 2*1i^(3/2)*Acondbot*int_besselterm;
    
    %'!!!!!!!!!using real c2!!!!!!!!!!
    c2 = real(c2);
    
%     'setting c2 = 0'
%     c2 = 0;
    
    %!!!sign of sqrt term does not effect T or Q except for signs
    omegapred = imag((-c2 + sqrt(c2^2 - 4*c1*c3))/(2*c1));
    
    
    %alternative omegapred, should be same
%     omega0pred = sqrt(c3/c1);
%     zeta = c2/(2*sqrt(c1*c3));
%     omegapred_alt = omega0pred*sqrt(1-zeta^2);
    
    
    cost = omega - omegapred;
    
    if ~isfinite(int_besselterm) %non-finite causes problems
        cost = NaN;
    end
    if omega <= 0 %keeps solver from using negative values
        cost = NaN;
    end
end


%% bessel terms
function besselterm = int_besselterm_fun(z,...
    rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
    Rcondtop,Rcondbot,Rlake,Lcond,Hlake,omega,...
    mu_laketop,mu_lakebot,mu_condtop,mu_condbot,conddip)
    
    [A,R] = Afun(z,Rcondtop,Rcondbot,Rlake,Lcond,Hlake);
    rho = rhofun(z,rho_laketop,rho_lakebot,rho_condtop,rho_condbot,Hlake,Lcond);
    theta = thetafun(z,conddip,Hlake);
    mu = mufun(z,mu_laketop,mu_lakebot,mu_condtop,mu_condbot,Hlake,Lcond);
    nu = mu/rho;
    
    
%     'using constant nu'
%     rhom = (rho_condtop+rho_condbot)/2;
%     nu = mu/rhom;
    
    
    besselterm1 = mu/(R*A);
    besselterm2 = sqrt(omega/nu)*besselj(1,...
        sqrt(omega/nu)*R*1i^(3/2), 1);
    besselterm3 = besselj(2,...
        sqrt(omega/nu)*R*1i^(3/2), 1);
    besselterm = besselterm1*besselterm2/besselterm3/sind(theta);
    
%     if isnan(besselterm)
%         'debug pause'
%     end
end


%% density over area
function drhodz_over_A = drhodz_over_A_fun(z,...
    rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
    Hlake,Lcond,Rcondtop,Rcondbot,Rlake)

    [~,drhodz] = rhofun(z,rho_laketop,rho_lakebot,rho_condtop,rho_condbot,Hlake,Lcond);
    A = Afun(z,Rcondtop,Rcondbot,Rlake,Lcond,Hlake);
    drhodz_over_A = drhodz/A;
end


%% area and radius
function [A,R] = Afun(z,Rcondtop,Rcondbot,Rlake,Lcond,Hlake)

    if z <= Hlake
        %R = Rcond + (Rlake-Rcond)/Hlake*(z - (L - Hlake));
        R = Rlake;
    else
        R = Rcondtop + (Rcondbot-Rcondtop)/Lcond*(z-Hlake);
    end
    A = pi*R^2;
end


%% theta
function [theta] = thetafun(z,conddip,Hlake)
    if z < Hlake
        theta = 90;
    else
        theta = conddip;
    end
end


%% density
function [rho,drhodz] = rhofun(z,...
    rho_laketop,rho_lakebot,rho_condtop,rho_condbot,...
    Hlake,Lcond)

    Ltaper = 0; %fixed taper len so no singularity
    if z <= Hlake-Ltaper
        drhodz = (rho_laketop-rho_lakebot)/(Hlake-Ltaper);
        rho = rho_laketop - drhodz*z;
    elseif z < Hlake %taper region
        drhodz = (rho_lakebot-rho_condtop)/Ltaper;
        rho = rho_lakebot - drhodz*(z-(Hlake-Ltaper));
    else
        drhodz = (rho_condtop-rho_condbot)/(Lcond-Ltaper);
        rho = rho_condtop - drhodz*(z-Hlake);
    end
end


%% viscosity
function [mu] = mufun(z,...
    mu_laketop,mu_lakebot,mu_condtop,mu_condbot,...
    Hlake,Lcond)

    Ltaper = 0; %fixed taper len so no singularity
    if z <= Hlake-Ltaper
        dmudz = (mu_laketop-mu_lakebot)/(Hlake-Ltaper);
        mu = mu_laketop - dmudz*z;
    elseif z < Hlake %taper region
        dmudz = (mu_lakebot-mu_condtop)/Ltaper;
        mu = mu_lakebot - dmudz*(z-(Hlake-Ltaper));
    else
        dmudz = (mu_condtop-mu_condbot)/(Lcond-Ltaper);
        mu = mu_condtop - dmudz*(z-Hlake);
    end
end

