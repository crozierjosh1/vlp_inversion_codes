conddip = 90;
Rres = 1250;
Zrescentroid = 1100-1940;
Zlaketop = 900;
Zlakebot = 700;
Zcondbot = Zrescentroid + Rres;
Hcolumn = Zlaketop - Zcondbot;
Hlake = Zlaketop - Zlakebot;
Rcondtop = 10;
Rcondbot = 10;
G = 3.08E9;
Rlake = 100;
TK = 1200 + 273.15;

H2O_laketop_wtp = 0.9;
H2O_lakebot_wtp = 2.0;
H2O_condtop_wtp = 2.0;
H2O_condbot_wtp = 1.0;
wtp_CO2_to_ppm = 1/(7.0778e-05);
CO2_laketop_ppm = 1.0*H2O_laketop_wtp*wtp_CO2_to_ppm;
CO2_lakebot_ppm = 1.0*H2O_lakebot_wtp*wtp_CO2_to_ppm;
CO2_condtop_ppm = 1.0*H2O_condtop_wtp*wtp_CO2_to_ppm;
CO2_condbot_ppm = 1.0*H2O_condbot_wtp*wtp_CO2_to_ppm;

% CO2_laketop_ppm = 2.5e4;
% CO2_lakebot_ppm = 2.5e4;
% CO2_condtop_ppm = 2.5e4;
% CO2_condbot_ppm = 0.5e4;

% H2O_laketop_wtp = 0.001;
% H2O_lakebot_wtp = 0.001;
% H2O_condtop_wtp = 0.001;
% H2O_condbot_wtp = 0.001;
% 
% CO2_laketop_ppm = 1;
% CO2_lakebot_ppm = 1;
% CO2_condtop_ppm = 1;
% CO2_condbot_ppm = 1;

currentFolder = pwd;
addpath(strcat(currentFolder,'\source'))
addpath(strcat(currentFolder,'\mains'))
CO2solppm_table = readmatrix('new_Values_CO2solubility_ppm_comb.csv');
H2Osolwtp_table = readmatrix('new_Values_H2Osolubility_wtp_comb.csv');
dz = 0.4; %depth increment for integration, recommend 0.4 or less
TolX = 1E-9; %for fzero in omega calc, has minimal impact on run time, recommend 1E-8 or less

plotprofiles = true;

[T,Q,T0chao,Tchao,Qchao,thermo_unstable,phimax,P_restop] = ...
  T_Q_driver_volatile_nonlin_bessel_fun(conddip,...%conduit dip angle
    Rres,...%reservoir radius
    Rcondtop,... %conduit top radius
    Rcondbot,...
    G,... %elastic wall rock shear modulus
    Rlake,... %lava lake radius
    Hcolumn,... %conduit length (including lake)
    H2O_condtop_wtp,... %wtp
    H2O_condbot_wtp,... %wtp
    CO2_condtop_ppm,... %ppm
    CO2_condbot_ppm,... %ppm
    H2O_laketop_wtp,... %wtp
    H2O_lakebot_wtp,... %wtp
    CO2_laketop_ppm,... %ppm
    CO2_lakebot_ppm,... %ppm
    Hlake,... %lake depth
    TK,...%conduit bottom Temp (Kelvin)
    TK,...%conduit top Temp (Kelvin)
    TK,...%lake bottom Temp (Kelvin)
    TK,...%lake top Temp (Kelvin)
    plotprofiles,...
    CO2solppm_table, H2Osolwtp_table,dz,TolX);