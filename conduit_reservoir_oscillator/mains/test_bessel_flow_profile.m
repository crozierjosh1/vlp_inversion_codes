%test bessel flow profile
%from womersley 1955

rho = 1000;
omega = 2*pi/15;
mu = 1E2;
nu = mu/rho;
R = 10;
rvec = linspace(0,R,100);
uvec = zeros(size(rvec));
dt = pi/omega/11;
tvec = [0:dt:pi/omega];
wmat = zeros(length(tvec),length(rvec));
for ii = 1:length(rvec)
    besselrterm = besselj(0, rvec(ii)*sqrt(omega/nu)*1i^(3/2));
    besselRterm = besselj(0, R*sqrt(omega/nu)*1i^(3/2));
    uvec(ii) = 1/(rho*1i*omega)*(1 - besselrterm/besselRterm);
    for jj = 1:length(tvec)
        wmat(jj,ii) = uvec(ii)*exp(1i*omega*tvec(jj));
    end
end

figure()
% subplot(2,1,1)
% plot(rvec,real(uvec),'b',rvec,imag(uvec),'r--',rvec,abs(uvec),'k:')
% subplot(2,1,2)
%colors = {[0.25,0.25,0.8],[0.25,0.8,0.25],[0.8,0.25,0.25]};
colors = jet(length(tvec));
for jj = 1:length(tvec)
    plot(rvec,real(wmat(jj,:)),'Color',colors(jj,:),'LineWidth',1)
%     hold on
%     plot(rvec,imag(wmat(jj,:)),'--','Color',colors{jj})
%     plot(rvec,abs(wmat(jj,:)),':','Color',colors{jj})
    hold on
end