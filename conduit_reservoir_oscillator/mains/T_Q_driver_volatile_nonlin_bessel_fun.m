function [T,Q,T0chao,Tchao,Qchao,thermo_unstable,phimax,P_restop] = ...
    T_Q_driver_volatile_nonlin_bessel_fun(conddip,...%conduit dip angle (deg)
        Rres,...%reservoir radius
        Rcondtop,... %conduit top radius
        Rcondbot,... %conduit bottom radius
        G,... %elastic wall rock shear modulus
        Rlake,... %lava lake radius
        L,... %conduit depth (including lake)
        H2O_condtop_wtp,... %wtp
        H2O_condbot_wtp,... %wtp
        CO2_condtop_ppm,... %ppm
        CO2_condbot_ppm,... %ppm
        H2O_laketop_wtp,... %wtp
        H2O_lakebot_wtp,... %wtp
        CO2_laketop_ppm,... %ppm
        CO2_lakebot_ppm,... %ppm
        Hlake,...%lake depth
        TK_condbot,...%conduit bottom Temp (Kelvin)
        TK_condtop,...%conduit top Temp (Kelvin)
        TK_lakebot,...%lake bottom Temp (Kelvin)
        TK_laketop,...%lake top Temp (Kelvin)
        plotprofiles,...
        CO2solppm_table, H2Osolwtp_table,dz,TolX)
% conduit-reservoir mode model
% piece-wise linear properties
% using magma properties from stratified volatile cases
% josh crozier 2019, based off scripts by chao liang
addpath(genpath('..\source'));
addpath(genpath('source'));


%% parameters
g = 9.81; %earth gravitational constant
omega_guess = 2*pi./[1E5,1E-2]; %interval for omega


%% magma properties
if ~exist('CO2solppm_table','var')
    %http://calcul-isto.cnrs-orleans.fr/thermodynamics/applications/#/script/js_compute~H2O-CO2%20systems~H2O%20and%20CO2%20solubility.js
    CO2solppm_table = readmatrix('new_Values_CO2solubility_ppm_comb.csv');
    H2Osolwtp_table = readmatrix('new_Values_H2Osolubility_wtp_comb.csv');
end
% depth increment, recommend 0.4 or less
% if ~exist('dz','var')
%     dz = 1;
% end
%for fzero in omega calc, has minimal impact on run time, recommend 1E-8 or less
% if ~exist('TolX','var')
%     TolX = 1E-6;
% end

magma_prop_struct = magma_prop_from_volatiles_variableA_nonlin(conddip,...
    H2O_condtop_wtp,H2O_condbot_wtp,CO2_condtop_ppm,CO2_condbot_ppm,...
    H2O_laketop_wtp,H2O_lakebot_wtp,CO2_laketop_ppm,CO2_lakebot_ppm,...
    Hlake,L,Rres,Rcondtop,Rcondbot,Rlake,NaN,...
    TK_condbot,TK_condtop,TK_lakebot,TK_laketop,plotprofiles,...
    CO2solppm_table,H2Osolwtp_table,dz);

% rhombar_res = magma_prop_struct.density_res;
% rhombar_cond = magma_prop_struct.density_cond;
% rho0bar_cond = magma_prop_struct.density_cond_bot;
% rhoLbar_cond = magma_prop_struct.density_cond_top;
rhoLbar = magma_prop_struct.density_condlake_top;
% mu_cond = magma_prop_struct.mu_cond;
betam_res = magma_prop_struct.density_res.^-1.*magma_prop_struct.ddensity_dP_res;
thermo_unstable = magma_prop_struct.thermo_unstable;
phimax = magma_prop_struct.phimax;
P_restop = magma_prop_struct.P_restop;

Acondtop = pi*((Rcondtop+Rcondbot)/2)^2;
if Hlake > 0
    Alake = pi*Rlake^2;
%     epsilon = Acondtop/Alake;
else
    Alake = Acondtop;
%     epsilon = 1;
end
betac = 3/(4*G); %reservoir compressibility
Vres = 4/3*pi*Rres^3;%reservoir volume


%% chao TQ calc
Qchao = NaN;
Tchao = NaN;
T0chao = NaN;
%{
kr = 1./((betam_res + betac)*Vres); %reservoir stiffness eq 73 Liang2019
vm_cond = mu_cond./rhombar_cond;%magma kinematic viscosity
deltarhobar = (rho0bar_cond - rhoLbar_cond)*sin(conddip) + epsilon*rhoLbar_cond; %eq 81 Liang 2019
gamma = kr*Acondtop./(deltarhobar*g);%dim param reservoir/gravity eq 82 Liang 2019
gprime = (1 + gamma).*deltarhobar./rhombar_cond*g;%reduced gravity eq 80 Liang 2019
T0chao = 2*pi*sqrt((L-Hlake)./gprime);%invisid period eq 85 Liang 2019
tauvis = ((Rcondtop+Rcondbot)/2)^2./vm_cond;%viscous diffusion time eq 92 Liang 2019
chis = T0chao/(2*pi)./tauvis; %eq 91 Liang 2019
nr = 40;
order = 8;
[A, Fp, op] = discretize(nr, chis, order);
try
    omega = eigs(A, 1, 1i);
catch
    omega = NaN;
end
Ts = 1/imag(omega);
Qchao = abs(imag(omega)/real(omega))/2;
Tchao = Ts.*T0chao;
%}


%% variable radius TQ calc
Ct = (betac + betam_res)*Vres;
Acond0 = pi*Rcondbot^2;
c1 = Acond0*magma_prop_struct.int_rho_over_A;
c3 = -Acond0*(g*(magma_prop_struct.int_drhodz_over_A - rhoLbar/Alake) - Ct^-1);

fzerooptions = optimset('Display','none',...
    'PlotFcns',[],...%{'optimplotfval'},...
    'FunValCheck','off',...
    'TolX',TolX);

%debug plot
%{
omega_vec = 2*pi./logspace(-4,6,80);
cost_vec = NaN(size(omega_vec));
for ii = 1:length(omega_vec)
    cost_vec(ii) = cost_fun(Acond0,c1,c3,omega_vec(ii),magma_prop_struct);
end
figure()
semilogx(2*pi./(omega_vec),cost_vec,'-k.')
grid on
%}

try
    omega = fzero(@(omega) cost_fun(Acond0,c1,c3,omega,magma_prop_struct),...
            omega_guess, fzerooptions);

    besseltermvec1 = magma_prop_struct.mid_mu_vec_condlake./...
        (magma_prop_struct.mid_Rvec_condlake.*magma_prop_struct.Acondlakevec);
    nu = magma_prop_struct.mid_mu_vec_condlake./...
        magma_prop_struct.mid_density_vec_condlake;
    %using scaled besselfuncs, but scales should cancel out
    besseltermvec2 = sqrt(omega./nu).*besselj(1,...
        sqrt(omega./nu).*magma_prop_struct.mid_Rvec_condlake*1i^(3/2), 1);
    besseltermvec3 = besselj(2,...
        sqrt(omega./nu).*magma_prop_struct.mid_Rvec_condlake*1i^(3/2), 1);
    besseltermvec = besseltermvec1.*besseltermvec2./besseltermvec3;
    conddipvec = 90+zeros(size(besseltermvec));
    conddipvec(magma_prop_struct.mid_zvec_condlake > Hlake) = conddip;
    int_besselterm = sum(magma_prop_struct.delta_zvec_condlake.*besseltermvec./sind(conddipvec));

    c2 = 2*1i^(3/2)*Acond0*int_besselterm;
    %'!!!!!!!!!using real c2!!!!!!!!!!
    c2 = real(c2);
    lambda = real((-c2 + sqrt(c2^2 - 4*c1*c3))/(2*c1));
    T = 2*pi/omega;
    Q = -pi/(T*lambda);
    
catch
    T = NaN;
    Q = NaN;
end

%debug plot
% figure()
% subplot(1,3,1)
% plot(magma_prop_struct.mid_density_vec_condlake, magma_prop_struct.mid_zvec_condlake)
% subplot(1,3,2)
% plot(magma_prop_struct.d_density_dz_vec_condlake, magma_prop_struct.mid_zvec_condlake)
% subplot(1,3,3)
% plot(magma_prop_struct.d_density_dz_vec_condlake./magma_prop_struct.Acondlakevec, magma_prop_struct.mid_zvec_condlake)

end

function cost = cost_fun(Acond_0,c1,c3,omega,magma_prop_struct)
    
%     magma_prop_struct = magma_prop_from_volatiles_variableA_nonlin(...
%         H2O_condtop_wtp,H2O_condbot_wtp,CO2_condtop_ppm,CO2_condbot_ppm,...
%         H2O_laketop_wtp,H2O_lakebot_wtp,CO2_laketop_ppm,CO2_lakebot_ppm,...
%         Hlake,L,Rres,Rcondtop,Rcondbot,Rlake,omega,TK_cond,TK_lake,false,...
%         CO2solppm_table,H2Osolwtp_table);
%     int_besselterm = magma_prop_struct.int_besselterm;

    besseltermvec1 = magma_prop_struct.mid_mu_vec_condlake./...
        (magma_prop_struct.mid_Rvec_condlake.*magma_prop_struct.Acondlakevec);
    nu = magma_prop_struct.mid_mu_vec_condlake./magma_prop_struct.mid_density_vec_condlake;
    %using scaled besselfuncs, but scales should cancel out
    besseltermvec2 = sqrt(omega./nu).*besselj(1,...
        sqrt(omega./nu).*magma_prop_struct.mid_Rvec_condlake*1i^(3/2), 1);
    besseltermvec3 = besselj(2,...
        sqrt(omega./nu).*magma_prop_struct.mid_Rvec_condlake*1i^(3/2), 1);
    besseltermvec = besseltermvec1.*besseltermvec2./besseltermvec3;
    int_besselterm = sum(magma_prop_struct.delta_zvec_condlake.*besseltermvec);

    c2 = 2*1i^(3/2)*Acond_0*int_besselterm;
    %'!!!!!!!!!using real c2!!!!!!!!!!
    c2 = real(c2);
    %!!!sign of sqrt term does not effect T or Q except for signs
    omegapred = imag((-c2 + sqrt(c2^2 - 4*c1*c3))/(2*c1));
    cost = omega - omegapred;
end

