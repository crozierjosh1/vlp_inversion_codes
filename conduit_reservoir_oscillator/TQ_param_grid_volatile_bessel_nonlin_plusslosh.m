%"jacobian" grid of how parameters effect T and Q for both
%conduit-reservoir and lake sloshing
%josh crozier 2020

addpath(genpath('mains'))
addpath(genpath('source'))

plotslosh = false;

N = 99; %number of variations of each param
lograt = 3500; %parameter range ratio at which to use log scale
Tmin = 0;
Tmax = 80;
Qmin = 0;
Qmax = 80;
lambdamin = 1E-7;
lambdamax = 1E0;
dz = 0.5; %depth increment for integration
TolX = 1E-8;


%% fixed parameters
tab = table('Size',[1,6],...
    'VariableTypes',{'string','string','double','logical','double','double'},...
    'VariableNames',{'name','text','default','vary','min','max'});


% tab(end+1,:) = {'Zlakebot', '', 700, false, NaN, NaN}; %lake floor elev (m asl)
tab(1,:) = {'theta_c', '', 0, false, NaN, NaN}; %angle of conduit from lake center
tab(end+1,:) = {'r_c', 'conduit offset (m)', 50, false, 0, 90}; %conduit top radius (m)
tab(end+1,:) = {'Zrescentroid', '', 1100-1940, false, 1100-2200, 1100-1800}; %reservoir centroid elev (m asl)


%% varied parameters
tab(end+1,:) = {'H2O_laketop', 'lake top H2O (wtp)', 1.25, true, 0.1, 3}; %lake top H2O (wtp)
tab(end+1,:) = {'CO2_laketop', 'lake top CO2 (10^3 ppm)', 30, true, 1, 60}; %lake top CO2 (ppm)
tab(end+1,:) = {'T_laketop', 'lake top temp (C)', 1200, true, 900, 1400}; %lake Temp (C)
tab(end+1,:) = {'Zlaketop', 'lake top el. (m ASL)', 950, true, 600, 1050}; %lake surface elev (m asl)
tab(end+1,:) = {'Rlake', 'lake radius (m)', 100, true, 10, 140}; %lake radius (m)
tab(end+1,:) = {'conddip', 'conduit dip (deg)', 90, true, 45, 90}; %conduit dip

tab(end+1,:) = {'H2O_condtop', 'conduit top H2O (wtp)', 1.25, true, 0.1, 3}; %conduit top H2O (wtp)
tab(end+1,:) = {'CO2_condtop', 'conduit top CO2 (10^3 ppm)', 30, true, 1, 60}; %conduit top CO2 (ppm)
tab(end+1,:) = {'T_condtop', 'conduit top temp (C)', 1200, true, 900, 1400}; %conduit Temp (C)
tab(end+1,:) = {'Zlakebot', 'lake bottom el. (m ASL)', 700, true, 500, 900}; %lake floor elev (m asl)
tab(end+1,:) = {'Rcondtop', 'conduit top radius (m)', 10, true, 1, 30}; %conduit top radius (m)
tab(end+1,:) = {'G', 'rock shear modulus (GPa)', 3.08, true, 0.5, 30}; %rock shear modulus (Pa)

tab(end+1,:) = {'H2O_condbot', 'conduit bottom H2O (wtp)', 1.25, true, 0.1, 3}; %conduit bottom H2O (wtp)
tab(end+1,:) = {'CO2_condbot', 'conduit bottom CO2 (10^3 ppm)', 30, true, 1, 60}; %conduit bottom CO2 (ppm)
tab(end+1,:) = {'T_condbot', 'conduit bottom temp (C)', 1200, true, 900, 1400}; %conduit Temp (C)
tab(end+1,:) = {'Rres', 'reservoir radius (m)', 1250, true, 1000, 1500}; %reservoir radius (m)
tab(end+1,:) = {'Rcondbot', 'conduit bottom radius (m)', 10, true, 1, 30}; %conduit top radius (m)




%% precalculated parameters
%http://calcul-isto.cnrs-orleans.fr/thermodynamics/applications/#/script/js_compute~H2O-CO2%20systems~H2O%20and%20CO2%20solubility.js
CO2solppm_table = readmatrix('new_Values_CO2solubility_ppm_comb.csv');
H2Osolwtp_table = readmatrix('new_Values_H2Osolubility_wtp_comb.csv');


%% loop through parameters
numplots = sum(tab.vary);
numplotcols = 6;%ceil(sqrt(numplots));
numplotrows = ceil(numplots/numplotcols);
figure()
tiledlayout(numplotrows,numplotcols,'TileSpacing','compact','Padding','compact')
plotcount = 0;
for ii = 1:height(tab)
    if tab.vary(ii)
        plotcount = plotcount + 1;
        %structure of default param values
        p = struct();
        for jj = 1:height(tab)
            p.(tab.name(jj)) = tab.default(jj);
        end

        %loop over param values
        Tvec = NaN(1,N);
        Qvec = NaN(1,N);
        Pvec = NaN(1,N);
        Tlakevec = NaN(1,N);
        Qlakevec = NaN(1,N);
        lambdatvec = NaN(1,N);
        lambdasvec = NaN(1,N);
        lambdabvec = NaN(1,N);
        lambdacvec = NaN(1,N);
        if tab.max(ii)/tab.min(ii) > lograt && sign(tab.max(ii)) == sign(tab.min(ii))
            logplot = true;
            pvec = logspace(log10(tab.min(ii)), log10(tab.max(ii)), N);
        else
            logplot = false;
            pvec = linspace(tab.min(ii), tab.max(ii), N);
        end
        
        if strcmp(tab.name(ii),'Rres')
            pvec_alt = NaN*pvec;
        end
        
        for jj = 1:N
            %change param value
            p.(tab.name(ii)) = pvec(jj);
            
            %calculate dependent parameters
            p.Hcolumn = p.Zlaketop - (p.Zrescentroid + p.Rres);
            p.Hlake = p.Zlaketop - p.Zlakebot;
            p.Lcond = p.Hcolumn - p.Hlake;
            p.TK_condbot = p.T_condbot + 273.15;
            p.TK_condtop = p.T_condtop + 273.15;
            p.TK_laketop = p.T_laketop + 273.15;
            
            if strcmp(tab.name(ii),'Rres')
                pvec_alt(jj) = (p.Zrescentroid + p.Rres);
            end

            %try
                [T,Q,~,~,~,unstable,phimax,P_restop] = ...
                    T_Q_driver_volatile_nonlin_bessel_fun(...
                    p.conddip,...%conduit dip angle
                    p.Rres,...%reservoir radius
                    p.Rcondtop,... %conduit top radius
                    p.Rcondbot,... %conduit bottom radius
                    p.G*1E9,... %elastic wall rock shear modulus
                    p.Rlake,... %lava lake radius
                    p.Hcolumn,... %magma column height (including lake)
                    p.H2O_condtop,... %wtp
                    p.H2O_condbot,... %wtp
                    p.CO2_condtop*10^3,... %ppm
                    p.CO2_condbot*10^3,... %ppm
                    p.H2O_laketop,... %wtp
                    p.H2O_condtop,...p.H2O_lakebot,... %wtp
                    p.CO2_laketop*10^3,... %ppm
                    p.CO2_condtop*10^3,...p.CO2_lakebot,... %ppm
                    p.Hlake,... %lake height
                    p.TK_condbot,... %conduit bottom temp
                    p.TK_condtop,...%conduit top temp
                    p.TK_condtop,...%lake bottom temp
                    p.TK_laketop,... %lake top temp
                    false,...
                    CO2solppm_table, H2Osolwtp_table, dz,TolX); 
                if ~unstable
                    Tvec(jj) = T;
                    Qvec(jj) = Q;
                    Pvec(jj) = P_restop;
                    
                    if plotslosh
                        [Tlake,Qlake,lambdat,lambdas,lambdab,lambdac] = ...
                            lake_slosh_TQ_volatile_fun(p.Hlake,... %lake height (m)
                            p.Rlake,... %lake radius (m)
                            p.Rcondtop,... %top conduit radius (m)
                            p.Rcondbot,... %bottom conduit radius (m)
                            p.Lcond,... %conduit length [excluding lake] (m)
                            p.H2O_condtop,... %wtp
                            p.H2O_condbot,... %wtp
                            p.CO2_condtop,... %ppm
                            p.CO2_condbot,... %ppm
                            p.H2O_laketop,... %wtp
                            p.H2O_condtop,...p.H2O_lakebot,... %wtp
                            p.CO2_laketop,... %ppm
                            p.CO2_condtop,...p.CO2_lakebot,... %ppm
                            p.TK_cond,...%Temp (Kelvin)
                            p.TK_lake,...%Temp (Kelvin)
                            p.Rres,...%reservoir radius (m)
                            p.G*1E9,... %rock shear modulus
                            p.r_c,... %radius to conduit from lake center (m)
                            p.theta_c,... %angle to conduit
                            false,...
                            CO2solppm_table, H2Osolwtp_table);
                        Tlakevec(jj) = Tlake;
                        Qlakevec(jj) = Qlake;
                        lambdatvec(jj) = lambdat;
                        lambdasvec(jj) = lambdas;
                        lambdabvec(jj) = lambdab;
                        lambdacvec(jj) = lambdac;
                    end
                end
            %catch
            %    'error' 
            %    jj
            %end
        end
        %plot
        nexttile
        if strcmp(tab.name(ii),'Rres')
            pvec = pvec_alt;
            tab.text(ii) = 'conduit bottom el. (m ASL)';
        end
        %
%         yyaxis('left')
        p1 = plot(pvec,Tvec,'b','Linewidth',1);
        hold on
        p4 = plot(pvec,Pvec*1e-5,'g:','Linewidth',2);
        if plotslosh
            p2 = plot(pvec,Tlakevec,':b','Linewidth',1);
        end
        if strcmp(tab.name(ii),'Rres')
            p3 = xline(p.Zrescentroid + tab.default(ii),'k--','Linewidth',2);
        else
            p3 = xline(tab.default(ii),'k--','Linewidth',2);
        end
        grid on
        grid minor
%         ylabel('T (s)')
        ylim([Tmin,Tmax])
        xlim([min(pvec),max(pvec)])
%         xlabel(tab.text(ii))
%         yyaxis('right')
        p2 = plot(pvec,Qvec,'r','Linewidth',1);
        plot(pvec,Qlakevec,':r','Linewidth',1)
%         ylabel('Q')
%         ylim([Qmin,Qmax])
        if logplot
            set(gca,'Xscale','Log')
        end
        drawnow
        if plotcount == 1 && plotslosh
            legend([p1,p2],{'conduit-reservoir','lake-sloshing'})
        end
        set(gca,'FontSize',12)
        if ii == height(tab)
            legend([p1,p2,p4,p3],{'T (s)','Q','conduit bottom pressure (\times10^5 Pa)','default value'})
        end
        yticks(0:20:80)
        
        col = mod(plotcount-1,numplotcols)+1;
        row = floor(plotcount/numplotcols)+1;
        if col == 1
            if row == 1
                ylabel('lava lake top','FontWeight','bold')
            elseif row == 2
                ylabel('conduit top','FontWeight','bold')
            elseif row == 3
                ylabel('conduit bottom','FontWeight','bold')
            end
        end
        if row == 1
            if col == 1
                title('total H2O\newline(wt%)')
            elseif col == 2
                title('total CO2\newline(ppm)')
            elseif col == 3
                title('magma temp\newline(C)')
            elseif col == 4
                title('elevation\newline(m ASL)')
            elseif col == 5
                title('radius\newline(m)')
            end
        end
        if col == 6
           title(tab.text(ii))
        end
        
        %}
        %plot just lake sloshing damping components
        %{
        yyaxis('left')
        plot(pvec,Tlakevec,'b','Linewidth',1);
        hold on
        xline(tab.default(ii),'k','Linewidth',1);
        grid on
        grid minor
        ylabel('T (s)')
        ylim([Tmin,Tmax])
        xlim([min(pvec),max(pvec)])
        xlabel(tab.text(ii))
        yyaxis('right')
        p1 = plot(pvec,lambdatvec,'-.r','Linewidth',1);
        p2 = plot(pvec,lambdasvec,'-r','Linewidth',1);
        p3 = plot(pvec,lambdabvec,':r','Linewidth',1);
        p4 = plot(pvec,lambdacvec,'--r','Linewidth',1);
        ylabel('Decay Rate')      
        set(gca,'Yscale','log')
        ylim([lambdamin,lambdamax])
        if logplot
            set(gca,'Xscale','Log')
        end
        drawnow
        if plotcount == 1
            legend([p1,p2,p3,p4],{'lake top','lake sides','lake bottom','conduit'})
        end
        %}
    end
end


