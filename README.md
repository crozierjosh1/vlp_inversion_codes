This DATSETNAMEreadme.txt file was generated on 2021-12-30 by Josh Crozier


GENERAL INFORMATION

1. Title of Dataset: Inversion results and codes corresponding to manuscript "Evolving magma temperature and volatile contents over the 2008-2018 eruption of Kilauea Volcano"

2. Author Information
	A. Principal Contact Information
		Name: Josh Crozier
		Institution: University of Oregon (now USGS California Volcano Observatory)
		Email: jcrozier@usgs.gov or crozierjosh1@gmail.com

	B. Alternate Contact Information
		Name: Leif Karlstrom
		Institution: University of Oregon
		Email: leif@uoregon.edu


3. Date of data collection (single date, range, approximate date): 
2008-01-01 to 2018-08-01

4. Geographic location of data collection: 
Kilauea Volcano, Hawaii, USA

5. Information about funding sources that supported the collection of the data: 
Leif Karlstrom acknowledges support for this work from NSF EAR-2036980


SHARING/ACCESS INFORMATION

1. Licenses/restrictions placed on the data:
Public Domain/CC-0

2. Links to publications that cite or use the data: 
Crozier, J and Karlstrom, L, Science Advances (2022) Evolving magma temperature and volatile contents over the 2008-2018 eruption of Kilauea Volcano

3. Links to other publicly accessible locations of the data: 
bitbucket.org/crozierjosh1/vlp_inversion_codes

4. Links/relationships to ancillary data sets: 
None

5. Was data derived from another source? 
yes, seismic catalogs were derived from the USGS HVO seismic network and GNSS data was obtained from UNR (http://geodesy.unr.edu/)

6. Recommended citation for this dataset: 
please cite the publication "Evolving magma temperature and volatile contents over the 2008-2018 eruption of Kilauea Volcano"


DATA & FILE OVERVIEW

1. File List: 

GPS_data: folder containing GPS data and inversion codes
	spheroid_invert_GPS.m: main script that conducts GPS inversions
	????PA.txt: GPS data from UNR
	P_summit_SCR....csv: GPS inversion results for different parameters
	Other files are helper functions and/or plotting resources
Conduit_reservoir_oscillator: folder containing magma resonance modeling codes
	mains: folder containing main model codes
		T_Q_driver_volatile_nonlin_bessel_script.m: script to run main model
		T_Q_driver_nonlin_bessel_script.m: script to run density/viscosity model
	source: folder containing helper functions and magma solubility tables
	Other files are helper functions and/or plotting resources
catalog_invert.m: main script running inversions for VLP catalog
volatile_inv_tab_cr....mat: VLP catalog inversions for different parameters
total_TQ_catalog_trimmed.mat: VLP catalog

Other files are helper functions and/or plotting resources


2. Relationship between files, if important: 
see file descriptions above

3. Additional related data collected that was not included in the current data package: 
None

4. Are there multiple versions of the dataset? 
no


METHODOLOGICAL INFORMATION

1. Description of methods used for collection/generation of data: 
see publication "Evolving magma temperature and volatile contents over the 2008-2018 eruption of Kilauea Volcano"

2. Methods for processing the data: 
see publication "Evolving magma temperature and volatile contents over the 2008-2018 eruption of Kilauea Volcano"

3. Instrument- or software-specific information needed to interpret the data: 
Matlab r2020, Matlab Wavelet Toolbox, (optional) Matlab Parallel Computing Toolbox
(codes were tested on both a Windows 10 PC and linux based Talapas HPC https://hpcf.uoregon.edu/content/talapas)

4. Standards and calibration information, if appropriate: 
NA

5. Environmental/experimental conditions: 
NA

6. Describe any quality-assurance procedures performed on the data: 
see publication "Evolving magma temperature and volatile contents over the 2008-2018 eruption of Kilauea Volcano"

7. People involved with sample collection, processing, analysis and/or submission: 
Josh Crozier and Leif Karlstrom
